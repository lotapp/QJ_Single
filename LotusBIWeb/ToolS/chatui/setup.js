﻿var bot = new ChatSDK({
    config: {
        navbar: {
            logo: 'https://gw.alicdn.com/tfs/TB1Wbldh7L0gK0jSZFxXXXWHVXa-168-33.svg',
            title: '浙江政务服务网',
        },
        robot: {
            avatar: '//gw.alicdn.com/tfs/TB1U7FBiAT2gK0jSZPcXXcKkpXa-108-108.jpg'
        },
        messages: [
            {
                type: 'text',
                content: {
                    text: '智能助理为您服务，请问有什么可以帮您？'
                }
            }
        ]
    },
    requests: {
        send: function (msg) {
            if (msg.type === 'text') {
                return {
                    url: '//api.server.com/ask',
                    data: {
                        q: msg.content.text
                    }
                };
            }
        }
    }
});
bot.run();