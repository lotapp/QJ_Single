﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using Senparc.Weixin.Entities;
using Senparc.Weixin.Work.AdvancedAPIs.MailList;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace QJY.API
{
    public class AuthManage
    {


        public SqlSugarClient Db;//用来处理事务多表查询和复杂的操作
        public JH_Auth_QYB DBCon = new JH_Auth_QYB();



        #region 登录及密码
        public void MODIFYPWD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            if (P1 == P2)
            {
                P1 = CommonHelp.GetMD5(P1);
                string userName = UserInfo.User.UserName;
                string uName = context.Request("username") ?? "";
                if (!string.IsNullOrEmpty(uName))
                {
                    userName = uName;
                }
                new JH_Auth_UserB().UpdatePassWord(UserInfo.User.ComId.Value, userName, P1);
            }
            else
            {
                msg.ErrorMsg = "确认密码不一致";
            }
        }
        #endregion


        #region 部门管理


        /// <summary>
        /// 添加部门
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">部门名称</param>
        /// <param name="P2">部门描述</param>
        /// <param name="strUserName"></param>
        public void ADDBRANCH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //remark2可作为是否分公司字段
            JH_Auth_Branch branch = new JH_Auth_Branch();
            branch = JsonConvert.DeserializeObject<JH_Auth_Branch>(P1);
            new JH_Auth_BranchB().AddBranch(UserInfo, branch, msg);
        }
        /// <summary>
        /// 获取部门信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETBRANCHBYCODE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int code = int.Parse(P1);
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, code);
            msg.Result = branch;
            msg.Result1 = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, branch.DeptRoot);
        }
        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void DELBRANCH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {


            int deptCode = int.Parse(P1);
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetEntity(d => d.DeptCode == deptCode);
            if (branch != null)
            {

                if (new JH_Auth_UserB().GetEntities(d => d.BranchCode == deptCode && d.ComId == UserInfo.User.ComId).ToList().Count > 0)
                {
                    msg.ErrorMsg = "本部门中存在用户，请先删除用户";
                    return;
                }
                if (new JH_Auth_BranchB().GetEntities(d => d.DeptRoot == branch.DeptCode).Count() > 0)
                {
                    msg.ErrorMsg = "本部门中存在子部门，请先删除子部门";
                    return;
                }
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp bm = new WXHelp(UserInfo.QYinfo);
                    bm.WX_DelBranch(branch.WXBMCode.ToString());
                }
                if (!new JH_Auth_BranchB().Delete(d => d.DeptCode == deptCode))
                {
                    msg.ErrorMsg = "删除部门失败";
                    return;
                }
            }


        }
        /// <summary>
        /// 获取部门列表，分配角色组织机构以及选择组织机构用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETALLBMUSERLISTNEW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //判断是否强制加载所有部门数据
            string isSupAdmin = P2;//是不是超级管理员
            DataTable dtBMS = new DataTable();
            int deptRoot = 1;//默认从根目录加载
            string branchId = "";

            //if (UserInfo.UserRoleCode.Contains("1218"))
            //{
            //    isSupAdmin = "Y";
            //    //超级管理员权限
            //}
            //if (isSupAdmin == "N")
            //{
            //    deptRoot = int.Parse(UserInfo.User.remark);
            //    branchId = UserInfo.UserBMQXCode;
            //    //不是超级管理员,从单位开始加载,权限部门设为空
            //}
            //获取有权限的部门Id
            string strUserTree = "[" + new JH_Auth_BranchB().GetBranchTree(deptRoot, UserInfo.User.ComId.Value, P1, branchId).TrimEnd(',') + "]";
            msg.Result = strUserTree;
            msg.Result1 = UserInfo.User.BranchCode;
        }

        //新分级显示获取部门数据(获取授权数据)
        public void GETALLBMNEW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtBMS = new DataTable();
            //获取没有权限的部门Id，后面需要删除
            string branchId = new JH_Auth_BranchB().GetBranchQX(UserInfo);
            if (UserInfo.UserRoleCode.Contains("1218"))
            {
                branchId = "";//超级管理员看所有数据
            }
            int rootBrancode = 1;
            //if (!UserInfo.UserRoleCode.Contains("1218"))
            //{
            //    rootBrancode = int.Parse(UserInfo.User.remark);
            //}
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * from JH_Auth_Branch  where DeptCode=" + rootBrancode + " and ComId=" + UserInfo.User.ComId.Value + " order by DeptShort DESC");
            dt.Columns.Add("ChildBranch", Type.GetType("System.Object"));
            dtBMS = new JH_Auth_BranchB().GetBranchList(rootBrancode, UserInfo.User.ComId.Value, branchId);
            dt.Rows[0]["ChildBranch"] = dtBMS;
            msg.Result = dt;
            msg.Result1 = new JH_Auth_BranchB().GetALLEntities();

        }

        #endregion

        #region 企业信息
        /// <summary>
        /// 添加或编辑企业信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void EDITCOMPANY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY company = new JH_Auth_QY();
            company = JsonConvert.DeserializeObject<JH_Auth_QY>(P1);
            if (company.ComId != 0)
            {
                if (!new JH_Auth_QYB().Update(company))
                {
                    msg.ErrorMsg = "修改企业信息失败";
                }
            }
            else
            {
                JH_Auth_QY company1 = new JH_Auth_QYB().GetEntity(d => d.QYName == company.QYName);
                if (company1 != null)
                {
                    msg.ErrorMsg = "企业名称已存在";
                    return;
                }
                company.CRUser = UserInfo.User.UserName;
                company.CRDate = DateTime.Now;
                if (!new JH_Auth_QYB().Insert(company))
                {
                    msg.ErrorMsg = "添加企业信息失败";
                }
            }
        }

        /// <summary>
        /// 获取企业信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETCOMPANYINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = UserInfo.QYinfo;
        }
        public void SAVECOMPANYQZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            UserInfo.QYinfo.DXQZ = P1;
            new JH_Auth_QYB().Update(UserInfo.QYinfo);
        }
        //更新下次不再提示
        public void UPDATECOMPANYNOALERT(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY qymodel = UserInfo.QYinfo;
            qymodel.SystemGGId = "Y";
            new JH_Auth_QYB().Update(qymodel);
        }
        #endregion

        #region 组织部门、人员

        /// <summary>
        /// 添加人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void ADDUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JH_Auth_User user = new JH_Auth_User();
            user = JsonConvert.DeserializeObject<JH_Auth_User>(P1);
            if (string.IsNullOrEmpty(user.UserName))
            {
                msg.ErrorMsg = "用户名必填";
                return;
            }

            if (!Regex.IsMatch(user.UserName.ToLower(), @"(?i)^[0-9a-z]+$"))
            {
                msg.ErrorMsg = "用户名必须是英文和数字组合";
                return;
            }
            //if (string.IsNullOrEmpty(user.mobphone))
            //{
            //    msg.ErrorMsg = "手机号必填";
            //    return;
            //}
            //Regex regexPhone = new Regex("^0?1[3|4|5|8|7][0-9]\\d{8}$");
            //if (!regexPhone.IsMatch(user.mobphone))
            //{
            //    msg.ErrorMsg = "手机号填写不正确";
            //    return;
            //}
            Regex regexOrder = new Regex("^[0-9]*$");
            if (user.UserOrder != null && !regexOrder.IsMatch(user.UserOrder.ToString()))
            {
                msg.ErrorMsg = "序号必须是数字";
                return;
            }
            if (user.ID != 0)
            {
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp wx = new WXHelp(UserInfo.QYinfo);
                    wx.WX_UpdateUser(user);
                }
                if (!new JH_Auth_UserB().Update(user))
                {
                    msg.ErrorMsg = "修改用户失败";
                }
            }
            else
            {
                JH_Auth_User user1 = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, user.UserName);
                if (user1 != null)
                {
                    msg.ErrorMsg = "用户已存在";
                    return;
                }
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.mobphone == user.mobphone && d.ComId == UserInfo.User.ComId).ToList();
                if (userList.Count > 0)
                {
                    msg.ErrorMsg = "此手机号的用户已存在";
                    return;
                }
                user.UserPass = CommonHelp.GetMD5(user.UserPass);
                user.ComId = UserInfo.User.ComId;
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp wx = new WXHelp(UserInfo.QYinfo);
                    wx.WX_CreateUser(user);
                }
                user.CRDate = DateTime.Now;
                user.CRUser = UserInfo.User.UserName;
                user.logindate = DateTime.Now;
                JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(user.ComId.Value, user.BranchCode);

                user.remark = branch.Remark1.Split('-')[0];
                if (!new JH_Auth_UserB().Insert(user))
                {
                    msg.ErrorMsg = "添加用户失败";
                }

            }

            if (P2 != "")
            {
                new JH_Auth_UserRoleB().Delete(d => d.UserName == user.UserName);
                foreach (string code in P2.Split(','))
                {
                    //添加默认员工角色
                    JH_Auth_UserRole Model = new JH_Auth_UserRole();
                    Model.UserName = user.UserName;
                    Model.RoleCode = int.Parse(code);
                    Model.ComId = user.ComId;
                    new JH_Auth_UserRoleB().Insert(Model);
                }
            }

            msg.Result = user;


        }


        /// <summary>
        /// 添加人员-简要
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDUSERV1(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string username = context.Request("username") ?? "";
            string BranchCode = context.Request("deptcode") ?? "";
            string phone = context.Request("phone") ?? "";
            string xm = context.Request("xm") ?? "";
            string rol = context.Request("rol") ?? "";
            string zhiwu = context.Request("zhiwu") ?? "";

            if (string.IsNullOrEmpty(username))
            {
                msg.ErrorMsg = "用户名必填";
                return;
            }

            if (!Regex.IsMatch(username.ToLower(), @"(?i)^[0-9a-z]+$"))
            {
                msg.ErrorMsg = "用户名必须是英文和数字组合";
                return;
            }


            if (new JH_Auth_UserB().GetEntities(d => d.UserName == username).Count() > 0)
            {
                JH_Auth_User USER = new JH_Auth_UserB().GetEntities(d => d.UserName == username).FirstOrDefault();
                USER.UserRealName = xm;
                USER.mobphone = phone;
                USER.zhiwu = zhiwu;

                new JH_Auth_UserB().Update(USER);
            }
            else
            {
                JH_Auth_User USER = new JH_Auth_User();
                USER.UserName = username;
                USER.UserRealName = xm;
                USER.UserPass = CommonHelp.GetMD5("abc123");
                USER.BranchCode = int.Parse(BranchCode);
                USER.CRUser = UserInfo.User.UserName;
                USER.IsUse = "Y";
                USER.ComId = 10334;
                USER.mobphone = phone;
                USER.CRDate = DateTime.Now;
                USER.zhiwu = zhiwu;
                new JH_Auth_UserB().Insert(USER);
            }

            new JH_Auth_UserRoleB().Delete(D => D.UserName == username);
            foreach (string item in rol.Split(','))
            {
                new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { ComId = UserInfo.User.ComId, UserName = username, RoleCode = int.Parse(item) });
            }
        }
        //批量设置部门
        public void PLSETBRANCH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string[] userNames = P1.Split(',');
            int branchCode = 0;
            int.TryParse(P2, out branchCode);
            List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.ComId == UserInfo.User.ComId && userNames.Contains(d.UserName)).ToList();
            foreach (JH_Auth_User user in userList)
            {
                JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(user.ComId.Value, branchCode);

                user.BranchCode = branchCode;
                user.remark = branch.Remark1.Split('-')[0];
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp wx = new WXHelp(UserInfo.QYinfo);
                    wx.WX_UpdateUser(user);
                }
                new JH_Auth_UserB().Update(user);
            }
        }
        /// <summary>
        /// 根据用户删除用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void DELUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = P1;
            foreach (string user in strUserName.Split(','))
            {
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {
                    WXHelp bm = new WXHelp(UserInfo.QYinfo);
                    bm.WX_DelUser(P1);
                }
                new JH_Auth_UserB().Delete(d => d.ComId == UserInfo.QYinfo.ComId && d.UserName == user);

            }



        }
        /// <summary>
        /// 更改用户是否禁用的状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">用户名</param>
        /// <param name="P2">状态</param>
        /// <param name="strUserName"></param>
        public void UPDATEUSERISUSE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            JH_Auth_User UPUser = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, P1);
            UPUser.IsUse = P2;

            if (UserInfo.QYinfo.IsUseWX == "Y")
            {
                WXHelp bm = new WXHelp(UserInfo.QYinfo);
                bm.WX_UpdateUser(UPUser);//为了更新微信用户状态
            }
            if (!new JH_Auth_UserB().Update(UPUser))
            {
                msg.ErrorMsg = "更新失败";
            }

        }

        public void SETISSHOWYD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_User user = UserInfo.User;
            user.IsShowYD = 1;
            new JH_Auth_UserB().Update(user);
        }

        /// <summary>
        /// 根据部门编号获取部门人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETUSERBYCODENEW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            if (!int.TryParse(P1, out deptCode))
            {
                deptCode = 1;
            }
            DataTable dt = new JH_Auth_UserB().GetUserListbyBranch(deptCode, P2, UserInfo.QYinfo.ComId);
            dt.Columns.Add("ROLENAME");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["ROLENAME"] = new JH_Auth_UserRoleB().GetRoleNameByUserName(dt.Rows[i]["UserName"].ToString(), UserInfo.User.ComId.Value);
            }
            msg.Result = dt;
        }
        /// <summary>
        /// 根据部门编号获取部门人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETUSERBYCODENEWPAGE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            int.TryParse(P1, out deptCode);
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, deptCode);
            if (branch == null) { msg.ErrorMsg = "数据异常"; }
            string strQXWhere = "";
            if (deptCode != 1)
            {
                strQXWhere = string.Format("And  (b.deptcode = '{0}' or  b.Remark1 like '{1}-%' )", branch.DeptCode, "1-" + branch.DeptCode);

            }
            string strWhere = string.Format(" (u.zhiwu<>'小组管理员' or u.zhiwu is null ) and u.username <>'administrator' and  u.ComId={0}   {1}", UserInfo.User.ComId, strQXWhere);
            if (P2 != "")
            {
                strWhere += string.Format(" And (u.UserName like '%{0}%'  or u.UserRealName like '%{0}%'  or b.DeptName like '%{0}%' or u.mobphone like '%{0}%' ) ", P2);
            }
            int page = 0;
            int pagecount = 8;
            int.TryParse(context["p"] == null ? "0" : context["p"].ToString(), out page);
            int.TryParse(context["pagecount"] == null ? "8" : context["pagecount"].ToString(), out pagecount);//页数
            page = page == 0 ? 1 : page;
            int total = 0;
            // DataTable dt = new JH_Auth_UserB().Db.SqlQueryable<Object>("select u.*,b.DeptName,b.DeptCode from JH_Auth_User u  inner join JH_Auth_Branch b on u.branchCode=b.DeptCode where " + strWhere).OrderBy("UserOrder asc").ToDataTablePage(page, pagecount, ref total);
            DataTable dt = new JH_Auth_UserB().Db.SqlQueryable<Object>("select u.*,b.DeptName,b.DeptCode,stuff((select distinct ','+ CAST( RoleCode AS VARCHAR) from (select jh_auth_userrole.RoleCode,jh_auth_userrole.UserName,RTrim(jh_auth_role.RoleName) as RoleName from jh_auth_userrole left join jh_auth_role on jh_auth_userrole.RoleCode= jh_auth_role.RoleCode) B where u.UserName=B.UserName for xml path('')),1,1,'') AS ROLECODE,stuff((select distinct ','+ CAST( RoleName AS VARCHAR) from (select jh_auth_userrole.RoleCode,jh_auth_userrole.UserName,RTrim(jh_auth_role.RoleName) as RoleName from jh_auth_userrole left join jh_auth_role on jh_auth_userrole.RoleCode= jh_auth_role.RoleCode) B where u.UserName=B.UserName for xml path('')),1,1,'') AS ROLENAME  from JH_Auth_User u  inner join JH_Auth_Branch b on u.branchCode=b.DeptCode where " + strWhere).OrderBy("UserOrder asc").ToDataTablePage(page, pagecount, ref total);

            msg.Result = dt;
            msg.Result1 = Math.Ceiling(total * 1.0 / 8);
            msg.Result2 = total;
        }
        //导出员工


        /// <summary>
        /// 根据部门编号获取可用人员
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETYUSERBYCODE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int deptCode = 0;
            if (!int.TryParse(P1, out deptCode))
            {
                deptCode = 1;
            }
            DataTable dtUser = new JH_Auth_UserB().GetUserListbyBranchUse(deptCode, P2, UserInfo);
            msg.Result = dtUser;
        }
        //根据角色获取用户
        public void GETUSERBYROLECODE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int roleCode = int.Parse(P1);
            DataTable dt = new JH_Auth_UserRoleB().GetUserDTByRoleCode(roleCode, UserInfo.User.ComId.Value);

            if (!UserInfo.UserRoleCode.Contains("1218"))
            {
                msg.Result = dt.FilterTable("remark='" + UserInfo.User.remark + "'");//根据权限找同一个单位得
            }
            else
            {
                msg.Result = dt;
            }

        }
        /// <summary>
        /// 获取前端需要的人员选择列表
        /// </summary>
        public void GETUSERJS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtUsers = new JH_Auth_UserB().GetDTByCommand(" SELECT UserName,UserRealName,DeptCode,DeptName FROM JH_Auth_User INNER JOIN JH_Auth_Branch ON JH_Auth_User.ComId=JH_Auth_Branch.ComId AND JH_Auth_User.BranchCode=JH_Auth_Branch.DeptCode WHERE  JH_Auth_Branch.ComId='" + UserInfo.User.ComId + "'");
            //获取选择用户需要的HTML和转化用户名需要的json数据
            msg.Result = dtUsers;
        }

        public void SETBRANCHLEADER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE JH_Auth_Branch set BranchLeader='{0}' where  DeptCode={1}", P1, P2);
            new JH_Auth_BranchB().ExsSql(strSql);
        }
        public void SETUSERLEADER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("UPDATE JH_Auth_User set UserLeader='{0}' where  username='{1}' and ComID={2}", P1, P2, UserInfo.User.ComId);
            new JH_Auth_BranchB().ExsSql(strSql);
        }
        #endregion




        #region 角色管理
        public void EDITROLE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Role role = new JH_Auth_Role();
            role = JsonConvert.DeserializeObject<JH_Auth_Role>(P1);

            if (role.RoleCode != 0)
            {
                if (role.RoleCode == 0 && P2 == "")
                {
                    msg.ErrorMsg = "管理员至少有一人！！！";
                    return;
                }
                if (!new JH_Auth_RoleB().Update(role))
                {
                    msg.ErrorMsg = "修改职务失败";
                }

            }
            else
            {
                if (string.IsNullOrEmpty(role.ComId.ToString()))
                {
                    JH_Auth_Role user1 = new JH_Auth_RoleB().GetEntity(d => d.RoleName == role.RoleName && d.ComId == UserInfo.User.ComId && d.IsUse == "Y");
                    if (user1 != null)
                    {
                        msg.ErrorMsg = "职务已存在";
                        return;
                    }
                    role.isSysRole = "N";
                    role.IsHasQX = "1";
                    role.ComId = UserInfo.User.ComId;
                    if (!new JH_Auth_RoleB().Insert(role))
                    {
                        msg.ErrorMsg = "添加职务失败";
                    }
                }

            }
            if (msg.ErrorMsg == "")
            {
                new JH_Auth_UserRoleB().Delete(d => d.RoleCode == role.RoleCode);
            }
            if (P2 != "")
            {
                try
                {
                    var insertObjs = new List<JH_Auth_UserRole>();

                    foreach (string name in P2.Split(','))
                    {
                        insertObjs.Add(new JH_Auth_UserRole() { UserName = name, ComId = UserInfo.User.ComId, RoleCode = role.RoleCode });
                    }
                    var s9 = new JH_Auth_UserRoleB().Db.Insertable(insertObjs.ToArray()).ExecuteCommand();
                }
                catch (Exception ex)
                {
                    msg.ErrorMsg = ex.Message;
                }
            }
            msg.Result = role;
        }

        public void SETROLEUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int intRoleCode = 0;
            int.TryParse(P1, out intRoleCode);
            var insertObjs = new List<JH_Auth_UserRole>();
            foreach (string name in P2.Split(','))
            {
                insertObjs.Add(new JH_Auth_UserRole() { UserName = name, ComId = UserInfo.User.ComId, RoleCode = intRoleCode });
            }
            var s9 = new JH_Auth_UserRoleB().Db.Insertable(insertObjs.ToArray()).ExecuteCommand();

        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="strUserName"></param>
        public void GETROLE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = " where r.PRoleCode<>-1 and r.RoleCode!=0 and ( r.ComId=0 or  r.ComId=" + UserInfo.User.ComId + ")";
            if (P2 == "Y" && !UserInfo.UserRoleCode.Contains("1218"))
            {
                //去掉超级管理员
                strWhere = strWhere + " AND  r.RoleCode !='1218'";

            }
            if (P1 != "")
            {
                int Id = int.Parse(P1);
                msg.Result1 = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, Id);
            }
            DataTable dt = new JH_Auth_RoleB().GetDTByCommand(@" select r.RoleCode,r.RoleName,r.isSysRole,r.RoleQX,r.IsHasQX,displayoRder,COUNT(distinct u.UserName) userCount from JH_Auth_Role r left join JH_Auth_UserRole ur on r.RoleCode=ur.RoleCode " + @"     
                                                               left join JH_Auth_User u on ur.UserName=u.UserName  " + strWhere + " group by r.RoleCode,r.RoleName,r.isSysRole,r.RoleQX,r.IsHasQX,displayoRder ORDER BY displayoRder");

            msg.Result = dt;
            msg.Result2 = UserInfo.QYinfo.ComId.ToString();

        }

        public void DELROLE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int ID = int.Parse(P1);
            JH_Auth_Role role = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == ID);
            if (role != null && role.ComId != 0 && role.RoleCode != 2)
            {
                new JH_Auth_RoleB().delRole(ID, UserInfo.User.ComId.Value);
            }
            else
            {
                msg.ErrorMsg = "此职务不能删除";
            }
        }
        public void GETROLEBYCODE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            int ComId = UserInfo.User.ComId.Value;
            if (Id == 0)
            {
                ComId = 0;
            }
            JH_Auth_Role Role = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == Id && d.ComId == ComId);
            msg.Result = Role;
            int roleCode = int.Parse(P1);
            msg.Result1 = new JH_Auth_UserRoleB().GetDTByCommand("SELECT DISTINCT u.UserName, u.UserRealName,userrole.RoleCode ," + SqlHelp.concat(" B.DeptName+'/'+u.UserRealName ") + " AS BUSER from JH_Auth_User u inner join   JH_Auth_UserRole  userrole on u.username=userrole.username  inner join   jh_auth_branch  B on u.BranchCode=B.DeptCode where userrole.RoleCode=" + roleCode);
            if (P2 == "Y")
            {
                //找同一部门的相同角色用户
                msg.Result1 = new JH_Auth_UserRoleB().GetDTByCommand("SELECT DISTINCT u.UserName, u.UserRealName,userrole.RoleCode ," + SqlHelp.concat(" B.DeptName+'/'+u.UserRealName ") + " AS BUSER from JH_Auth_User u inner join   JH_Auth_UserRole  userrole on u.username=userrole.username  inner join   jh_auth_branch  B on u.BranchCode=B.DeptCode where U.BranchCode='" + UserInfo.User.BranchCode.ToString() + "' AND userrole.RoleCode=" + roleCode);

            }
            if (!string.IsNullOrEmpty(Role.RoleQX))
            {
                //去掉非叶子节点,不然树不好显示啊
                string strQXJG = "";
                DataTable dtqxjg = new JH_Auth_UserRoleB().GetDTByCommand("SELECT DeptCode FROM jh_auth_branch WHERE DeptCode IN (" + Role.RoleQX + ") ");
                for (int i = 0; i < dtqxjg.Rows.Count; i++)
                {
                    strQXJG = strQXJG + dtqxjg.Rows[i]["DeptCode"].ToString() + ",";
                }
                msg.Result2 = strQXJG.Trim(',');
            }

        }

        /// <summary>
        /// 获取用户权限
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETROLEFUN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            if (!string.IsNullOrEmpty(P1) && UserInfo.UserRoleCode != "" && UserInfo.User.isSupAdmin != "Y")
            {
                msg.Result = new JH_Auth_RoleB().GetModelFun(UserInfo.User.ComId.Value, UserInfo.UserRoleCode, P1);
            }
            else
            {

                msg.Result = new JH_Auth_RoleB().GetModelFun(UserInfo.User.ComId.Value, "0", P1);
            }
        }
        //删除角色人员
        public void DELROLEUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int roleCode = 0;
            int.TryParse(P1, out roleCode);
            new JH_Auth_UserRoleB().Delete(d => d.RoleCode == roleCode && P2.Split(',').Contains(d.UserName));
        }



        /// <summary>
        /// 删除人员为0的角色
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void CLEARROLE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            new JH_Auth_UserRoleB().ExsSclarSql(" DELETE FROM jh_auth_role WHERE RoleCode NOT IN ( SELECT DISTINCT RoleCode FROM jh_auth_userrole ) ");
        }
        #endregion




        #region 消息中心
        public void GETXXZXIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" ComId={0} and UserTO='{1}' and isRead=0", UserInfo.User.ComId, UserInfo.User.UserName);
            string strSQL = string.Format(@"SELECT  MsgType,MsgContent,CRDate,UserFrom,isRead,ID,MsgLink from JH_Auth_User_Center Where  {0}  order by CRDate desc", strWhere);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSQL);
            msg.Result = dt;
            msg.Result1 = new JH_Auth_User_CenterB().ExsSclarSql("SELECT count(0) from  JH_Auth_User_Center Where  " + strWhere);
        }
        //抄送给我的消息
        public void GETXXZXABOUTLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format("  isCS='Y'  And  ComId={0} and UserTO='{1}' and isRead=0 ", UserInfo.User.ComId, UserInfo.User.UserName);
            if (P1 != "")
            {
                strWhere += string.Format(" And MsgType='{0}'", P1);
            }
            string strSQL = string.Format(@"SELECT * from JH_Auth_User_Center Where {0}", strWhere);
            var dt = new JH_Auth_User_CenterB().Db.SqlQueryable<JH_Auth_User_Center>(strSQL).OrderBy(d => d.CRDate, OrderByType.Desc).Take(8);
            msg.Result = dt;
            msg.Result1 = new JH_Auth_User_CenterB().ExsSclarSql("SELECT count(0) from  JH_Auth_User_Center Where  " + strWhere);
        }
        //抄送给我的消息
        public void GETXXZXABOUTTYPE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format("  isCS='Y'  And  ComId={0} and UserTO='{1}' and isRead=0 ", UserInfo.User.ComId, UserInfo.User.UserName);
            string strSQL = string.Format(@"SELECT DISTINCT MsgType from JH_Auth_User_Center Where {0} ", strWhere);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSQL);
            msg.Result = dt;

        }
        /// <summary>
        /// 获取用户消息列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">消息类型</param>
        /// <param name="P2">消息内容模糊查询</param>
        /// <param name="strUserName"></param>
        public void GETXXZXISTPAGE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Format(" ComId={0}  And UserTO='{1}'", UserInfo.User.ComId, UserInfo.User.UserName);
            if (P1 != "")
            {
                strWhere += string.Format(" and isRead in ({0}) ", P1);
            }
            if (P2 != "")
            {
                strWhere += string.Format(" and MsgContent like  '%{0}%'", P2);

            }
            string msgType = context["msgType"] != null ? context["msgType"].ToString() : "";
            if (msgType != "")
            {
                strWhere += string.Format(" and MsgType ='{0}'", msgType);
            }
            string msgTypes = context["msgTypes"] != null ? context["msgTypes"].ToString() : "";
            if (msgTypes != "")
            {
                msgTypes = System.Web.HttpUtility.UrlDecode(msgTypes);
                strWhere += string.Format(" and MsgModeID in ('{0}')", msgTypes.Replace(",", "','"));
            }

            int page = 0;
            int pagecount = 8;
            int.TryParse(context["pagecount"] != null ? context["pagecount"].ToString() : "8", out pagecount);
            int.TryParse(context["p"] != null ? context["p"].ToString() : "0", out page);
            page = page == 0 ? 1 : page;
            int total = 0;
            var dt = new JH_Auth_User_CenterB().Db.Queryable<JH_Auth_User_Center>().Where(strWhere).Select("MsgType,MsgModeID,MsgContent,CRDate,UserFrom,isRead,ID,wxLink,MsgLink").OrderBy(it => it.CRDate).ToPageList(page, pagecount, ref total);

            msg.Result = dt;
            msg.Result1 = total;

        }

        //是否有未读消息
        public void HASREADMSG(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string SQL = string.Format("select count(1) from JH_Auth_User_Center where ComId={0} and UserTO='{1}' and isRead=0 ", UserInfo.User.ComId, UserInfo.User.UserName);
            object ss2 = new JH_Auth_User_CenterB().ExsSclarSql(SQL);
            if (ss2 != null)
            {
                msg.Result = "1";  //1:标记有未读消息
            }
        }
        /// <summary>
        /// 更新读取状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void UPDTEREADSTATES(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string strSql = "";
                string status = context["s"] != null ? context["s"].ToString() : "1";

                strSql = string.Format("UPDATE JH_Auth_User_Center set isRead='{0}' where ID in ({1}) ", status, P1);

                new JH_Auth_User_CenterB().ExsSql(strSql);
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }

        /// <summary>
        /// 根据消息类别设置消息状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void UPMSGSTATE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                string strSql = "";
                strSql = string.Format("UPDATE JH_Auth_User_Center set isRead='1' where MsgModelID = '{0}' AND UserTO='{1}' ", P1, UserInfo.User.UserName);
                new JH_Auth_User_CenterB().ExsSql(strSql);
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }
        /// <summary>
        /// 获取消息中心类型
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETUSERCENTERTYPE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = string.Empty;
            if (P1 != "")
            {
                string msgTypes = System.Web.HttpUtility.UrlDecode(P1);
                strWhere += string.Format(" and MsgModeID in ('{0}')", msgTypes.Replace(",", "','"));
            }
            string strSql = string.Format("SELECT  DISTINCT MsgType from JH_Auth_User_Center where ComId={0} and  userTo='{1}' " + strWhere, UserInfo.User.ComId, UserInfo.User.UserName);
            msg.Result = new JH_Auth_User_CenterB().GetDTByCommand(strSql);
        }
        //获取详细详情
        public void GETXXZXMODEL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_User_Center userCenter = new JH_Auth_User_CenterB().GetEntity(d => d.ID == Id && d.ComId == UserInfo.User.ComId);
            msg.Result = userCenter;
        }

        //删除消息中心消息
        public void DELXXZX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {
                if (P1 != "")
                {
                    string strSql = string.Format("Delete JH_Auth_User_Center where ID in ({0}) ", P1);
                    new JH_Auth_User_CenterB().ExsSql(strSql);
                }
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
        }
        public void GETXXZXMODELINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format(@"SELECT DISTINCT MsgType,MsgModeID,SUM( case when isRead=0 then 1 else 0 END) num ,MAX(CRDate) CRDate 
                                      from JH_Auth_User_Center where ComId={0}  and UserTO='{1}' group by MsgType,MsgModeID order by CRDate DESC", UserInfo.User.ComId, UserInfo.User.UserName);
            DataTable dt = new JH_Auth_User_CenterB().GetDTByCommand(strSql);
            dt.Columns.Add("NewXX", Type.GetType("System.Object"));
            foreach (DataRow row in dt.Rows)
            {
                string strSql2 = "SELECT  * from JH_Auth_User_Center where ComId=" + UserInfo.User.ComId + "  and   UserTO='" + UserInfo.User.UserName + "' and MsgType='" + row["MsgType"] + "' ";
                if (row["num"].ToString() != "0")
                {
                    row["NewXX"] = new JH_Auth_User_CenterB().GetDTByCommand(strSql2 + " and isRead=0 order by CRDate DESC").SplitDataTable(1, 1);
                }
                else
                {
                    row["NewXX"] = new JH_Auth_User_CenterB().GetDTByCommand(strSql2 + " and isRead=1 order by CRDate DESC").SplitDataTable(1, 1);
                }
            }
            msg.Result = dt.OrderBy(" num desc");
        }
        #endregion


        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETUSERBYUSERNAME(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //如果获取当前用户信息
            msg.Result = UserInfo;

        }


        public void GETUSERBYUSERNAMEOLD(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //已经弃用
            msg.Result = new JH_Auth_UserB().GetUserInfo(UserInfo.User.ComId.Value, P1);

        }






        #region 字典管理
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void INITZIDIAN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //如果获取当前用户信息，直接返回，否则按用户名查找
            msg.Result = new JH_Auth_ZiDianB().CurrentDb.AsQueryable().WhereIF(P1 != "", it => P1.Split(',').Contains(it.Class) || P1.Split(',').Contains(it.Remark1)).ToList().Select(D => new { ClassName = D.Class, Remark1 = D.Remark1 }).Distinct();

        }





        /// <summary>
        /// 获取字典数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETZIDIAN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //如果获取当前用户信息，直接返回，否则按用户名查找
            msg.Result = new JH_Auth_ZiDianB().GetZDList().Where(d => d.Class == P1 || d.Remark1 == P1).OrderBy(D => D.Remark2);
        }

        /// <summary>
        /// 修改字典状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETZIDIAN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //0是彻底删除,1是启用,2是禁用
            int[] ids = P1.SplitTOInt(',');
            if (P2 == "0")
            {
                new JH_Auth_ZiDianB().Db.Deleteable<JH_Auth_ZiDian>().Where(d => ids.Contains(d.ID)).ExecuteCommand();
            }
            else if (P2 == "1")
            {
                new JH_Auth_ZiDianB().Db.Updateable<JH_Auth_ZiDian>().SetColumns(it => it.Remark == "启用").Where(d => ids.Contains(d.ID)).ExecuteCommand();
            }
            else
            {
                new JH_Auth_ZiDianB().Db.Updateable<JH_Auth_ZiDian>().SetColumns(it => it.Remark == "禁用").Where(d => ids.Contains(d.ID)).ExecuteCommand();
            }
            new JH_Auth_ZiDianB().ClearZDCathe();

        }

        /// <summary>
        /// 编辑字典数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SAVETYPEMODEL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_ZiDian zidian = JsonConvert.DeserializeObject<JH_Auth_ZiDian>(P1);
            if (zidian.TypeName.Length > 72)
            {
                msg.ErrorMsg = "分类名称建议不超过20个字!";
                return;
            }

            if (zidian.ID == 0)
            {
                List<JH_Auth_ZiDian> zidiannew = new JH_Auth_ZiDianB().GetEntities(d => d.TypeNO == zidian.TypeNO && d.Class == zidian.Class && d.ID != zidian.ID).ToList();
                if (zidiannew.Count > 0)
                {
                    msg.ErrorMsg = "此分类已存在";
                    return;
                }
                zidian.ComId = UserInfo.User.ComId;
                zidian.Remark = "启用";
                zidian.CRDate = DateTime.Now;
                zidian.CRUser = UserInfo.User.UserName;
                new JH_Auth_ZiDianB().Insert(zidian);
            }
            else
            {
                new JH_Auth_ZiDianB().Update(zidian);
            }
            new JH_Auth_ZiDianB().ClearZDCathe();
            msg.Result = zidian;
        }
        #endregion

        #region 微信使用

        /// <summary>
        /// 获取部门下的列表
        /// </summary>
        /// <param name="wx"></param>
        /// <param name="listALL"></param>
        public void GetNextWxUser(WXUserBR wx, List<JH_Auth_Branch> listALL)
        {
            var list = (from p in listALL
                        where p.DeptRoot == wx.DeptCode
                        orderby p.DeptShort
                        select new WXUserBR { DeptCode = p.DeptCode, DeptName = p.DeptName }).ToList();

            wx.SubDept = list;
            foreach (var v in list)
            {
                var users = new JH_Auth_UserB().GetEntities(d => d.BranchCode == v.DeptCode && d.IsUse == "Y").ToList().Select(d => new { d.ID, d.UserName, d.UserRealName, d.telphone, d.mobphone, d.UserGW });
                v.DeptUser = users;
                v.DeptUserNum = users.Count();
                wx.DeptUserNum = wx.DeptUserNum + users.Count();
                GetNextWxUser(v, listALL);
            }
        }

        /// <summary>
        /// 初始化移动端
        /// </summary>
        public void WXINIT(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtUsers = new JH_Auth_UserB().GetDTByCommand(" SELECT UserName,UserRealName,mobphone FROM JH_Auth_User where ComId='" + UserInfo.User.ComId + "'");
            //获取选择用户需要的HTML和转化用户名需要的json数据
            msg.Result = dtUsers;
            msg.Result2 = UserInfo.User.UserName + "," + UserInfo.User.UserRealName + "," + UserInfo.User.BranchCode + "," + UserInfo.BranchInfo.DeptName;
            msg.Result3 = UserInfo.QYinfo.FileServerUrl;
            msg.Result4 = UserInfo.QYinfo.QYCode;

        }


        public void GETUSERINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = UserInfo;
        }

        /// <summary>
        /// 搜索关键字对应的用户和部门
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSEARCHINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new JH_Auth_BranchB().GetEntities(D => D.DeptName.Contains(P1));
            msg.Result1 = new JH_Auth_BranchB().GetEntities(D => D.DeptName.Contains(P1));

        }





        #endregion





        #region 菜单应用

        //获取应用菜单及接口
        public void GETFUNCTION(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("select JH_Auth_Model.* from  JH_Auth_Model WHERE JH_Auth_Model.ComId='0'  and JH_Auth_Model.ModelStatus='0' and JH_Auth_Model.PModelCode<>'' ORDER by ModelType");
            DataTable dt = new JH_Auth_ModelB().GetDTByCommand(strSql);
            dt.Columns.Add("FunData", Type.GetType("System.Object"));
            DataTable dtRoleFun = new JH_Auth_RoleFunB().GetDTByCommand(@"SELECT DISTINCT fun.*,rolefun.ActionCode RoleFun,rolefun.FunCode   from JH_Auth_Function fun left join JH_Auth_RoleFun rolefun on fun.ID=rolefun.FunCode and rolefun.ComId=" + UserInfo.User.ComId + " and rolefun.RoleCode= " + P1 + " Where  fun.ComId=0 or fun.ComId=" + UserInfo.User.ComId + " ORDER BY fun.FunOrder ");
            int roleId = 0;
            int.TryParse(P1, out roleId);
            JH_Auth_Role roleModel = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == roleId && d.ComId == UserInfo.User.ComId);
            string isinit = "N";//是否需要默认加载权限
            if (roleModel.isSysRole == "Y")
            {
                DataRow[] roleFun = dtRoleFun.Select(" RoleFun is not null");
                isinit = roleFun.Count() > 0 ? "N" : "Y";//>0已分配过权限，==0未分配权限
            }

            foreach (DataRow row in dt.Rows)
            {
                int modelId = int.Parse(row["ID"].ToString());
                row["FunData"] = dtRoleFun.FilterTable(" ModelID =" + modelId);
            }
            msg.Result = dt;
            msg.Result1 = isinit;
        }
        //添加角色接口权限
        public void ADDROLEFUN(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int roleCode = int.Parse(P1);
            //删除之前设置的接口权限
            new JH_Auth_RoleFunB().Delete(d => d.ComId == UserInfo.User.ComId && d.RoleCode == roleCode);
            //添加要设置的接口权限
            List<JH_Auth_RoleFun> roleFunList = JsonConvert.DeserializeObject<List<JH_Auth_RoleFun>>(P2);
            foreach (JH_Auth_RoleFun fun in roleFunList)
            {
                fun.ComId = UserInfo.User.ComId;
                new JH_Auth_RoleFunB().Insert(fun);
            }
        }
        //获取应用二级菜单
        public void GETFUNCTIONDATE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int modelId = int.Parse(P1);
            msg.Result = new JH_Auth_FunctionB().GetEntities(d => d.ModelID == modelId);
        }
        public void DELFUNDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            new JH_Auth_FunctionB().Delete(d => d.ID == Id);
        }

        //获取二级菜单详细
        public void GETFUNCTIONMODEL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            msg.Result = new JH_Auth_FunctionB().GetEntity(d => d.ID == Id);
        }  //初始化系统菜单数据


        //设置列表页参数
        public void SETLISTPAR(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_Function model = new JH_Auth_FunctionB().GetEntity(d => d.ID == Id);
            model.ExtData = P2;
            model.ActionData = context.Request("ActionData") ?? "";
            new JH_Auth_FunctionB().Update(model);
        }

        /// <summary>
        /// 获取列表模板数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETLISTTEMP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_Function model = new JH_Auth_FunctionB().GetEntity(d => d.ID == Id);
            msg.Result = model;
        }




        #endregion

        #region 用户自定义分组
        //获取自定义列表
        public void GETUSERGROUP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new JH_Auth_UserCustomDataB().GetEntities(d => (d.UserName == UserInfo.User.UserName || d.Remark == "SHARE") && d.ComId == UserInfo.User.ComId && d.DataType == P1);
            if (P1 == "DXGL")
            {
                string sql = string.Format("SELECT Distinct DataContent1 FROM JH_Auth_UserCustomData WHERE dataType='DXGL' AND UserName = '{0}' AND ComId={1} GROUP BY DataContent1 ORDER BY DataContent1 DESC", UserInfo.User.UserName, UserInfo.User.ComId);
                msg.Result1 = new JH_Auth_UserCustomDataB().GetDTByCommand(sql);
            }
        }


        /// <summary>
        /// 添加自定义配置项
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDUSERGROUP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWY = context["ISWY"] != null ? context["ISWY"].ToString() : "";
            string DataType = context["DataType"] != null ? context["DataType"].ToString() : "";
            if (strWY == "Y")
            {
                //先删除再新增,保证唯一
                new JH_Auth_UserCustomDataB().Delete(D => D.CRUser == UserInfo.User.UserName && D.DataType == DataType);
            }
            JH_Auth_UserCustomData customData = new JH_Auth_UserCustomData();
            customData.ComId = UserInfo.User.ComId;
            customData.CRDate = DateTime.Now;
            customData.CRUser = UserInfo.User.UserName;
            customData.DataContent = P1;
            customData.DataContent1 = P2.Trim();
            customData.DataType = DataType;
            customData.Remark = "";
            if (UserInfo.UserRoleCode.Contains("1218"))
            {
                customData.Remark = "SHARE";
            }
            customData.UserName = UserInfo.User.UserName;
            new JH_Auth_UserCustomDataB().Insert(customData);
            msg.Result = customData;





        }
        //根据组id获取用户列表
        public void GETUSERLISTBYGROUP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            JH_Auth_UserCustomData customData = new JH_Auth_UserCustomDataB().GetEntity(d => d.ID == Id);
            string[] usernames = customData.DataContent1.Split(',');
            msg.Result = new JH_Auth_UserB().GetEntities(d => usernames.Contains(d.UserName) && d.ComId == UserInfo.User.ComId);

        }
        //删除用户自定义分组 ，短信模板
        public void DELUSERGROUP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            new JH_Auth_UserCustomDataB().Delete(d => d.ID == Id);
        }
        //删除通讯录分类分组
        public void DELUSERGROUPTXL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int Id = int.Parse(P1);
            DataTable dt = new JH_Auth_UserCustomDataB().GetDTByCommand("SELECT * from SZHL_TXL where TagName=" + Id + " and ComId=" + UserInfo.User.ComId);
            if (dt.Rows.Count == 0)
            {
                new JH_Auth_UserCustomDataB().Delete(d => d.ID == Id);
            }
            else
            {
                msg.ErrorMsg = "请先删除此分类下的人员信息";
            }
        }


        #endregion

        #region 系统日志

        /// <summary>
        /// 获取日志数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETXTRZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int pageNo = int.Parse(context.Request("pageNo") ?? "1");
            int pageSize = int.Parse(context.Request("pageSize") ?? "200");
            string strQDATA = context.Request("qdata");
            string strQWhere = "";
            if (P1 != "")
            {
                strQWhere += string.Format(" 1=1  and LogType IN ('{0}')", P1);  //多个类型逗号隔开传过来
            }
            if (!string.IsNullOrEmpty(strQDATA))
            {
                JArray querylist = JsonConvert.DeserializeObject(strQDATA) as JArray;
                strQWhere = strQWhere + CommonHelp.GetQuertSQL(querylist);
            }



            int total = 0;
            var dt = new JH_Auth_LogB().Db.Queryable<JH_Auth_Log>().Where(strQWhere).Select(f => new
            {
                f.ID,
                f.LogType,
                f.LogContent,
                f.CRUser,
                f.CRDate,
                f.IP,
                f.netcode,
                f.Remark,
                f.Remark1,

            }).OrderBy(it => it.CRDate, OrderByType.Desc).ToPageList(pageNo, pageSize, ref total);
            msg.Result = dt;
            msg.DataLength = total;

        }
        public void DELXTRZ(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = "1=1";
            if (P1 != "" && P2 != "")
            {
                switch (P1)
                {
                    case "1": strWhere += " and ID ='" + P2 + "'"; break;
                    case "2": strWhere += " and ID in(" + P2 + ")"; break;
                }
                try
                {
                    new JH_Auth_LogB().ExsSql(" delete from JH_Auth_Log where " + strWhere);
                }
                catch (Exception ex)
                {
                    msg.ErrorMsg = ex.Message;
                }
            }
            else
            {
                msg.ErrorMsg = "删除失败";
            }
        }
        #endregion



        #region 缓存管理
        /// <summary>
        /// 获取缓存数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETALLCATHE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            msg.Result = CacheHelp.GetAll();

        }


        /// <summary>
        /// 删除系统缓存
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DELCATHE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            CacheHelp.Remove(P1);
        }
        #endregion




        #region 设置常用菜单显示
        //设置手机APP，PC首页菜单显示应用
        public void SETAPPINDEX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string type = context["type"] != null ? context["type"].ToString() : "APPINDEX";//默认为APP首页显示菜单，传值为PC首页的快捷方式按钮

            foreach (string str in P1.Split(','))
            {
                string[] content = str.Split(':');
                string modelCode = content[0];
                //判断是否存在菜单的数据，存在只更新状态，不存在添加
                JH_Auth_UserCustomData customData = new JH_Auth_UserCustomDataB().GetEntity(d => d.UserName == UserInfo.User.UserName && d.DataType == type && d.ComId == UserInfo.User.ComId && d.DataContent == modelCode);
                string status = content[1];
                if (customData != null)
                {
                    customData.DataContent1 = status;
                    new JH_Auth_UserCustomDataB().Update(customData);
                }
                else
                {
                    customData = new JH_Auth_UserCustomData();
                    customData.ComId = UserInfo.User.ComId;
                    customData.UserName = UserInfo.User.UserName;
                    customData.CRDate = DateTime.Now;
                    customData.CRUser = UserInfo.User.UserName;
                    customData.DataContent = modelCode;
                    customData.DataContent1 = status;
                    customData.DataType = type;
                    new JH_Auth_UserCustomDataB().Insert(customData);
                }
                if (type == "APPINDEX")
                {
                    msg.Result = customData;
                }
            }


        }
        #endregion

        #region 设置部门人员的查看角色
        public void SETROLEQX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            int roleCode = int.Parse(P2);
            JH_Auth_Role role = new JH_Auth_RoleB().GetEntity(d => d.RoleCode == roleCode);
            role.RoleQX = P1;
            role.IsHasQX = context["qx"] != null ? context["qx"].ToString() : "1";
            new JH_Auth_RoleB().Update(role);
            msg.Result = role;
        }
        #endregion



        #region 获取数及容量使用情况
        public void GETDXANDSPACE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            decimal DXCost = decimal.Parse(CommonHelp.GetConfig("DXCost", "1"));
            //已发送短信总数量
            msg.Result = new SZHL_DXGLB().GetEntities(d => d.ComId == UserInfo.User.ComId.Value).Count();
            msg.Result1 = (int)(UserInfo.QYinfo.AccountMoney.Value / DXCost);
            msg.Result2 = UserInfo.QYinfo.QySpace / 10000000000;

            decimal Size = 0;

            msg.Result3 = Size;
        }
        #endregion

















        #region 常用菜单设置
        /// <summary>
        /// 获取权限应用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">获取权限应用的类型,m:移动,否则就是pc</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETINDEXMENUNEW(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            DataTable dtModel = new JH_Auth_ModelB().GETMenuList(UserInfo);
            dtModel.Columns.Add("FunData", typeof(DataTable));
            if (dtModel != null && dtModel.Rows.Count > 0)
            {
                foreach (DataRow row in dtModel.Rows)
                {
                    row["FunData"] = new JH_Auth_RoleB().GetModelFun(UserInfo.User.ComId.Value, UserInfo.UserRoleCode, row["ID"].ToString(), P1);
                }
            }
            msg.Result = dtModel;
            msg.Result2 = UserInfo.User.isSupAdmin;
            msg.Result3 = new JH_Auth_FunctionB().GetEntities(D => D.PageName == "问题反馈").FirstOrDefault();
            msg.Result4 = UserInfo.User;

        }
        #endregion




        #region 应用管理
        public void GETYYDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = "";
            if (P1 != "")
            {
                strWhere = " AND (ModelName like '%" + P1 + "%'OR ModelType like '%" + P1 + "%') ";
            }
            if (string.IsNullOrEmpty(P2))
            {
                P2 = "1";
            }
            else
            {
                P2 = "0";
            }
            string strSQL = "SELECT * FROM JH_Auth_Model WHERE isSys='" + P2 + "' AND  1=1" + strWhere + " ORDER BY ModelType";
            DataTable dt = new JH_Auth_ModelB().GetDTByCommand(strSQL);
            msg.Result = dt;
        }


        public void DELYY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int intYYID = int.Parse(P1);
            new JH_Auth_ModelB().Delete(d => d.ID == intYYID);
            new JH_Auth_FunctionB().Delete(d => d.ModelID == intYYID);
        }

        public void GETYY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int intYYID = int.Parse(P1);
            JH_Auth_Model model = new JH_Auth_ModelB().GetEntity(d => d.ID == intYYID);
            msg.Result = model;
            msg.Result1 = new JH_Auth_FunctionB().GetEntities(d => d.ModelID == intYYID && d.IsInit != "m");
            msg.Result2 = new JH_Auth_FunctionB().GetEntities(d => d.ModelID == intYYID && d.IsInit == "m");


        }



        /// <summary>
        /// 新增应用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDYY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Model MODELO = JsonConvert.DeserializeObject<JH_Auth_Model>(P1);


            if (new JH_Auth_ModelB().GetEntities(D => D.ModelCode == MODELO.ModelCode || D.ModelName == MODELO.ModelName).Count() > 0)
            {
                msg.ErrorMsg = "不能添加重复的应用代码或应用名称!";
                return;
            }
            else
            {
                JH_Auth_Model MODEL = new JH_Auth_Model();
                MODEL.ModelCode = MODELO.ModelCode;
                MODEL.PModelCode = MODELO.PModelCode;
                MODEL.ModelType = MODELO.ModelType;
                MODEL.ModelName = MODELO.ModelName;
                MODEL.ORDERID = MODELO.ORDERID;
                MODEL.WXUrl = MODELO.WXUrl;
                MODEL.IsSys = MODELO.IsSys;
                MODEL.ModelStatus = "0";
                MODEL.IsSQ = "1";
                MODEL.CRDate = DateTime.Now;
                MODEL.CRUser = UserInfo.User.UserName;
                MODEL.ComId = 0;
                MODEL.AppType = "1";
                MODEL.IsKJFS = 0;
                new JH_Auth_ModelB().Insert(MODEL);
            }



        }


        /// <summary>
        /// 编辑保存应用数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SAVEYY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Model MODEL = JsonConvert.DeserializeObject<JH_Auth_Model>(P1);
            new JH_Auth_ModelB().Update(MODEL);


            List<JH_Auth_Function> FUNLIST = JsonConvert.DeserializeObject<List<JH_Auth_Function>>(P2);
            FUNLIST.ForEach(d => d.CRDate = DateTime.Now);
            FUNLIST.ForEach(d => d.CRUser = UserInfo.User.UserName);
            FUNLIST.ForEach(d => d.ComId = 0);
            FUNLIST.ForEach(d => d.ModelID = MODEL.ID);
            // FUNLIST.ForEach(d => d.ActionData = "[]");
            foreach (JH_Auth_Function item in FUNLIST)
            {
                if (item.ID == 0)
                {
                    //新增
                    if (string.IsNullOrEmpty(item.ActionData))
                    {
                        item.ActionData = "[]";
                    }
                    new JH_Auth_FunctionB().Insert(item);
                }
                else
                {
                    new JH_Auth_FunctionB().Update(item);
                    //更新
                }
            }

            string MENUData = context["MENU"] != null ? context["MENU"].ToString() : "";
            if (MENUData.Trim(',') != "")
            {
                List<JH_Auth_Function> MEUNLIST = JsonConvert.DeserializeObject<List<JH_Auth_Function>>(MENUData);
                MEUNLIST.ForEach(d => d.CRDate = DateTime.Now);
                MEUNLIST.ForEach(d => d.CRUser = UserInfo.User.UserName);
                MEUNLIST.ForEach(d => d.ComId = 0);
                MEUNLIST.ForEach(d => d.ModelID = MODEL.ID);
                // FUNLIST.ForEach(d => d.ActionData = "[]");
                foreach (JH_Auth_Function item in MEUNLIST)
                {
                    if (item.ID == 0)
                    {
                        new JH_Auth_FunctionB().Insert(item);
                    }
                    else
                    {
                        new JH_Auth_FunctionB().Update(item);
                        //更新
                    }
                }

            }


            string delid = context["DIDS"] != null ? context["DIDS"].ToString() : "";
            if (delid.Trim(',') != "")
            {
                new JH_Auth_FunctionB().ExsSclarSql("delete FROM JH_Auth_Function where id in ('" + delid.ToFormatLike(',') + "')");
                //删除
            }


        }






        /// <summary>
        /// 导出应用JSON脚本
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DCYYJB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            YYModel yy = new YYModel();
            yy.Model = new JH_Auth_ModelB().GetEntities(D => D.ID.ToString() == P1).FirstOrDefault();
            //菜单
            yy.funs = new JH_Auth_FunctionB().GetEntities(D => D.ModelID == yy.Model.ID).ToList();


            //表单
            yy.pds = new Yan_WF_PDB().GetEntities(D => D.gltable == yy.Model.ID.ToString()).ToList();

            //数据集
            yy.dset = new BI_DB_SetB().GetEntities(D => D.Type == yy.Model.ID.ToString()).ToList();
            string[] strdsids = yy.dset.Select(p => $"{p.ID}").ToArray();
            yy.dims = new BI_DB_DimB().GetEntities(D => strdsids.Contains(D.STID.ToString())).ToList();

            //table
            yy.tab = new BI_DB_TableB().GetEntities(D => D.ModelID == yy.Model.ID).ToList();
            string[] strtableids = yy.tab.Select(p => $"{p.ID}").ToArray();
            yy.fids = new BI_DB_TablefiledB().GetEntities(D => strtableids.Contains(D.TableID.ToString())).ToList();


            //仪表盘
            yy.ybps = new BI_DB_YBPB().GetEntities(D => D.DimID == yy.Model.ID).ToList();

            msg.Result = yy;
        }


        /// <summary>
        /// 导入应用JSON脚本
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void DRYYJB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //处理应用


            var DB = new JH_Auth_ModelB().Db;
            YYModel yymodel = JsonConvert.DeserializeObject<YYModel>(P1);
            if (new JH_Auth_ModelB().GetEntities(d => d.ModelCode == yymodel.Model.ModelCode).Count() > 0)
            {
                msg.ErrorMsg = "不能导入已存在的应用代码ModelCode!";
                return;
            }
            try
            {
                //DB.BeginTran();

                yymodel.Model.ID = 0;
                DB.Insertable<JH_Auth_Model>(yymodel.Model).ExecuteReturnEntity();

                //处理表单
                yymodel.pds.ForEach(d =>
                {
                    d.gltable = yymodel.Model.ID.ToString();
                    d.ID = 0;

                });
                DB.Insertable<Yan_WF_PD>(yymodel.pds).ExecuteReturnEntity();

                //处理仪表盘
                yymodel.ybps.ForEach(d =>
                {
                    d.DimID = yymodel.Model.ID;
                    d.ID = 0;

                });
                DB.Insertable<BI_DB_YBP>(yymodel.ybps).ExecuteReturnEntity();


                //处理功能菜单
                yymodel.funs.ForEach(d =>
                {
                    d.ModelID = yymodel.Model.ID;
                    d.ID = 0;

                });
                DB.Insertable<JH_Auth_Function>(yymodel.funs).ExecuteReturnEntity();



                yymodel.dset.ForEach(d =>
                {

                    string strOLDID = d.ID.ToString();
                    List<BI_DB_Dim> dimdata = yymodel.dims.Where(m => m.STID.ToString() == strOLDID).ToList();

                    d.Type = yymodel.Model.ID.ToString();
                    d.ID = 0;

                    DB.Insertable<BI_DB_Set>(d).ExecuteReturnEntity();

                    dimdata.ForEach(p =>
                    {
                        p.ID = 0;
                        p.STID = d.ID;
                    });
                    DB.Insertable<BI_DB_Dim>(dimdata).ExecuteReturnEntity();

                });

                yymodel.tab.ForEach(d =>
                {
                    string strOLDID = d.ID.ToString();
                    List<BI_DB_Tablefiled> fiddata = yymodel.fids.Where(m => m.TableID.ToString() == strOLDID).ToList();

                    d.ModelID = yymodel.Model.ID;
                    d.ID = 0;
                    DB.Insertable<BI_DB_Table>(d).ExecuteReturnEntity();

                    List<DbColumnInfo> ListFild = new List<DbColumnInfo>();

                    fiddata.ForEach(p =>
                    {
                        p.ID = 0;
                        p.TableID = d.ID;
                        DbColumnInfo colID = new DbColumnInfo();
                        colID.DataType = p.DataType;
                        colID.DbColumnName = p.DbColumnName;
                        colID.IsPrimarykey = p.IsPrimarykey == "0" ? true : false;
                        colID.IsIdentity = p.IsIdentity == "0" ? true : false;
                        colID.IsNullable = p.IsNullable == "0" ? true : false;
                        colID.Length = p.Length;
                        colID.ColumnDescription = p.ColumnDescription;
                        colID.TableName = d.TableName;

                        ListFild.Add(colID);
                    });
                    DB.Insertable<BI_DB_Tablefiled>(fiddata).ExecuteReturnEntity();
                    DB.DbMaintenance.CreateTable(d.TableName, ListFild);


                });
                //DB.CommitTran();
            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message.ToString();
                //DB.CommitTran();
            }
        }
        #endregion



        #region 自定义应用
        public void GETINITDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            var tabledata = new BI_DB_TableB().GetEntities(D => D.ModelID.ToString() == P1);
            msg.Result = tabledata;
            //应用表数据

            var dataset = new BI_DB_SetB().GetEntities(D => D.Type == P1);
            msg.Result1 = dataset;
            //应用数据集数据
            var formdata = new Yan_WF_PDB().GetEntities(D => D.gltable == P1);
            msg.Result2 = formdata;
            //应用表单数据

            var ybdata = new BI_DB_YBPB().GetEntities(D => D.DimID.ToString() == P1);
            msg.Result3 = ybdata;
            //应用报表数据


            var sjydata = new BI_DB_SourceB().GetALLEntities();
            msg.Result4 = sjydata;
            //应用报表数据
        }




        public void SENDXXFB(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            Yan_WF_PI PI = new Yan_WF_PIB().GetEntity(D => D.ID.ToString() == P1);


            DataTable dtGG = new Yan_WF_PIB().GetDTByCommand("SELECT * FROM qj_sys_tzgg WHERE  intProcessStanceid='" + PI.ID.ToString() + "'");
            if (dtGG.Rows.Count > 0)
            {
                string touser = dtGG.Rows[0]["jsjs"].ToString();
                string dataid = dtGG.Rows[0]["ID"].ToString();
                string title = dtGG.Rows[0]["title"].ToString();
                JH_Auth_UserB.UserInfo UserInfofrom = new JH_Auth_UserB().GetUserInfo(UserInfo.User.ComId.Value, PI.CRUser);
                new JH_Auth_User_CenterB().SendMsg(UserInfo, "XTGL", title, dataid, touser, "/MobWeb/UI_XW.html?id=" + dataid);
            }


            //应用报表数据
        }

        #endregion







        #region 导入用户
        public void SAVEIMPORTUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string branchMsg = "", branchErrorMsg = "", userMsg = "";
            int i = 0, j = 0;
            DataTable dt = new DataTable();
            dt = JsonConvert.DeserializeObject<DataTable>(P1);
            dt.Columns.Add("BranchCode");
            JH_Auth_Branch branchroot = new JH_Auth_BranchB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.DeptRoot == -1);


            foreach (DataRow row in dt.Rows)
            {
                int bRootid = branchroot.DeptCode;
                string branchName = row[4].ToString();
                if (branchName != "")
                {
                    string[] branchNames = branchName.Split('/');
                    string strBranch = branchNames[0];
                    JH_Auth_Branch branchModel = new JH_Auth_BranchB().GetEntity(d => d.DeptName == strBranch && d.ComId == UserInfo.User.ComId);
                    if (branchModel == null)
                    {
                        branchModel = new JH_Auth_Branch();
                        branchModel.DeptName = branchNames[0];
                        branchModel.DeptDesc = branchNames[0];
                        branchModel.ComId = UserInfo.User.ComId;
                        branchModel.DeptRoot = bRootid;
                        branchModel.CRDate = DateTime.Now;
                        branchModel.CRUser = UserInfo.User.UserName;
                        int sum = new JH_Auth_BranchB().Db.Queryable<JH_Auth_Branch>().Max(it => it.DeptCode);
                        branchModel.DeptCode = sum + 1;

                        new JH_Auth_BranchB().Insert(branchModel);
                        branchModel.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, branchModel.DeptRoot) + branchModel.DeptCode;
                        new JH_Auth_BranchB().Update(branchModel);
                    }
                }
            }


            int rowIndex = 0;
            foreach (DataRow row in dt.Rows)
            {
                rowIndex++;
                string branchName = row[4].ToString();
                if (branchName != "")
                {
                    string[] branchNames = branchName.Split('/');
                    string strPBranch = branchNames[0];

                    JH_Auth_Branch PbranchModel = new JH_Auth_BranchB().GetEntity(d => d.DeptName == strPBranch && d.ComId == UserInfo.User.ComId);
                    int bRootid = PbranchModel.DeptCode;
                    for (int l = 1; l < branchNames.Length; l++)
                    {
                        string strBranch = branchNames[1];
                        JH_Auth_Branch branchModel = new JH_Auth_BranchB().GetEntity(d => d.DeptName == strBranch && d.DeptRoot == PbranchModel.DeptCode && d.ComId == UserInfo.User.ComId);
                        if (branchModel != null)
                        {
                            bRootid = branchModel.DeptCode;
                            if (l == branchNames.Length - 1)
                            {
                                row["BranchCode"] = branchModel.DeptCode;
                            }
                        }
                        else
                        {
                            branchModel = new JH_Auth_Branch();
                            branchModel.DeptName = strBranch;
                            branchModel.DeptDesc = strBranch;
                            branchModel.ComId = UserInfo.User.ComId;
                            branchModel.DeptRoot = bRootid;
                            branchModel.CRDate = DateTime.Now;
                            branchModel.CRUser = UserInfo.User.UserName;
                            int sum = new JH_Auth_BranchB().Db.Queryable<JH_Auth_Branch>().Max(it => it.DeptCode);
                            branchModel.DeptCode = sum + 1;

                            new JH_Auth_BranchB().Insert(branchModel);
                            branchModel.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, branchModel.DeptRoot) + branchModel.DeptCode;
                            new JH_Auth_BranchB().Update(branchModel);
                            try
                            {
                                bRootid = branchModel.DeptCode;
                                if (l == branchNames.Length - 1)
                                {
                                    row["BranchCode"] = branchModel.DeptCode;
                                }
                                i++;
                                branchMsg += "新增部门“" + strBranch + "”成功<br/>";
                            }
                            catch (Exception ex)
                            {

                                branchErrorMsg += "部门：" + strBranch + "失败 " + msg.ErrorMsg + "<br/>";
                            }
                        }
                    }
                    string userName = row[2].ToString();
                    JH_Auth_User userModel = new JH_Auth_UserB().GetEntity(d => d.UserName == userName && d.ComId == UserInfo.User.ComId);
                    if (userModel == null)
                    {
                        JH_Auth_User userNew = new JH_Auth_User();
                        if (row["BranchCode"].ToString() != "")
                        {
                            int tempcode = int.Parse(row["BranchCode"].ToString());
                            JH_Auth_Branch branchTemp = new JH_Auth_BranchB().GetEntity(d => d.DeptCode == tempcode && d.ComId == UserInfo.User.ComId);

                            userNew.BranchCode = branchTemp.DeptCode;
                            userNew.remark = branchTemp.Remark1.Split('-')[0];
                        }
                        else
                        {
                            userNew.BranchCode = bRootid;
                        }
                        userNew.ComId = UserInfo.User.ComId;
                        userNew.IsUse = "Y";
                        userNew.mailbox = row[3].ToString();
                        userNew.mobphone = row[7].ToString();
                        userNew.RoomCode = row[8].ToString();
                        userNew.Sex = row[1].ToString();
                        userNew.telphone = row[10].ToString();
                        DateTime result;
                        if (DateTime.TryParse(row[11].ToString(), out result))
                        {
                            userNew.Birthday = result;
                        }

                        userNew.UserGW = row[6].ToString();
                        userNew.UserName = row[2].ToString();
                        userNew.UserRealName = row[0].ToString();
                        userNew.zhiwu = row[5].ToString() == "" ? "员工" : row[5].ToString();
                        userNew.UserPass = CommonHelp.GetMD5(P2);
                        userNew.CRDate = DateTime.Now;
                        userNew.CRUser = UserInfo.User.UserName;

                        if (!string.IsNullOrEmpty(row[9].ToString()))
                        {
                            int orderNum = 0;
                            int.TryParse(row[9].ToString(), out orderNum);
                            userNew.UserOrder = orderNum;

                        }
                        try
                        {
                            msg.ErrorMsg = "";
                            if (string.IsNullOrEmpty(userNew.UserName))
                            {
                                msg.ErrorMsg = "用户名必填";
                            }
                            //Regex regexPhone = new Regex("^0?1[3|4|5|8|7][0-9]\\d{8}$");
                            //if (!regexPhone.IsMatch(userNew.UserName))
                            //{
                            //    msg.ErrorMsg = "用户名必须为手机号";
                            //}
                            if (string.IsNullOrEmpty(userNew.mobphone))
                            {
                                msg.ErrorMsg = "手机号必填";
                            }
                            //if (!regexPhone.IsMatch(userNew.mobphone))
                            //{
                            //    msg.ErrorMsg = "手机号填写不正确";
                            //}
                            Regex regexOrder = new Regex("^[0-9]*$");
                            if (userNew.UserOrder != null && !regexOrder.IsMatch(userNew.UserOrder.ToString()))
                            {
                                msg.ErrorMsg = "序号必须是数字";
                            }
                            if (msg.ErrorMsg != "")
                            {
                                userMsg += "第" + rowIndex + "行" + msg.ErrorMsg + "<br/>";
                            }
                            if (msg.ErrorMsg == "")
                            {
                                new JH_Auth_UserB().Insert(userNew);
                                JH_Auth_Role role = new JH_Auth_RoleB().GetEntity(d => d.RoleName == userNew.zhiwu);
                                if (role == null)
                                {
                                    role = new JH_Auth_Role();
                                    role.PRoleCode = 0;
                                    role.RoleName = userNew.zhiwu;
                                    role.RoleDec = userNew.zhiwu;
                                    role.IsUse = "Y";
                                    role.isSysRole = "N";
                                    role.leve = 0;
                                    role.ComId = UserInfo.User.ComId;
                                    role.DisplayOrder = 0;
                                    new JH_Auth_RoleB().Insert(role);
                                }

                                var insertObjs = new List<JH_Auth_UserRole>();
                                var s9 = new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { UserName = userNew.UserName, ComId = UserInfo.User.ComId, RoleCode = role.RoleCode });
                                string isFS = context["issend"] != null ? context["issend"].ToString() : "";
                                if (isFS.ToLower() == "true")
                                {
                                    string content = string.Format("尊敬的" + userNew.UserName + "用户您好：你已被添加到" + UserInfo.QYinfo.QYName + ",账号：" + userNew.mobphone + "，密码" + P2 + ",登录请访问" + UserInfo.QYinfo.WXUrl);
                                    new SZHL_DXGLB().SendSMS(userNew.mobphone, content, userNew.ComId.Value);
                                }
                                j++;
                            }
                        }
                        catch (Exception ex)
                        {
                            userMsg += "第" + rowIndex + "行" + msg.ErrorMsg + "<br/>";
                        }

                    }
                    else
                    {

                        userMsg += "第" + rowIndex + "行" + "用户“" + row[2].ToString() + "”已存在<br/>";
                    }
                }
                else
                {
                    branchErrorMsg += "第" + rowIndex + "行所在部门必填<br/>";
                }

            }
            msg.Result = branchErrorMsg + "<br/>" + userMsg;
            msg.Result1 = "新增部门" + i + "个,新增用户" + j + "个<br/>" + branchMsg + (branchMsg == "" ? "" : "<br/>");
        }



        #endregion

        #region 获取系统首页用户数量信息


        /// <summary>
        /// 获取管理员首页数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETUSERCOUNT(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {



            string strSql = string.Format("SELECT COUNT(0) TotalUser,isnull(sum(case when isgz=1 then 1 else 0 end ),0) gzCount,isnull(sum(case when isgz=4 then 1 else 0 end ),0) wgzCount,isnull(sum(case when IsUse!='Y' then 1 else 0 end ),0) wjhCount from JH_Auth_User where ComId={0}", UserInfo.User.ComId);
            msg.Result = new JH_Auth_UserB().GetDTByCommand(strSql);
            msg.Result1 = UserInfo.QYinfo.IsUseWX;
            msg.Result2 = CommonHelp.getSysInfo();

            msg.Result3 = new JH_Auth_BranchB().GetALLEntities().Count() - 1;
            msg.Result4 = new JH_Auth_RoleB().GetALLEntities().Count();
            msg.Result5 = new JH_Auth_UserB().GetDTByCommand("SELECT TOP 3 ID, CRUserName,fbdw,DName,CRDate,title,ggtype FROM qj_sys_tzgg WHERE  ','+jsjs+',' like '%," + UserInfo.User.UserName + ",%'  ORDER BY CRDate DESC ");


            DataTable dtTEMP = new JH_Auth_UserB().GetALLEntities().Select(D => new { D.UserName, D.UserRealName }).ToDataTable();
            DataTable dtxtdt = new JH_Auth_UserB().GetDTByCommand("SELECT  TOP 8 *   FROM jh_auth_log  ORDER BY CRDate DESC");
            msg.Result6 = dtxtdt;


            msg.Result7 = new JH_Auth_LogB().GetDTByCommand("SELECT   DISTINCT CRUser   FROM jh_auth_log  WHERE CRDate BETWEEN '" + DateTime.Now.AddMinutes(-30) + "' AND  GETDATE()").Rows.Count;
            //msg.Result3 = Logc.ToString();





        }
        #endregion

        /// <summary>
        /// 同步通讯录
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">初始化密码</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>


        #region 企业号相关
        /// <summary>
        /// 将系统的组织架构同步到微信中去
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TBBRANCHUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            //判断是否启用微信后，启用部门需要同步添加微信部门
            if (UserInfo.QYinfo.IsUseWX == "Y")
            {

                #region 同步部门

                //系统部门
                List<JH_Auth_Branch> branchList = new JH_Auth_BranchB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.WXBMCode == null).ToList();

                WXHelp wx = new WXHelp(UserInfo.QYinfo);
                //微信部门
                GetDepartmentListResult bmlist = wx.WX_GetBranchList("");
                foreach (JH_Auth_Branch branch in branchList)
                {
                    List<DepartmentList> departList = bmlist.department.Where(d => d.name == branch.DeptName).ToList();
                    WorkJsonResult result = null;
                    if (departList.Count() > 0)
                    {
                        branch.WXBMCode = int.Parse(departList[0].id.ToString());
                        result = wx.WX_UpdateBranch(branch);
                    }
                    else
                    {

                        int branchWxCode = int.Parse(wx.WX_CreateBranchTB(branch).ToString());
                        branch.WXBMCode = branchWxCode;
                    }
                    new JH_Auth_BranchB().Update(branch);
                }

                #endregion

                #region 同步人员
                JH_Auth_Branch branchModel = new JH_Auth_BranchB().GetEntity(d => d.DeptRoot == -1 && d.ComId == UserInfo.User.ComId);

                GetDepartmentMemberInfoResult yg = wx.WX_GetDepartmentMemberInfo(branchModel.WXBMCode.Value);
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.ComId == UserInfo.User.ComId && d.UserName != "administrator").ToList();
                foreach (JH_Auth_User user in userList)
                {
                    if (yg.userlist.Where(d => d.name == user.UserName || d.mobile == user.mobphone).Count() > 0)
                    {
                        wx.WX_UpdateUser(user);
                    }
                    else
                    {

                        wx.WX_CreateUser(user);
                    }
                }
                #endregion
            }
        }



        /// <summary>
        /// 从企业微信同步到系统里
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TBTXL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {

                int bmcount = 0;
                int rycount = 0;
                if (P1 == "")
                {
                    msg.ErrorMsg = "请输入初始密码";
                    return;
                }
                WXHelp wx = new WXHelp(UserInfo.QYinfo);
                #region 更新部门
                GetDepartmentListResult bmlist = wx.WX_GetBranchList("");
                foreach (var wxbm in bmlist.department.OrderBy(d => d.parentid))
                {
                    var bm = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == wxbm.id);
                    if (bm == null)
                    {
                        #region 新增部门
                        JH_Auth_Branch jab = new JH_Auth_Branch();
                        jab.WXBMCode = int.Parse(wxbm.id.ToString());
                        jab.ComId = UserInfo.User.ComId;
                        jab.DeptName = wxbm.name;
                        jab.DeptDesc = wxbm.name;
                        jab.DeptShort = int.Parse(wxbm.order.ToString());

                        if (wxbm.parentid == 0)//如果是跟部门,设置其跟部门为-1
                        {
                            jab.DeptRoot = -1;
                        }
                        else
                        {
                            var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == wxbm.parentid);
                            jab.DeptRoot = bm1.DeptCode;
                            jab.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, jab.DeptRoot);
                        }

                        int sum = new JH_Auth_BranchB().Db.Queryable<JH_Auth_Branch>().Max(it => it.DeptCode);
                        jab.DeptCode = sum + 1;
                        new JH_Auth_BranchB().Insert(jab);
                        jab.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, jab.DeptRoot) + jab.DeptCode;
                        new JH_Auth_BranchB().Update(jab);


                        bmcount = bmcount + 1;
                        #endregion
                    }
                    else
                    {
                        //同步部门时放弃更新现有部门

                    }
                }
                #endregion

                #region 更新人员
                JH_Auth_Branch branchModel = new JH_Auth_BranchB().GetEntity(d => d.DeptRoot == -1 && d.ComId == UserInfo.User.ComId);

                GetDepartmentMemberInfoResult yg = wx.WX_GetDepartmentMemberInfo(branchModel.WXBMCode.Value);
                foreach (var u in yg.userlist)
                {
                    var user = new JH_Auth_UserB().GetUserByUserName(UserInfo.QYinfo.ComId, u.userid);
                    if (user == null)
                    {
                        #region 新增人员
                        JH_Auth_User jau = new JH_Auth_User();
                        jau.ComId = UserInfo.User.ComId;
                        jau.UserName = u.userid;
                        jau.UserPass = CommonHelp.GetMD5(P1);
                        jau.UserRealName = u.name;
                        jau.Sex = u.gender == 1 ? "男" : "女";
                        if (u.department.Length > 0)
                        {
                            int id = int.Parse(u.department[0].ToString());
                            var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == id);
                            jau.BranchCode = bm1.DeptCode;
                            jau.remark = bm1.Remark1.Split('-')[0];//用户得部门路径

                        }
                        jau.mailbox = u.email;
                        jau.mobphone = u.mobile;
                        jau.zhiwu = string.IsNullOrEmpty(u.position) ? "员工" : u.position;
                        jau.IsUse = "Y";

                        if (u.status == 1 || u.status == 4)
                        {
                            jau.isgz = u.status.ToString();
                        }
                        jau.txurl = u.avatar;

                        new JH_Auth_UserB().Insert(jau);

                        rycount = rycount + 1;
                        #endregion

                        //为所有人增加普通员工的权限
                        JH_Auth_Role rdefault = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.isSysRole == "Y" && p.RoleName == "员工");//找到默认角色
                        if (rdefault != null)
                        {
                            JH_Auth_UserRole jaurdefault = new JH_Auth_UserRole();
                            jaurdefault.ComId = UserInfo.User.ComId;
                            jaurdefault.RoleCode = rdefault.RoleCode;
                            jaurdefault.UserName = jau.UserName;
                            new JH_Auth_UserRoleB().Insert(jaurdefault);
                        }


                    }
                    else
                    {
                        //同步人员时放弃更新现有人员
                        #region 更新人员
                        user.UserRealName = u.name;
                        if (u.department.Length > 0)
                        {
                            int id = int.Parse(u.department[0].ToString());
                            var bm1 = new JH_Auth_BranchB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.WXBMCode == id);
                            user.BranchCode = bm1.DeptCode;
                        }
                        user.mailbox = u.email;
                        user.mobphone = u.mobile;
                        user.zhiwu = string.IsNullOrEmpty(u.position) ? "员工" : u.position;
                        user.Sex = u.gender == 1 ? "男" : "女";
                        if (u.status == 1 || u.status == 4)
                        {
                            user.IsUse = "Y";
                            user.isgz = u.status.ToString();
                        }
                        else if (u.status == 2)
                        {
                            user.IsUse = "N";
                        }
                        user.txurl = u.avatar;

                        new JH_Auth_UserB().Update(user);
                        #endregion
                    }

                    #region 更新角色(职务)
                    if (!string.IsNullOrEmpty(u.position))
                    {
                        var r = new JH_Auth_RoleB().GetEntity(p => p.ComId == UserInfo.User.ComId && p.RoleName == u.position);

                        if (r == null)
                        {
                            JH_Auth_Role jar = new JH_Auth_Role();
                            jar.ComId = UserInfo.User.ComId;
                            jar.RoleName = u.position;
                            jar.RoleDec = u.position;
                            jar.PRoleCode = 0;
                            jar.isSysRole = "N";
                            jar.IsUse = "Y";
                            jar.leve = 0;
                            jar.DisplayOrder = 0;

                            new JH_Auth_RoleB().Insert(jar);

                            JH_Auth_UserRole jaur = new JH_Auth_UserRole();
                            jaur.ComId = UserInfo.User.ComId;
                            jaur.RoleCode = jar.RoleCode;
                            jaur.UserName = u.userid;
                            new JH_Auth_UserRoleB().Insert(jaur);


                        }
                        else
                        {

                        }
                    }
                    #endregion
                }
                #endregion



                msg.Result1 = bmcount;
                msg.Result2 = rycount;


            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.ToString();
            }
        }

        //同步关注状态
        public void TBGZSTATUS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            try
            {

                JH_Auth_Branch branchModel = new JH_Auth_BranchB().GetEntity(d => d.DeptRoot == -1 && d.ComId == UserInfo.User.ComId);

                #region 同步用户关注状态
                WXHelp wx = new WXHelp(UserInfo.QYinfo);
                GetDepartmentMemberInfoResult yg = wx.WX_GetDepartmentMemberInfo(branchModel.WXBMCode.Value);

                if (yg != null && yg.userlist != null)
                {
                    foreach (var u in yg.userlist)
                    {

                        JH_Auth_User user = new JH_Auth_UserB().GetEntity(d => d.ComId == UserInfo.User.ComId && d.UserName == u.userid);

                        if (user != null && u != null && (u.status == 1 || u.status == 4))
                        {
                            user.isgz = u.status.ToString();
                            new JH_Auth_UserB().Update(user);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                msg.ErrorMsg = ex.Message;
            }
            #endregion

        }

        public void YZCOMPANYQYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_QY company = new JH_Auth_QY();
            company = JsonConvert.DeserializeObject<JH_Auth_QY>(P1);


            if (string.IsNullOrEmpty(company.corpSecret) || string.IsNullOrEmpty(company.corpId))
            {
                msg.ErrorMsg = "初始化企业号信息失败,corpId,corpSecret 不能为空";
                return;
            }
            if (!new JH_Auth_QYB().Update(company))
            {
                msg.ErrorMsg = "初始化企业号信息失败";
                return;
            }

        }




        /// <summary>
        /// 获取具有手机端的应用列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETWXAPP(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            msg.Result = new JH_Auth_ModelB().GetEntities(d => !string.IsNullOrEmpty(d.WXUrl)).OrderBy(d => d.ORDERID);
        }




        /// <summary>
        /// 保存应用Token和EncodingAESKey
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SAVEMODEL(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Model model = JsonConvert.DeserializeObject<JH_Auth_Model>(P1);
            if (model.ID != 0)
            {
                if (string.IsNullOrEmpty(model.AppID))
                {
                    msg.ErrorMsg = "至少选择一个企业号应用才能绑定";
                    return;
                }

                if (model.AppType == "1" && (string.IsNullOrEmpty(model.Token) || string.IsNullOrEmpty(model.EncodingAESKey)))
                {
                    msg.ErrorMsg = "Token、EncodingAESKey、企业号应用不能为空";
                }
                else
                {
                    new JH_Auth_ModelB().Update(model);
                }
            }
            else
            {
                msg.ErrorMsg = "绑定失败";
            }
        }

        /// <summary>
        /// @用的查询用户数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETUSERSBYKEY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dt1 = new JH_Auth_UserB().GetDTByCommand("SELECT  UserName,UserRealName,C.DeptName, B.DeptName as DeptName1   FROM JH_Auth_User a LEFT  JOIN  JH_Auth_Branch B on A.BranchCode=B.DeptCode  INNER   JOIN   JH_Auth_Branch C on b.DeptRoot=c.DeptCode WHERE   a.isTX='N' AND UserRealName LIKE '%" + P1 + "%'").SplitDataTable(1, 5);
            DataTable dt2 = new JH_Auth_UserB().GetDTByCommand("SELECT  B.deptName,A.deptName  AS deptName1,A.DeptCode FROM JH_Auth_Branch A INNER JOIN   JH_Auth_Branch B on A.DeptRoot=B.DeptCode  WHERE A.deptName LIKE '%" + P1 + "%' OR B.deptName LIKE '%" + P1 + "%'");

            dt1.AddColum("PNAME", '/', "DeptName", "DeptName1");
            dt2.AddColum("PNAME", '/', "deptName", "deptName1");

            msg.Result = dt1;
            msg.Result1 = dt2;


        }




        /// <summary>
        /// 获取当前用户能看到的系统公告
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETMYXTGG(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string datacount = P1;
            msg.Result = new JH_Auth_UserB().GetDTByCommand("SELECT TOP " + datacount + " ID, CRUserName,fbdw,DName,CRDate,title,ggtype FROM qj_sys_tzgg WHERE ( ','+jsjs+',' like '%," + UserInfo.User.UserName + ",%'  OR jsjs='' )  ORDER BY CRDate DESC ");


        }



        #endregion





        public void TJRY(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strPID = P1;
            Yan_WF_PI PI = new Yan_WF_PIB().GetEntity(D => D.ID.ToString() == P1);
            string glz = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjCascader62557", "1");
            string glzid = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjCascader62557");
            string username = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput42808");
            string xm = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput17448");
            string phone = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput83845");
            string rolecode = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput58151");

            int rol = 0;
            int.TryParse(rolecode, out rol);
            if (new JH_Auth_UserB().GetEntities(d => d.UserName == username).Count() > 0)
            {
                JH_Auth_User USER = new JH_Auth_UserB().GetEntities(d => d.UserName == username).FirstOrDefault();
                USER.UserRealName = xm;
                USER.mobphone = phone;
                new JH_Auth_UserB().Update(USER);
            }
            else
            {
                JH_Auth_User USER = new JH_Auth_User();
                USER.UserName = username;
                USER.UserRealName = xm;
                USER.UserPass = CommonHelp.GetMD5("abc123");
                USER.BranchCode = int.Parse(glzid);
                USER.CRUser = UserInfo.User.UserName;
                USER.IsUse = "Y";
                USER.ComId = 10334;
                USER.mobphone = phone;
                USER.CRDate = DateTime.Now;
                new JH_Auth_UserB().Insert(USER);
                new JH_Auth_UserRoleB().Insert(new JH_Auth_UserRole() { ComId = UserInfo.User.ComId, UserName = username, RoleCode = rol });
            }

        }



        #region 操作权限
        /// <summary>
        /// 获取具体操作代码权限
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETCZQXMX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable dtQX = DBCon.GetDTByCommand("select * from qj_qxkz ");
            msg.Result = dtQX;

            DataTable dtCZQX = DBCon.GetDTByCommand("SELECT * from dbo.qj_qxkz where  kssj < GETDATE() and jssj> GETDATE()");
            msg.Result1 = dtCZQX;

        }



        /// <summary>
        /// 保存操作权限信息
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SAVECZQX(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //职务
            DataTable dt = new DataTable();
            dt = JsonConvert.DeserializeObject<DataTable>(P1);
            List<Dictionary<string, object>> dc = DBCon.Db.Utilities.DataTableToDictionaryList(dt);//5.0.23版本支持
            var t666 = new JH_Auth_QYB().Db.Updateable(dc).AS("qj_qxkz").WhereColumns("id").ExecuteCommand();
        }


        #endregion
    }

    public class WXUserBR
    {
        public int DeptCode { get; set; }
        public string DeptName { get; set; }
        public dynamic DeptUser { get; set; }
        public int DeptUserNum { get; set; }
        public List<WXUserBR> SubDept { get; set; }
    }

    public class ExtDataModel
    {
        public int ComId { get; set; }
        public int ID { get; set; }
        public string TableName { get; set; }
        public string TableFiledColumn { get; set; }
        public string TableFiledName { get; set; }
        public string TableFileType { get; set; }
        public string DefaultOption { get; set; }
        public string DefaultValue { get; set; }
        public string IsRequire { get; set; }
        public int? ExtendModeID { get; set; }
        public int? ExtID { get; set; }
        public int? DataID { get; set; }
        public string ExtendDataValue { get; set; }
    }



    public class YYModel
    {
        public JH_Auth_Model Model { get; set; }
        public List<JH_Auth_Function> funs { get; set; }
        public List<BI_DB_Table> tab { get; set; }
        public List<Yan_WF_PD> pds { get; set; }
        public List<BI_DB_Set> dset { get; set; }

        public List<BI_DB_Dim> dims { get; set; }

        public List<BI_DB_Tablefiled> fids { get; set; }

        public List<BI_DB_YBP> ybps { get; set; }


    }

}