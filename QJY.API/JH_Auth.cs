﻿using Newtonsoft.Json;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace QJY.API
{
    #region 系统基础模块



    #region 系统模块
    //用户表
    public class JH_Auth_UserB : BaseEFDao<JH_Auth_User>
    {

        /// <summary>
        /// 基础身份信息
        /// </summary>
        public class BaseUserInfo
        {
            /// <summary>
            /// 唯一识别码,工号或则学好
            /// </summary>
            public string xh { get; set; }

            /// <summary>
            /// 姓名
            /// </summary>
            public string xm { get; set; }

            /// <summary>
            /// 所在机构
            /// </summary>
            public string szjg { get; set; }
            /// <summary>
            /// 所在机构代码
            /// </summary>
            public string jgdm { get; set; }
            /// <summary>
            /// 性别
            /// </summary>
            public string xb { get; set; }
            /// <summary>
            /// 名族
            /// </summary>

            public string phone { get; set; }

            /// <summary>
            /// 职务
            /// </summary>
            public string roleinfo { get; set; }

            /// <summary>
            /// 身份
            /// </summary>
            public string sf { get; set; }

        }

        public class UserInfo : BaseUserInfo
        {
            public JH_Auth_User User;
            public JH_Auth_QY QYinfo;
            public JH_Auth_Branch BranchInfo;
            public string UserRoleCode;
            public string UserBMQXCode;
            public string UserDWName;
            public string UserQXLevel;
        }

        public UserInfo GetUserInfoByWxopenid(string wxopenid)
        {

            UserInfo UserInfo = new UserInfo();
            UserInfo.User = new JH_Auth_UserB().GetUserByWxopenid(wxopenid);
            if (UserInfo.User != null)
            {
                UserInfo = GetUserInfo(UserInfo.User.ComId.Value, UserInfo.User.UserName);

            }

            return UserInfo;
        }


        public UserInfo GetUserInfo(int intComid, string strUserName)
        {
            UserInfo UserInfo = new UserInfo();
            JH_Auth_User User = new JH_Auth_UserB().GetUserByUserName(intComid, strUserName);
            UserInfo.User = User;
            UserInfo.UserRoleCode = new JH_Auth_UserRoleB().GetRoleCodeByUserName(UserInfo.User.UserName, UserInfo.User.ComId.Value);
            UserInfo.QYinfo = new JH_Auth_QYB().GetEntity(d => d.ComId == UserInfo.User.ComId.Value);
            UserInfo.BranchInfo = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.QYinfo.ComId, UserInfo.User.BranchCode);
            string strQXJG = (GetQXBMByUserRole(UserInfo.QYinfo.ComId, UserInfo.UserRoleCode) + ",1," + UserInfo.BranchInfo.Remark1.Replace("-", ",") + "," + new JH_Auth_BranchB().GetSubBranchList(User.BranchCode.ToString())).Trim(',');
            UserInfo.UserBMQXCode = strQXJG.SplitTOList(',').Distinct().ToList().ListTOString(',');
            UserInfo.UserDWName = new JH_Auth_UserB().GetUserDWName(UserInfo.User.ComId.Value, UserInfo.User.UserName);
            UserInfo.UserQXLevel = new JH_Auth_RoleB().GetRoleLev(UserInfo.UserRoleCode);

            return UserInfo;
        }

        public JH_Auth_User GetUserByUserName(int ComID, string UserName)
        {
            JH_Auth_User branchmodel = new JH_Auth_User();
            branchmodel = new JH_Auth_UserB().GetEntity(d => d.UserName == UserName);
            return branchmodel;
        }
        public string GetUserByUserRealName(int ComID, string RealName)
        {
            string strUserName = "";
            JH_Auth_User user = new JH_Auth_UserB().GetEntities(d => d.ComId == ComID && d.UserRealName == RealName).FirstOrDefault();
            if (user != null)
            {
                strUserName = user.UserName;
            }
            return strUserName;
        }
        public JH_Auth_User GetUserByPCCode(string PCCode)
        {
            JH_Auth_User branchmodel = new JH_Auth_User();
            branchmodel = new JH_Auth_UserB().GetEntity(d => d.pccode == PCCode);
            return branchmodel;
        }



        /// <summary>
        /// 根据微信openid获取用户信息
        /// </summary>
        /// <param name="PCCode"></param>
        /// <returns></returns>
        public JH_Auth_User GetUserByWxopenid(string WXopenid)
        {
            JH_Auth_User USER = new JH_Auth_User();
            USER = new JH_Auth_UserB().GetEntity(d => d.weixinCard == WXopenid);
            return USER;
        }

        /// <summary>
        /// 根据角色获取当前角色拥有的部门权限
        /// </summary>
        /// <param name="ComID"></param>
        /// <param name="UserRoleCode"></param>
        /// <returns></returns>
        public string GetQXBMByUserRole(int ComID, string UserRoleCode)
        {
            string strReturn = "";
            if (!string.IsNullOrWhiteSpace(UserRoleCode))
            {
                DataTable dt = new JH_Auth_RoleB().GetDTByCommand("SELECT RoleQX FROM JH_Auth_Role WHERE ROleCode in (" + UserRoleCode + ")");
                List<string> ListQXBM = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string strBMCodes = dt.Rows[i]["RoleQX"].ToString();
                    foreach (string bmcode in strBMCodes.Split(','))
                    {
                        if (!ListQXBM.Contains(bmcode))
                        {
                            ListQXBM.Add(bmcode);
                        }
                    }
                }
                strReturn = ListQXBM.ListTOString(',').Trim(',');
            }
            return strReturn;
        }

        public string GetUserRealName(int intComid, string strUserName)
        {
            string[] USERS = strUserName.Split(',');
            string UserRealName = "";
            var Users = new JH_Auth_UserB().CurrentDb.AsQueryable().Where(it => USERS.Contains(it.UserName)).ToList();
            foreach (var item in Users)
            {
                UserRealName = UserRealName + item.UserRealName + ",";
            }
            return UserRealName.Trim(',');

        }

        /// <summary>
        /// 获取用户单位名称
        /// </summary>
        /// <param name="ComId"></param>
        /// <param name="strUser"></param>
        /// <returns></returns>
        public string GetUserDWName(int ComId, string strUser)
        {

            try
            {
                string strSql = string.Format("SELECT DeptName FROM JH_Auth_User  INNER JOIN JH_Auth_Branch ON  JH_Auth_User.remark=JH_Auth_Branch.DeptCode WHERE JH_Auth_User.ComId={0} and UserName='{1}'", ComId, strUser);
                return new JH_Auth_UserB().ExsSclarSql(strSql).ToString();
            }
            catch (Exception)
            {
                return "无此单位";
            }
        }

        /// <summary>
        /// 获取单位信息
        /// </summary>
        /// <param name="strUser"></param>
        /// <returns></returns>
        public string GetUserDWInfo(string strUser)
        {

            try
            {
                string strSql = string.Format("SELECT JH_Auth_Branch.*  FROM JH_Auth_User  INNER JOIN JH_Auth_Branch ON  JH_Auth_User.remark=JH_Auth_Branch.DeptCode WHERE  UserName='{0}'", strUser);
                return new JH_Auth_UserB().ExsSclarSql(strSql).ToString();
            }
            catch (Exception)
            {
                return "无此单位";
            }
        }
        /// <summary>
        ///  获取用户手机号
        /// </summary>
        /// <param name="ComId"></param>
        /// <param name="strUser"></param>
        /// <returns></returns>
        public string GetUserSPhone(string strUser)
        {
            try
            {
                string strPhone = "";
                string strSql = string.Format("SELECT mobphone FROM jh_auth_user WHERE UserName IN('" + strUser.ToFormatLike() + "')");
                DataTable dt = new JH_Auth_UserB().GetDTByCommand(strSql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strPhone = strPhone + dt.Rows[i]["mobphone"].ToString() + ",";
                }

                return strPhone.TrimEnd(',');
            }
            catch (Exception)
            {
                return "查询手机号有误";
            }


        }





        /// <summary>
        /// 获取数据查询条件
        /// </summary>
        /// <param name="ComId"></param>
        /// <param name="strDW"></param>
        /// <returns></returns>
        public string GetDWUserSWhere(JH_Auth_UserB.UserInfo UserInfo, string strUserFiled = "CRUser", string isAllData = "")
        {
            string strWhere = "";
            if (!UserInfo.UserRoleCode.Contains("1218") && isAllData == "")//如果不是超级管理员
            {
                string strUsers = "";
                DataTable dt = new JH_Auth_UserB().GetDTByCommand("SELECT UserName FROM  JH_Auth_User WHERE BranchCode IN ('" + UserInfo.UserBMQXCode.Trim(',').ToFormatLike() + "')");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strUsers = strUsers + dt.Rows[i][0].ToString() + ",";
                }
                strUsers = strUsers.TrimEnd(',');
                strWhere = " AND " + strUserFiled + " IN ('" + strUsers.ToFormatLike() + "')";
            }
            return strWhere;
        }



        public void UpdateloginDate(int ComId, string strUser)
        {
            string strSql = string.Format("UPDATE JH_Auth_User SET logindate='{0}' WHERE ComId={1} and UserName = '{2}'", DateTime.Now.ToString("yyyy-MM-dd HH:ss"), ComId, strUser.ToFormatLike());
            new JH_Auth_UserB().ExsSql(strSql);
        }





        public void UpdatePassWord(int ComId, string strUser, string strNewPassWord)
        {
            string strSql = string.Format("UPDATE JH_Auth_User SET UserPass='{0}' WHERE ComId={1} and UserName in ('{2}')", strNewPassWord, ComId, strUser.ToFormatLike());
            new JH_Auth_UserB().ExsSql(strSql);
        }


        /// <summary>
        /// 根据部门获取用户列表
        /// </summary>
        /// <param name="branchCode">部门编号</param>
        /// <param name="strFilter">姓名，部门，手机号</param>
        /// <returns></returns>
        public DataTable GetUserListbyBranch(int branchCode, string strFilter, int ComId)
        {
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(ComId, branchCode);
            string strSQL = "select u.*,b.DeptName,b.DeptCode from  JH_Auth_User u  inner join JH_Auth_Branch b on u.branchCode=b.DeptCode where 1=1 ";
            strSQL += string.Format(" And  b.Remark1 like '%{0}%'", branch.Remark1);

            if (strFilter != "")
            {
                strSQL += string.Format(" And (u.UserName like '%{0}%'  or u.UserRealName like '%{0}%'  or b.DeptName like '%{0}%' or u.mobphone like '%{0}%')", strFilter);
            }
            DataTable dt = new JH_Auth_UserB().GetDTByCommand(strSQL + " ORDER by b.DeptCode,u.UserOrder asc");
            return dt;
        }




        /// <summary>
        /// 根据部门获取可用用户列表，包含下级部门
        /// </summary>
        /// <param name="branchCode">部门编号</param>
        /// <param name="strFilter">姓名，部门，手机号</param>
        /// <param name="comId">公司ID</param>
        /// <returns></returns>
        public DataTable GetUserListbyBranchUse(int branchCode, string strFilter, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(UserInfo.User.ComId.Value, branchCode);
            string strQXWhere = string.Format(" And  b.Remark1 like '%{0}%'", branch.Remark1);

            string strSQL = "select u.*,b.DeptName,b.DeptCode from  JH_Auth_User u  inner join JH_Auth_Branch b on u.branchCode=b.DeptCode where u.IsUse='Y' and u.ComId=" + UserInfo.User.ComId;
            strSQL += string.Format(" {0} ", strQXWhere);

            if (strFilter != "")
            {
                strSQL += string.Format(" And (u.UserName like '%{0}%'  or u.UserRealName like '%{0}%'  or b.DeptName like '%{0}%' or u.mobphone like '%{0}%')", strFilter);
            }
            DataTable dt = new JH_Auth_UserB().GetDTByCommand(strSQL + " order by b.DeptShort,ISNULL(u.UserOrder, 1000000) asc");
            return dt;
        }


        /// <summary>
        /// 找到用户的直属上级,先找用户表leader,再找部门leader
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns></returns>
        public string GetUserLeader(int ComId, string strUserName)
        {
            string strLeader = "";
            UserInfo UserInfo = this.GetUserInfo(ComId, strUserName);
            if (!string.IsNullOrEmpty(UserInfo.User.UserLeader))
            {
                strLeader = UserInfo.User.UserLeader;
            }
            else
            {
                strLeader = UserInfo.BranchInfo.BranchLeader;
            }

            return strLeader;
        }


    }

    public class JH_Auth_User_CenterB : BaseEFDao<JH_Auth_User_Center>
    {
        /// <summary>
        /// 添加消息并发送微信消息
        /// </summary>
        /// <param name="UserInfo"></param>
        /// <param name="ModelCode"></param>
        /// <param name="content"></param>
        /// <param name="Id"></param>
        /// <param name="JSR"></param>
        public void SendMsg(JH_Auth_UserB.UserInfo UserInfo, string ModelCode, string content, string Id, string JSR, string strMsgUrl = "")
        {

            JH_Auth_Model Model = new JH_Auth_ModelB().GetModeByCode(ModelCode);
            JH_Auth_User_Center userCenter = new JH_Auth_User_Center();
            userCenter.ComId = UserInfo.QYinfo.ComId;
            userCenter.CRUser = UserInfo.User.UserName;
            userCenter.CRDate = DateTime.Now;
            userCenter.MsgContent = content;
            userCenter.MsgType = Model == null ? "" : Model.ModelName;
            userCenter.UserFrom = UserInfo.User.UserName;
            userCenter.isRead = 0;
            userCenter.DataId = Id;
            userCenter.MsgModeID = ModelCode;
            userCenter.isCS = "Y";
            userCenter.Remark = "0";
            userCenter.MsgLink = strMsgUrl;
            if (strMsgUrl == "")
            {
                userCenter.MsgLink = GetMsgLink(ModelCode, Id.ToString(), UserInfo.QYinfo);
                userCenter.wxLink = GetMsgLink(ModelCode, Id.ToString(), UserInfo.QYinfo);
            }


            List<string> jsrs = JSR.Split(',').Distinct().ToList();//去重接收人
            foreach (string people in jsrs)
            {
                if (!string.IsNullOrEmpty(people))
                {
                    userCenter.UserTO = people;
                    new JH_Auth_User_CenterB().Insert(userCenter);
                }

            }

        }

        /// <summary>
        /// 获取消息链接
        /// </summary>
        /// <param name="modelCode"></param>
        /// <param name="Id"></param>
        /// <param name="Qyinfo"></param>
        /// <returns></returns>
        public string GetMsgLink(string modelCode, string Id, JH_Auth_QY Qyinfo)
        {
            if (modelCode != "TXSX")
            {
                string url = "";
                // Uri="/View_Mobile/UI/UI_COMMON.html?funcode=" + modelCode + "_" + type + "_" + Id + "&corpId=" + Qyinfo.corpId;
                return url;
            }
            else
            {
                string url = "/ViewV5/AppPage/FORMBI/FormManage.html?vtype=2&piid=" + Id;
                return url;
            }
        }


        /// <summary>
        /// 阅读消息
        /// </summary>
        /// <param name="UserInfo"></param>
        /// <param name="DataID"></param>
        /// <param name="ModelCode"></param>
        public void ReadMsg(JH_Auth_UserB.UserInfo UserInfo, int DataID, string ModelCode)
        {
            Task<string> TaskCover = Task.Factory.StartNew<string>(() =>
            {
                string strSql = string.Format("UPDATE JH_Auth_User_Center SET isRead='1',ReadDate='{4}' WHERE DataId='{0}'AND ComId='{1}' AND UserTO='{2}' AND  MsgModeID='{3}'", DataID, UserInfo.User.ComId.Value, UserInfo.User.UserName, ModelCode, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                object obj = new JH_Auth_User_CenterB().ExsSclarSql(strSql);
                return "";
            });
        }
    }


    //部门表
    public class JH_Auth_BranchB : BaseEFDao<JH_Auth_Branch>
    {

        public void AddBranch(JH_Auth_UserB.UserInfo UserInfo, JH_Auth_Branch branch, Msg_Result msg)
        {

            if (branch.DeptCode == 0)//DeptCode==0为添加部门
            {
                //获取要添加的部门名称是否存在，存在提示用户，不存在添加
                JH_Auth_Branch branch1 = new JH_Auth_BranchB().GetEntity(d => d.DeptName == branch.DeptName && d.ComId == UserInfo.User.ComId && d.DeptRoot == branch.DeptRoot);
                if (branch1 != null)
                {
                    msg.ErrorMsg = "同级别内此部门已存在";
                    return;
                }

                branch.ComId = UserInfo.User.ComId;
                branch.CRUser = UserInfo.User.UserName;
                branch.CRDate = DateTime.Now;
                int sum = new JH_Auth_BranchB().Db.Queryable<JH_Auth_Branch>().Max(it => it.DeptCode);
                branch.DeptCode = sum + 1;
                //添加部门，失败提示用户，成功赋值微信部门Code并更新
                if (!new JH_Auth_BranchB().Insert(branch))
                {
                    msg.ErrorMsg = "添加部门失败";
                    return;
                }
                //获取上级的Path，用于上级查找下级所有部门或用户
                branch.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, branch.DeptRoot) + branch.DeptCode;
                new JH_Auth_BranchB().Update(branch);
                if (UserInfo.QYinfo.IsUseWX == "Y")
                {

                    WXHelp bm = new WXHelp(UserInfo.QYinfo);
                    long branid = bm.WX_CreateBranch(branch);
                    branch.WXBMCode = int.Parse(branid.ToString());
                    new JH_Auth_BranchB().Update(branch);



                }
                msg.Result = branch;
            }
            else//DeptCode不等于0时为修改部门
            {
                if (branch.DeptRoot != -1)
                {
                    branch.Remark1 = new JH_Auth_BranchB().GetBranchNo(UserInfo.User.ComId.Value, branch.DeptRoot) + branch.DeptCode;
                }
                if (UserInfo.QYinfo.IsUseWX == "Y" && branch.DeptRoot != -1)
                {
                    WXHelp bm = new WXHelp(UserInfo.QYinfo);
                    bm.WX_UpdateBranch(branch);
                }
                if (!new JH_Auth_BranchB().Update(branch))
                {
                    msg.ErrorMsg = "修改部门失败";
                    return;
                }
            }

        }

        public JH_Auth_Branch GetBMByDeptCode(int ComID, int DeptCode)
        {
            JH_Auth_Branch branchmodel = new JH_Auth_Branch();

            branchmodel = new JH_Auth_BranchB().GetEntity(d => d.DeptCode == DeptCode);

            return branchmodel;
        }


        public override bool Update(JH_Auth_Branch entity)
        {

            if (base.Update(entity))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool Delete(JH_Auth_Branch entity)
        {

            return base.Delete(entity);
        }



        /// <summary>
        /// 根据部门代码删除部门及部门人员
        /// </summary>
        /// <param name="intBranchCode"></param>
        public void DelBranchByCode(int intBranchCode)
        {
            new JH_Auth_BranchB().Delete(d => d.DeptCode == intBranchCode);
            new JH_Auth_UserB().Delete(d => d.BranchCode == intBranchCode);
        }

        /// <summary>
        /// 获取机构数用在assginuser.ASPX中
        /// </summary>
        /// <param name="intRoleCode">角色代码</param>
        /// <param name="intBranchCode">机构代码</param>
        /// <returns></returns>
        public string GetBranchUserTree(string CheckNodes, int intBranchCode)
        {
            StringBuilder strTree = new StringBuilder();
            var q = new JH_Auth_BranchB().GetEntities(d => d.DeptRoot == intBranchCode);
            foreach (var item in q)
            {
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',name:'{2}',{3}}},", item.DeptCode, item.DeptRoot, item.DeptName, item.DeptRoot == -1 || item.DeptRoot == 0 ? "open:true" : "open:false");
                strTree.Append(GetUserByBranch(CheckNodes, item.DeptCode));
                strTree.Append(GetBranchUserTree(CheckNodes, item.DeptCode));
            }
            return strTree.Length > 0 ? strTree.ToString() : "";
        }

        public string GetUserByBranch(string CheckNodes, int intBranchCode)
        {
            StringBuilder strTree = new StringBuilder();
            var q = new JH_Auth_UserB().GetEntities(d => d.BranchCode == intBranchCode);
            foreach (var item in q)
            {
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',name:'{2}',isUser:'{3}',{4}}},", item.UserName, intBranchCode, item.UserRealName, "Y", CheckNodes.SplitTOList(',').Contains(item.UserName) ? "checked:true" : "checked:false");
            }
            return strTree.Length > 0 ? strTree.ToString() : "";
        }




        /// <summary>
        /// 获取组织机构树
        /// </summary>
        /// <param name="intDeptCode">机构代码</param>
        /// <returns></returns>
        public DataTable GetBranchList(int intDeptCode, int comId, string branchQX = "", int index = 0)
        {
            DataTable dtRoot = new DataTable();
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * from JH_Auth_Branch  where DeptRoot=" + intDeptCode + " and ComId=" + comId + " order by DeptShort DESC");
            dt.Columns.Add("ChildBranch", Type.GetType("System.Object"));
            dt.Columns.Add("blname");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["blname"] = new JH_Auth_UserB().GetUserRealName(comId, dt.Rows[i]["BranchLeader"].ToString());
            }
            foreach (DataRow row in dt.Rows)
            {
                int deptCode = int.Parse(row["DeptCode"].ToString());
                index++;
                if (branchQX == "")
                {
                    row["ChildBranch"] = GetBranchList(deptCode, comId, branchQX, index);
                }
                else
                {
                    if (branchQX.SplitTOInt(',').Contains(deptCode))
                    {
                        row.Delete();
                    }
                    else
                    {
                        row["ChildBranch"] = GetBranchList(deptCode, comId, branchQX, index);
                    }
                }
            }
            dt.AcceptChanges();
            if (dtRoot.Rows.Count > 0)
            {
                dtRoot.Rows[0]["ChildBranch"] = dt;
                return dtRoot;
            }
            else
            {
                return dt;
            }
        }
        /// <summary>
        /// 获取组织机构树
        /// </summary>
        /// <param name="intDeptCode">机构代码</param>
        /// <returns></returns>
        public string GetBranchTree(int intDeptCode, string checkValue)
        {
            string[] checkIds = checkValue.Split(',');
            StringBuilder strTree = new StringBuilder();
            var q = new JH_Auth_BranchB().GetEntities(d => d.DeptRoot == intDeptCode);
            foreach (var item in q)
            {
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',attr:'{2}',name:'{3}',leader:'{4}',isuse:'{5}',order:'{6}',checked:{7}}},", item.DeptCode, item.DeptRoot, "Branch", item.DeptName, item.Remark1, item.Remark2, item.DeptShort, Array.IndexOf(checkIds, item.DeptCode.ToString()) > -1 ? "true" : "false");
                strTree.Append(GetBranchTree(item.DeptCode, checkValue));
            }
            return strTree.Length > 0 ? strTree.ToString() : "";
        }
        /// <summary>
        /// 获取组织机构树
        /// </summary>
        /// <param name="intDeptCode">机构代码</param>
        /// <returns></returns>
        public string GetBranchTreeNew(int intDeptCode, int comId, int index = 0)
        {

            StringBuilder strTree = new StringBuilder();
            if (index == 0)
            {
                JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(comId, intDeptCode);
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',attr:'{2}',name:'{3}',leader:'{4}',isuse:'{5}',order:'{6}',{7}}},", branch.DeptCode, branch.DeptRoot, "Branch", branch.DeptName, branch.Remark1, branch.Remark2, branch.DeptShort, index == 0 ? "open:true" : "open:false");
                index++;
            }
            var q = new JH_Auth_BranchB().GetEntities(d => d.DeptRoot == intDeptCode && d.ComId == comId);
            foreach (var item in q)
            {
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',attr:'{2}',name:'{3}',leader:'{4}',isuse:'{5}',order:'{6}',{7}}},", item.DeptCode, item.DeptRoot, "Branch", item.DeptName, item.Remark1, item.Remark2, item.DeptShort, index == 0 ? "open:true" : "open:false");
                index++;
                strTree.Append(GetBranchTreeNew(item.DeptCode, comId, index));
            }
            return strTree.Length > 0 ? strTree.ToString() : "";
        }

        /// <summary>
        /// 获取组织机构树
        /// </summary>
        /// <param name="intDeptCode">机构代码</param>
        /// <returns></returns>
        public string GetBranchTree(int intDeptCode, int comId, string checkval, string branchQX = "", int index = 0)
        {
            StringBuilder strTree = new StringBuilder();
            if (index == 0)
            {
                JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(comId, intDeptCode);
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',attr:'{2}',name:'{3}',leader:'{4}',isuse:'{5}',order:'{6}',{7},checked:{8}}},", branch.DeptCode, branch.DeptRoot, "Branch", branch.DeptName, branch.Remark1, branch.Remark2, branch.DeptShort, index == 0 ? "open:true" : "open:false", Array.IndexOf(checkval.Split(','), branch.DeptCode.ToString()) > -1 ? "true" : "false");
                index++;
            }

            var q = new JH_Auth_BranchB().GetEntities(d => d.DeptRoot == intDeptCode && d.ComId == comId);
            foreach (var item in q)
            {
                //两种情况加载机构1:权限为空格代表拥有全部权限,2：权限部门包含当前部门
                if (branchQX == "" || branchQX.SplitTOInt(',').Contains(item.DeptCode))
                {
                    strTree.AppendFormat("{{id:'{0}',pId:'{1}',attr:'{2}',name:'{3}',leader:'{4}',isuse:'{5}',order:'{6}',{7},checked:{8}}},", item.DeptCode, item.DeptRoot, "Branch", item.DeptName, item.BranchLeader, item.Remark2, item.DeptShort, index == 0 ? "open:true" : "open:false", Array.IndexOf(checkval.Split(','), item.DeptCode.ToString()) > -1 ? "true" : "false");
                    strTree.Append(GetBranchTree(item.DeptCode, comId, checkval, branchQX, index));
                }
                index++;

            }
            return strTree.Length > 0 ? strTree.ToString() : "";
        }


        //获取JSON用户信息

        /// <summary>
        /// 获取部门级别编号
        /// </summary>
        /// <param name="DeptRoot"></param>
        /// <returns></returns>
        public string GetBranchNo(int ComID, int DeptRoot)
        {
            string BranchNo = "";
            var BranchUP = new JH_Auth_BranchB().GetBMByDeptCode(ComID, DeptRoot);
            //如果添加的上级部门存在，并且添加的同级部门中存在数据，获取同级部门的最后一个编号+1
            if (BranchUP != null)
            {
                BranchNo = BranchUP.Remark1 == "" ? "" : BranchUP.Remark1 + "-";
            }
            return BranchNo;
        }

        public class BranchUser
        {
            public int BranchID { get; set; }
            public string BranchName { get; set; }
            public string BranchFzr { get; set; }

            public List<BranchUser> SubBranch { get; set; }
            public List<JH_Auth_User> SubUsers { get; set; }

        }
        /// <summary>
        /// 获取当前部门不能查看的部门Ids
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="ComId"></param>
        /// <returns></returns>
        public string GetBranchQX(JH_Auth_UserB.UserInfo userInfo)
        {
            string strSql = string.Format("SELECT DeptCode from JH_Auth_Branch where ComId={0}  And  DeptCode NOT IN ('" + userInfo.UserBMQXCode.ToFormatLike() + "') ", userInfo.User.ComId);
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand(strSql);
            string qxbranch = "";
            foreach (DataRow row in dt.Rows)
            {
                qxbranch += row["DeptCode"] + ",";
            }
            qxbranch = qxbranch.Length > 0 ? qxbranch.Substring(0, qxbranch.Length - 1) : "";
            return qxbranch;
        }



        /// <summary>
        /// 获取下属部门
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public string GetSubBranchList(string strDeptCode)
        {
            string strSql = string.Format("SELECT DeptCode from JH_Auth_Branch where Remark1 like '%{0}%'", strDeptCode);
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand(strSql);
            string qxbranch = "";
            foreach (DataRow row in dt.Rows)
            {
                qxbranch += row["DeptCode"] + ",";
            }
            return qxbranch.TrimEnd(',');
        }
    }





    //角色表
    public class JH_Auth_RoleB : BaseEFDao<JH_Auth_Role>
    {

        /// <summary>
        /// 获取角色树
        /// </summary>
        /// <param name="intRoleCode">角色代码</param>
        /// <returns></returns>
        public string GetRoleTree(int intRoleCode)
        {
            StringBuilder strTree = new StringBuilder();
            var q = new JH_Auth_RoleB().GetEntities(d => d.PRoleCode == intRoleCode);
            foreach (var item in q)
            {
                strTree.AppendFormat("{{id:'{0}',pId:'{1}',icon:'../../Image/admin/users.png',issys:'{2}',isuse:'{3}',name:'{4}',nodedec:'{5}'}},", item.RoleCode, item.PRoleCode, item.isSysRole, item.IsUse, item.RoleName, item.RoleDec);
                strTree.Append(GetRoleTree(item.RoleCode));
            }
            return strTree.Length > 0 ? strTree.ToString() : "";
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="intRoleCode">角色代码</param>
        /// <returns></returns>
        public string delRole(int intRoleCode, int ComId)
        {
            new JH_Auth_RoleB().Delete(d => intRoleCode == d.RoleCode && d.isSysRole != "Y" && d.ComId == ComId);
            new JH_Auth_UserRoleB().Delete(d => intRoleCode == d.RoleCode && d.ComId == ComId);
            new JH_Auth_RoleFunB().Delete(d => d.RoleCode == intRoleCode && d.ComId == ComId);
            return "Success";
        }

        public DataTable GetModelFun(int ComId, string RoleCode, string strModeID, string strMenuType = "")
        {

            string strwhere = " and JH_Auth_Function.isinit<>'m' ";
            if (strMenuType == "m")
            {
                //移动端
                strwhere = " and JH_Auth_Function.isinit='m' ";
            }

            DataTable dt = new JH_Auth_UserRoleB().GetDTByCommand("SELECT  DISTINCT JH_Auth_Function.ID, JH_Auth_Function.ModelID, JH_Auth_Function.Remark,JH_Auth_Function.PageName,JH_Auth_Function.ExtData,JH_Auth_RoleFun.ActionCode as ActionData,JH_Auth_Function.PageUrl,JH_Auth_Function.FunOrder,JH_Auth_Function.PageCode,JH_Auth_Function.isiframe FROM JH_Auth_RoleFun INNER JOIN JH_Auth_Function ON JH_Auth_RoleFun.FunCode=JH_Auth_Function.ID WHERE  JH_Auth_Function.ComId='0' and  RoleCode IN (" + RoleCode + ")  AND ModelID='" + strModeID + "'" + strwhere + "    order by JH_Auth_Function.FunOrder,JH_Auth_Function.PageName");
            DataTable dtReturn = dt.DefaultView.ToTable(true, new string[] { "ID", "ModelID", "Remark", "PageName", "ExtData", "PageUrl", "FunOrder", "PageCode", "isiframe" });

            dtReturn.Columns.Add("ActionData");
            for (int i = 0; i < dtReturn.Rows.Count; i++)
            {
                string strTemp = "";
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    if (dtReturn.Rows[i]["ID"].ToString() == dt.Rows[m]["ID"].ToString())
                    {
                        strTemp = strTemp + dt.Rows[m]["ActionData"].ToString() + ",";
                    }

                }
                dtReturn.Rows[i]["ActionData"] = strTemp.Trim(',').SplitTOList(',').Distinct().ToList().ListTOString(',');
            }

            return dtReturn;
        }


        /// <summary>
        /// 获取用户级别
        /// </summary>
        /// <param name="RoleCodeS"></param>
        /// <returns></returns>
        public string GetRoleLev(string RoleCodeS)
        {
            DataTable dt = new JH_Auth_UserRoleB().GetDTByCommand("SELECT ISNULL(MAX(IsHasQX) , '1') AS yhlevel  FROM jh_auth_role WHERE RoleCode IN ('" + RoleCodeS.ToFormatLike() + "')");
            return dt.Rows[0][0].ToString();
        }


    }



    //用户角色表
    public class JH_Auth_UserRoleB : BaseEFDao<JH_Auth_UserRole>
    {


        /// <summary>
        /// 获取用户的角色代码
        /// </summary>
        /// <param name="strUserName">用户名</param>
        /// <returns></returns>
        public string GetRoleCodeByUserName(string strUserName, int ComId = 0)
        {
            string strRoleCode = "";
            var q = new JH_Auth_UserRoleB().GetEntities(d => d.UserName == strUserName);
            foreach (var item in q)
            {
                strRoleCode = strRoleCode + item.RoleCode.ToString() + ",";
            }
            return strRoleCode.TrimEnd(',');
        }

        /// <summary>
        /// 获取用户的角色代码
        /// </summary>
        /// <param name="strUserName">用户名</param>
        /// <returns></returns>
        public string GetRoleNameByUserName(string strUserName, int ComId = 0)
        {
            string strRoleName = "";
            string strRoleCode = GetRoleCodeByUserName(strUserName, ComId);
            DataTable dt = new JH_Auth_UserRoleB().GetDTByCommand("SELECT RoleName from JH_Auth_Role  where  RoleCode IN ('" + strRoleCode.ToFormatLike() + "') ");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strRoleName = strRoleName + dt.Rows[i]["RoleName"].ToString().Trim() + ",";
            }
            return strRoleName.TrimEnd(',');
        }



        /// <summary>
        /// 根据角色获取相应用户
        /// </summary>
        /// <param name="intRoleCode"></param>
        /// <returns></returns>
        public string GetUserByRoleCode(int intRoleCode)
        {
            return new JH_Auth_UserRoleB().GetEntities(d => d.RoleCode == intRoleCode).Select(d => d.UserName).ToList().ListTOString(',');
        }

        public DataTable GetUserDTByRoleCode(int intRoleCode, int ComId)
        {
            DataTable dt = new JH_Auth_UserRoleB().GetDTByCommand(" SELECT JH_Auth_User.* FROM JH_Auth_UserRole ur inner join JH_Auth_User on ur.username=JH_Auth_User.username where  JH_Auth_User.IsUse='Y' And JH_Auth_User.ComId=" + ComId + " And  ur.rolecode= " + intRoleCode);
            return dt;
        }



        /// <summary>
        /// 判断是否从属同一机构
        /// </summary>
        /// <param name="strFQUser"></param>
        /// <param name="strSHUser"></param>
        /// <returns></returns>
        public bool IsSameOrg(string strFQUser, string strSHUser)
        {

            DataTable dt = new Yan_WF_TIB().GetDTByCommand("SELECT  BranchCode from JH_Auth_User   where UserName in ('" + strFQUser + "','" + strSHUser + "')");
            if (dt.Rows[0]["BranchCode"].ToString() == dt.Rows[1]["BranchCode"].ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }






    public class JH_Auth_ZiDianB : BaseEFDao<JH_Auth_ZiDian>
    {



        /// <summary>
        /// 获取字典数据
        /// </summary>
        /// <returns></returns>
        public List<JH_Auth_ZiDian> GetZDList()
        {
            List<JH_Auth_ZiDian> ListData = CacheHelp.Get("zidian") as List<JH_Auth_ZiDian>;
            if (ListData != null)
            {
                return ListData;
            }
            else
            {
                ListData = base.GetALLEntities().ToList();
                CacheHelp.Set("zidian", ListData);
                return ListData;
            }
        }


        public void ClearZDCathe()
        {
            CacheHelp.Remove("zidian");
        }


        /// <summary>
        /// 获取字典统计Table
        /// </summary>
        /// <returns></returns>
        public static DataTable GetZDTJTable(string strClass)
        {
            DataTable dt = new DataTable();
            List<JH_Auth_ZiDian> ZDS = SysHelp.SysCathe.ZDList().FindAll(D => D.Class == strClass);
            foreach (var item in ZDS)
            {
                dt.Columns.Add(item.TypeName);
            }
            return dt;
        }


    }





    public class JH_Auth_LogB : BaseEFDao<JH_Auth_Log>
    {

        /// <summary>
        /// 请求日志
        /// </summary>
        /// <param name="Action"></param>
        /// <param name="LogContent"></param>
        /// <param name="OutData"></param>
        /// <param name="strUser"></param>
        /// <param name="InData"></param>
        /// <param name="ComID"></param>
        /// <param name="strIP"></param>
        /// <param name="strNetCode"></param>
        public void InsertLog(string Action, string LogContent, string OutData, string strUser, string InData, int ComID, string strIP, string strNetCode = "")
        {
            //LinkedList<string> linkListPerson = new LinkedList<string>();
            //需要利用缓存排除掉重复请求的日志
            LinkedList<string> linkListPerson = CacheHelp.Get("log") as LinkedList<string>;
            string catchkey = strUser + LogContent + strNetCode;
            if (linkListPerson == null)
            {
                linkListPerson = new LinkedList<string>();
            }
            string strJKSM = "";
            if (LogContent.Split('/').Length > 0)
            {
                strJKSM = CommonHelp.GetApiDes(LogContent.Split('/').Last());
            }
            if (strJKSM != "")
            {
                Task<string> TaskCover = Task.Factory.StartNew<string>(() =>
                {

                    try
                    {
                        string strPageUrl = "";
                        string strPageName = "";
                        if (strNetCode.Split(',').Length > 0)
                        {
                            strPageUrl = strNetCode.Split(',')[0].ToString();
                            if (strNetCode.Split(',').Length > 1)
                            {
                                strPageName = strNetCode.Split(',')[1].ToString();
                            }
                        }
                        if (!linkListPerson.Contains(catchkey))
                        {
                            linkListPerson.AddLast(catchkey);
                            if (linkListPerson.Count > 5)
                            {
                                linkListPerson.RemoveFirst();
                            }
                            CacheHelp.Set("log", linkListPerson);
                            JH_Auth_Log log = new JH_Auth_Log()
                            {
                                ComId = ComID.ToString(),
                                LogType = Action,//日志类型(Form删除日志,错误日志)
                                LogContent = LogContent,//请求头
                                Remark = strJKSM,
                                IP = strIP,//IP
                                CRUser = strUser,//
                                CRDate = DateTime.Now,
                                Remark1 = strPageUrl,//操作页面
                                netcode = strPageName
                            };
                            this.Insert(log);
                            new JH_Auth_LogB().ExsSql("INSERT INTO [dbo].[qj_logdes]( [logid], [indata], [outdata]) VALUES ( " + log.ID + ", N'" + InData + "', N'" + OutData + "');");
                        }

                    }
                    catch (Exception)
                    {


                    }
                    return "";
                });
            }

        }
        /// <summary>
        /// 系统日志
        /// </summary>
        /// <param name="Action"></param>
        /// <param name="LogContent"></param>
        /// <param name="ReMark"></param>
        /// <param name="strUser"></param>
        /// <param name="strUserName"></param>
        /// <param name="ComID"></param>
        /// <param name="strIP"></param>
        /// <param name="strNetCode"></param>
        public void InsertDelLog(string Action, string LogContent, string ReMark, string strUser, string strUserName, int ComID, string strIP, string strNetCode = "")
        {
            Task<string> TaskCover = Task.Factory.StartNew<string>(() =>
            {
                this.Insert(new JH_Auth_Log()
                {
                    ComId = ComID.ToString(),
                    LogType = "数据删除",//日志类型(Form删除日志,错误日志)
                    LogContent = LogContent,//请求头
                    Remark = ReMark,//请求内容
                    IP = strIP,//IP
                    Remark1 = strUserName,//返回内容
                    CRUser = strUser + strUserName,//
                    CRDate = DateTime.Now,
                    netcode = strNetCode
                });
                return "";
            });
        }
    }


    public class JH_Auth_VersionB : BaseEFDao<JH_Auth_Version>
    {

        public void SetUserVerSion(string strUserCode, string strVerID)
        {

            JH_Auth_Version Model = this.GetEntity(d => d.ID.ToString() == strVerID);
            Model.ReadUsers = Model.ReadUsers + "," + strUserCode;
            this.Update(Model);
        }
    }

    public class JH_Auth_QYB : BaseEFDao<JH_Auth_QY>
    {


    }
    public class JH_Auth_QysetB : BaseEFDao<JH_Auth_Qyset>
    {


    }
    public class JH_Auth_UserCustomDataB : BaseEFDao<JH_Auth_UserCustomData>
    {

    }
    public class JH_Auth_RoleFunB : BaseEFDao<JH_Auth_RoleFun>
    { }

    public class JH_Auth_FunctionB : BaseEFDao<JH_Auth_Function>
    { }

    public class JH_Auth_ModelB : BaseEFDao<JH_Auth_Model>
    {

        public JH_Auth_Model GetModeByCode(string strModeCode)
        {
            JH_Auth_Model QYModel = new JH_Auth_ModelB().GetEntity(d => d.ModelCode == strModeCode);
            return QYModel;
        }

        /// <summary>
        /// 获取首页显示菜单
        /// </summary>
        /// <param name="UserInfo">用户信息</param>
        /// <param name="modelType">APPINDEX APP首页  PCINDEX PC首页</param>
        /// <returns></returns>
        public DataTable GETMenuList(JH_Auth_UserB.UserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(UserInfo.UserRoleCode))
            {
                string strSql = string.Format(@"SELECT DISTINCT  model.*
                                             FROM JH_Auth_RoleFun rf INNER JOIN JH_Auth_Function fun on rf.FunCode=fun.ID 
                                            INNER join JH_Auth_Model model on fun.ModelID=model.ID
                                            where fun.ComId='0' AND model.ModelStatus=0 and rf.RoleCode in ({0})", UserInfo.UserRoleCode);
                strSql = strSql + " ORDER by model.ORDERID ";
                return new JH_Auth_ModelB().GetDTByCommand(strSql);
            }
            return new DataTable();
        }
    }
    public class SZHL_TXSXB : BaseEFDao<SZHL_TXSX>
    {
    }
    public class SZHL_DXGLB : BaseEFDao<SZHL_DXGL>
    {

        public string SendSMS(string telephone, string content, int ComId = 0)
        {
            string err = "";
            try
            {
                string dxqz = "企捷科技";
                decimal amcountmoney = 0;
                var qy = new JH_Auth_QYB().GetEntity(d => d.ComId == ComId);
                if (qy != null)
                {
                    dxqz = qy.DXQZ;
                    amcountmoney = qy.AccountMoney.HasValue ? qy.AccountMoney.Value : 0;
                }

                string[] tels = telephone.Trim().Replace(" ", "").Replace("\n", "").Replace("\r", "").Replace('，', ',').Split(',');

                //判断金额是否够
                decimal DXCost = decimal.Parse(CommonHelp.GetConfig("DXCost"));
                decimal amount = tels.Length * DXCost;
                if (ComId != 0 && amcountmoney < amount) //短信余额不足
                {
                    err = "短信余额不足";
                }
                else
                {
                    string re = "";
                    foreach (string tel in tels)
                    {
                        re = CommonHelp.SendDX(tel, content + "【" + dxqz + "】", "");
                    }

                    err = "发送成功";

                    //扣款
                    if (ComId != 0 && qy != null)
                    {
                        qy.AccountMoney = qy.AccountMoney - amount;
                        new JH_Auth_QYB().Update(qy);


                    }

                }
            }
            catch { }
            return err;
        }

    }
    #endregion







    #region 扩展字段
    public class JH_Auth_ExtendModeB : BaseEFDao<JH_Auth_ExtendMode>
    {
        //获取扩展字段的值
        public DataTable GetExtData(int? ComId, string FormCode, string DATAID, string PDID = "")
        {
            string strWhere = string.Empty;
            if (PDID != "") { strWhere = " and j.PDID='" + PDID + "'"; }
            return new JH_Auth_ExtendModeB().GetDTByCommand(string.Format("select j.ComId, j.ID, j.TableName, j.TableFiledColumn, j.TableFiledName, j.TableFileType, j.DefaultOption, j.DefaultValue, j.IsRequire, d.ExtendModeID, d.ID AS ExtID, d.DataID, d.ExtendDataValue from JH_Auth_ExtendMode j join JH_Auth_ExtendData d on j.ComId=d.ComId and j.ID=d.ExtendModeID where j.ComId='{0}' and j.TableName='{1}' and d.DataID='{2}' and d.ExtendDataValue<>'' and d.ExtendDataValue is not null  " + strWhere, ComId, FormCode, DATAID));
        }

        public DataTable GetExtDataAll(int? ComId, string FormCode, string DATAID, string PDID = "")
        {
            string strWhere = string.Empty;
            if (PDID != "") { strWhere = " and j.PDID='" + PDID + "'"; }
            return new JH_Auth_ExtendModeB().GetDTByCommand(string.Format("select j.ComId, j.ID, j.TableName, j.TableFiledColumn, j.TableFiledName, j.TableFileType, j.DefaultOption, j.DefaultValue, j.IsRequire, d.ExtendModeID, d.ID AS ExtID, d.DataID, d.ExtendDataValue from JH_Auth_ExtendMode j join JH_Auth_ExtendData d on j.ComId=d.ComId and j.ID=d.ExtendModeID where j.ComId='{0}' and j.TableName='{1}' and d.DataID='{2}' " + strWhere, ComId, FormCode, DATAID));
        }

        public DataTable GetExtColumnAll(int? ComId, string FormCode, string PDID = "")
        {
            string strWhere = string.Empty;
            if (PDID != "") { strWhere = " and j.PDID='" + PDID + "'"; }
            return new JH_Auth_ExtendModeB().GetDTByCommand(string.Format("select j.ComId, j.ID, j.TableName, j.TableFiledColumn, j.TableFiledName, j.TableFileType, j.DefaultOption, j.DefaultValue, j.IsRequire from JH_Auth_ExtendMode j where j.ComId='{0}' and j.TableName='{1}'" + strWhere, ComId, FormCode));
        }
    }
    public class JH_Auth_ExtendDataB : BaseEFDao<JH_Auth_ExtendData>
    {


        public string GetFiledValue(string FiledName, int pdid, int piid)
        {
            string value = "";
            JH_Auth_ExtendData model = new JH_Auth_ExtendDataB().GetEntities(d => d.PDID == pdid && d.DataID == piid && d.ExFiledColumn == FiledName).FirstOrDefault();
            if (model != null)
            {
                value = model.ExtendDataValue;
            }
            return value;
        }



        /// <summary>
        /// 获取导入excel的字段
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public List<IMPORTYZ> GetTable(string code, int comid)
        {
            string json = string.Empty;

            if (comid != 0)
            {
                json = json.Substring(0, json.Length - 1);

                DataTable dtExtColumn = new JH_Auth_ExtendModeB().GetExtColumnAll(comid, code);
                foreach (DataRow drExt in dtExtColumn.Rows)
                {
                    json = json + ",{\"Name\":\"" + drExt["TableFiledName"].ToString() + "\",\"Length\":\"0\",\"IsNull\":\"0\"}";
                }

                json = json + "]";
            }

            List<IMPORTYZ> cls = JsonConvert.DeserializeObject<List<IMPORTYZ>>(json);
            return cls;

        }



        public class IMPORTYZ
        {
            public string Name { get; set; }
            public int Length { get; set; }
            public int IsNull { get; set; }
            public string IsRepeat { get; set; }
            public string IsExist { get; set; }
        }



    }

    #endregion













    #endregion



}
