﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using QJY.Common;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.TenPay.V3;
using System;
using System.Configuration;
using System.Data;
using System.Xml.Linq;

namespace QJY.API
{
    public class WeiXinPay
    {
        public static void WXPayJsapi(ref Msg_Result msg, string userID, string orderID, string code, string userip, decimal ddje)
        {

            try
            {
                string wxappid = ConfigurationManager.AppSettings["wxappid"];
                string wxappsecret = ConfigurationManager.AppSettings["wxappsecret"];
                string wxmchid = ConfigurationManager.AppSettings["wxmchid"];
                string wxmchidkey = ConfigurationManager.AppSettings["wxmchidkey"];
                string TenPayV3Notify = ConfigurationManager.AppSettings["TenPayV3Notify"];


                TenPayV3Info ten = new TenPayV3Info(wxappid, wxappsecret, wxmchid, wxmchidkey, "", "", TenPayV3Notify, "");

                if (string.IsNullOrEmpty(code))
                {
                    msg.ErrorMsg = "您拒绝了授权！";
                    return;
                }

                if (string.IsNullOrEmpty(orderID))
                {
                    msg.ErrorMsg = "订单信息不存在！";
                    return;
                }
                DataTable dtReturn = new JH_Auth_BranchB().GetDTByCommand("select top 1 * from qj_sfjs_ysdata where id='" + orderID + "'");

                if (dtReturn.Rows.Count != 1)
                {
                    msg.ErrorMsg = "订单信息错误！";
                    return;
                }

                if (dtReturn.Rows[0]["id"].ToString() == "")
                {
                    msg.ErrorMsg = "订单信息错误！";
                    return;
                }
                //if (order.payStatus != 0)
                //{
                //    msg.ErrorMsg = "订单状态有误!或您已支付.";
                //    return;
                //}
                //通过，用code换取access_token
                CommonHelp.WriteLOG("3");

                var openIdResult = OAuthApi.GetAccessToken(ten.AppId, ten.AppSecret, code);
                CommonHelp.WriteLOG(JsonConvert.SerializeObject(openIdResult));

                if (openIdResult.errcode != ReturnCode.请求成功)
                {
                    msg.ErrorMsg = "错误：" + openIdResult.errmsg;
                    return;
                }

                string timeStamp = "";
                string nonceStr = "";
                string paySign = "";

                Random MyRandom = new Random();
                int RandomNum = MyRandom.Next(1001, 9999);
                int RandomNum2 = MyRandom.Next(1001, 9999);
                string sp_billno = DateTime.Now.ToString("HHssmmfff") + RandomNum.ToString() + RandomNum2.ToString();
                dtReturn.Rows[0]["BusinessNo"] = sp_billno;
                new JH_Auth_BranchB().ExsSclarSql("UPDATE qj_sfjs_ysdata SET BusinessNo='" + sp_billno + "',zfczr='" + userID + "' WHERE id='" + dtReturn.Rows[0]["ID"].ToString() + "'");
                CommonHelp.WriteLOG("5");

                //当前时间 yyyyMMdd
                string date = DateTime.Now.ToString("yyyyMMdd");


                RequestHandler packageReqHandler = new RequestHandler(null);
                packageReqHandler.Init();
                timeStamp = TenPayV3Util.GetTimestamp();
                nonceStr = TenPayV3Util.GetNoncestr();
                packageReqHandler.SetParameter("appid", ten.AppId);       //公众账号ID
                packageReqHandler.SetParameter("mch_id", ten.MchId);          //商户号
                packageReqHandler.SetParameter("nonce_str", nonceStr);                    //随机字符串
                packageReqHandler.SetParameter("body", "水费");    //商品信息
                packageReqHandler.SetParameter("out_trade_no", sp_billno);      //商家订单号
                packageReqHandler.SetParameter("total_fee", ((int)(ddje * 100)).ToString());                //商品金额,以分为单位(money * 100).ToString()
                packageReqHandler.SetParameter("spbill_create_ip", userip);  //用户的公网ip，不是商户服务器IP
                packageReqHandler.SetParameter("notify_url", ten.TenPayV3Notify);           //接收财付通通知的URL
                packageReqHandler.SetParameter("trade_type", Senparc.Weixin.TenPay.TenPayV3Type.JSAPI.ToString());                        //交易类型
                packageReqHandler.SetParameter("openid", openIdResult.openid);                      //用户的openId

                string sign = packageReqHandler.CreateMd5Sign("key", ten.Key);
                packageReqHandler.SetParameter("sign", sign);                //签名
                string data = packageReqHandler.ParseXML();
                CommonHelp.WriteLOG("6");

                var result = TenPayV3.Unifiedorder(data);
                var res = XDocument.Parse(result);
                CommonHelp.WriteLOG(res.ToString());

                string prepayId = res.Element("xml").Element("prepay_id").Value;

                //设置支付参数
                RequestHandler paySignReqHandler = new RequestHandler(null);
                paySignReqHandler.SetParameter("appId", ten.AppId);
                paySignReqHandler.SetParameter("timeStamp", timeStamp);
                paySignReqHandler.SetParameter("nonceStr", nonceStr);
                paySignReqHandler.SetParameter("package", string.Format("prepay_id={0}", prepayId));
                paySignReqHandler.SetParameter("signType", "MD5");
                paySign = paySignReqHandler.CreateMd5Sign("key", ten.Key);
                msg.Result = new JObject(
                        new JProperty("appId", ten.AppId),
                        new JProperty("timeStamp", timeStamp),
                        new JProperty("nonceStr", nonceStr),
                        new JProperty("package", string.Format("prepay_id={0}", prepayId)),
                        new JProperty("signType", "MD5"),
                        new JProperty("paySign", paySign)
                    );


            }
            catch (Exception ex)
            {
                string ss = ex.Message;
                CommonHelp.WriteLOG("缴费失败" + ex.Message);
                msg.ErrorMsg = "支付订单失败";
            }
        }

        public static string WXPayNotifyUrl()
        {
            try
            {
                ResponseHandler resHandler = new ResponseHandler(null);

                string return_code = resHandler.GetParameter("return_code");
                string return_msg = resHandler.GetParameter("return_msg");

                resHandler.SetKey(ConfigurationManager.AppSettings["wxmchidkey"]);
                //验证请求是否从微信发过来（安全）
                if (resHandler.IsTenpaySign())
                {

                    //正确的订单处理

                    //修改订单状态为已支付
                    string transaction_id = resHandler.GetParameter("transaction_id");
                    string out_trade_no = resHandler.GetParameter("out_trade_no"); //订单号码
                    string total_fee = resHandler.GetParameter("total_fee");  //订单金额

                    //CommonHelp.WriteLOG("反馈：订单号" + out_trade_no + "金额" + total_fee);

                    string ddje = (int.Parse(total_fee) / 100).ToString();


                    //更新订单支付金额和支付状态
                    new JH_Auth_UserB().ExsSclarSql(" UPDATE qj_sfjs_ysdata SET zfzt='已支付',zfsj=GETDATE(),zffs='微信支付' where BusinessNo='" + out_trade_no + "'");

                    //new JWGL_BMJF().SearchXsdjksbmupdate(out_trade_no, ddje, "");



                    //验证订单号
                    //BW_Order_Header order = new OrderService().getOrderByBNO(out_trade_no);
                    //if (order == null)
                    //{
                    //    throw new Exception("订单信息不存在!" + out_trade_no);
                    //}
                    //if (order.OrderID == "" || order.ActualAmount <= 0)
                    //{
                    //    throw new Exception("订单信息错误!" + out_trade_no);
                    //}
                    //if (order.Status != 0)
                    //{
                    //    throw new Exception("订单状态有误!或您已支付");
                    //}
                    //if (order.ActualAmount <= 0 || ((int)(order.ActualAmount * 100)).ToString() != total_fee)
                    //{
                    //    throw new Exception("订单金额有误!");
                    //}
                    //else//正常支付
                    //{
                    //    DbHelperSQLJW.ExecuteSql("UPDATE BW_Order_Header SET Status=1,payStatus = 1,PayTime=GETDATE(),Payment=3 WHERE OrderID='" + order.OrderID + "' ");
                    //}
                }

                string xml = string.Format(@"<xml>
                   <return_code><![CDATA[{0}]]></return_code>
                   <return_msg><![CDATA[{1}]]></return_msg>
                </xml>", return_code, return_msg);

                return xml;
            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG(ex.Message);
            }
            return "wrong";
        }
    }
}
