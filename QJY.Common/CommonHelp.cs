﻿using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using QJY.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Web;
using System.Xml;

namespace QJY.Common
{
    public class CommonHelp
    {


        #region EXCEL操作
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="table"></param>
        /// <param name="fileName"></param>
        public static MemoryStream RenderToExcel(DataTable table)
        {
            MemoryStream ms = new MemoryStream();

            using (table)
            {
                IWorkbook workbook = new HSSFWorkbook();
                ISheet sheet = workbook.CreateSheet();
                IRow headerRow = sheet.CreateRow(0);

                // handling header.
                foreach (DataColumn column in table.Columns)
                    headerRow.CreateCell(column.Ordinal).SetCellValue(column.Caption);//If Caption not set, returns the ColumnName value

                // handling value.
                int rowIndex = 1;

                foreach (DataRow row in table.Rows)
                {
                    IRow dataRow = sheet.CreateRow(rowIndex);

                    foreach (DataColumn column in table.Columns)
                    {
                        dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                    }

                    rowIndex++;
                }

                workbook.Write(ms);
                ms.Flush();
                ms.Position = 0;
            }

            //using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            //{
            //    byte[] data = ms.ToArray();

            //    fs.Write(data, 0, data.Length);
            //    fs.Flush();
            //    data = null;
            //}
            return ms;
        }
        public static HSSFWorkbook ExportToExcel(DataTable dt)
        {

            HSSFWorkbook workbook = new HSSFWorkbook();
            if (dt.Rows.Count > 0)
            {
                ISheet sheet = workbook.CreateSheet("Sheet1");

                ICellStyle HeadercellStyle = workbook.CreateCellStyle();
                HeadercellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                HeadercellStyle.Alignment = HorizontalAlignment.Center;
                HeadercellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;
                HeadercellStyle.FillPattern = FillPattern.SolidForeground;
                HeadercellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;

                //字体
                NPOI.SS.UserModel.IFont headerfont = workbook.CreateFont();
                headerfont.Boldweight = (short)FontBoldWeight.Bold;
                headerfont.FontHeightInPoints = 12;
                HeadercellStyle.SetFont(headerfont);


                //用column name 作为列名
                int icolIndex = 0;
                IRow headerRow = sheet.CreateRow(0);
                foreach (DataColumn dc in dt.Columns)
                {
                    ICell cell = headerRow.CreateCell(icolIndex);
                    cell.SetCellValue(dc.ColumnName);
                    cell.CellStyle = HeadercellStyle;
                    icolIndex++;
                }

                ICellStyle cellStyle = workbook.CreateCellStyle();

                //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
                cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
                cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;


                NPOI.SS.UserModel.IFont cellfont = workbook.CreateFont();
                cellfont.Boldweight = (short)FontBoldWeight.Normal;
                cellStyle.SetFont(cellfont);

                //建立内容行
                int iRowIndex = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    int iCellIndex = 0;
                    IRow irow = sheet.CreateRow(iRowIndex + 1);
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        string strsj = string.Empty;
                        if (dr[i] != null)
                        {
                            strsj = dr[i].ToString();
                        }
                        ICell cell = irow.CreateCell(iCellIndex);
                        cell.SetCellValue(strsj);
                        cell.CellStyle = cellStyle;
                        iCellIndex++;
                    }
                    iRowIndex++;
                }

                //自适应列宽度
                for (int i = 0; i < icolIndex; i++)
                {
                    sheet.AutoSizeColumn(i);
                }



                //using (MemoryStream ms = new MemoryStream())
                //{
                //    workbook.Write(ms);

                //    HttpContext curContext = HttpContext.Current;


                //    // 设置编码和附件格式
                //    curContext.Response.ContentType = "application/vnd.ms-excel";
                //    curContext.Response.ContentEncoding = Encoding.UTF8;
                //    curContext.Response.Charset = "";
                //    curContext.Response.AppendHeader("Content-Disposition",
                //        "attachment;filename=" + HttpUtility.UrlEncode(Name + "_导出文件_" + DateTime.Now.Ticks + ".xls", Encoding.UTF8));

                //    curContext.Response.BinaryWrite(ms.GetBuffer());

                //    workbook = null;
                //    ms.Close();
                //    ms.Dispose();

                //    curContext.Response.End();
                //}
            }
            return workbook;

        }

        public static void ExprotToExcel(DataTable dt, string strFilePath)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();

            ISheet sheet = workbook.CreateSheet("Sheet1");

            ICellStyle HeadercellStyle = workbook.CreateCellStyle();
            HeadercellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.Alignment = HorizontalAlignment.Center;
            HeadercellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;
            HeadercellStyle.FillPattern = FillPattern.SolidForeground;
            HeadercellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;

            //字体
            NPOI.SS.UserModel.IFont headerfont = workbook.CreateFont();
            headerfont.Boldweight = (short)FontBoldWeight.Bold;
            headerfont.FontHeightInPoints = 12;
            HeadercellStyle.SetFont(headerfont);


            //用column name 作为列名
            int icolIndex = 0;
            IRow headerRow = sheet.CreateRow(0);
            foreach (DataColumn dc in dt.Columns)
            {
                ICell cell = headerRow.CreateCell(icolIndex);
                cell.SetCellValue(dc.ColumnName);
                cell.CellStyle = HeadercellStyle;
                icolIndex++;
            }

            ICellStyle cellStyle = workbook.CreateCellStyle();

            //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
            cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
            cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;


            NPOI.SS.UserModel.IFont cellfont = workbook.CreateFont();
            cellfont.Boldweight = (short)FontBoldWeight.Normal;
            cellStyle.SetFont(cellfont);

            //建立内容行
            int iRowIndex = 0;
            foreach (DataRow dr in dt.Rows)
            {
                int iCellIndex = 0;
                IRow irow = sheet.CreateRow(iRowIndex + 1);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string strsj = string.Empty;
                    if (dr[i] != null)
                    {
                        strsj = dr[i].ToString();
                    }
                    ICell cell = irow.CreateCell(iCellIndex);
                    cell.SetCellValue(strsj);
                    cell.CellStyle = cellStyle;
                    iCellIndex++;
                }
                iRowIndex++;
            }

            //自适应列宽度
            for (int i = 0; i < icolIndex; i++)
            {
                sheet.AutoSizeColumn(i);
            }


            FileStream fs2 = File.Create(strFilePath);
            workbook.Write(fs2);
            fs2.Close();

        }
        public static void ExprotToExcel(DataTable dt, string strFilePath, DataTable dtMergeCells)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("Sheet1");

            ICellStyle HeadercellStyle = workbook.CreateCellStyle();
            HeadercellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            HeadercellStyle.Alignment = HorizontalAlignment.Center;
            HeadercellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;
            HeadercellStyle.FillPattern = FillPattern.SolidForeground;
            HeadercellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.SkyBlue.Index;

            //字体
            NPOI.SS.UserModel.IFont headerfont = workbook.CreateFont();
            headerfont.Boldweight = (short)FontBoldWeight.Bold;
            headerfont.FontHeightInPoints = 12;
            HeadercellStyle.SetFont(headerfont);


            //用column name 作为列名
            int icolIndex = 0;
            IRow headerRow = sheet.CreateRow(0);
            foreach (DataColumn dc in dt.Columns)
            {
                ICell cell = headerRow.CreateCell(icolIndex);
                cell.SetCellValue(dc.ColumnName);
                cell.CellStyle = HeadercellStyle;
                icolIndex++;
            }

            ICellStyle cellStyle = workbook.CreateCellStyle();

            //为避免日期格式被Excel自动替换，所以设定 format 为 『@』 表示一率当成text來看
            cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");
            cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyle.VerticalAlignment = VerticalAlignment.Center;
            cellStyle.Alignment = HorizontalAlignment.Center;

            NPOI.SS.UserModel.IFont cellfont = workbook.CreateFont();
            cellfont.Boldweight = (short)FontBoldWeight.Normal;
            cellStyle.SetFont(cellfont);

            //建立内容行
            int iRowIndex = 0;
            foreach (DataRow dr in dt.Rows)
            {
                int iCellIndex = 0;
                IRow irow = sheet.CreateRow(iRowIndex + 1);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string strsj = string.Empty;
                    if (dr[i] != null)
                    {
                        strsj = dr[i].ToString().Trim();
                    }
                    ICell cell = irow.CreateCell(iCellIndex);
                    cell.SetCellValue(strsj);
                    cell.CellStyle = cellStyle;
                    iCellIndex++;
                }
                iRowIndex++;
            }

            //自适应列宽度
            for (int i = 0; i < icolIndex; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            if (dtMergeCells != null)
            {
                for (int i = 0; i < dtMergeCells.Rows.Count; i++)
                {
                    int row = int.Parse(dtMergeCells.Rows[i]["row"].ToString());
                    int rowend = int.Parse(dtMergeCells.Rows[i]["rowend"].ToString());
                    int col = int.Parse(dtMergeCells.Rows[i]["col"].ToString());
                    int colend = int.Parse(dtMergeCells.Rows[i]["colend"].ToString());
                    if (row != rowend)
                    {
                        sheet.AddMergedRegion(new CellRangeAddress(row, rowend, col, colend));
                    }
                }
            }

          
            //sheet.AddMergedRegion(new CellRangeAddress(1, 6, 2, 2));
            //sheet.AddMergedRegion(new CellRangeAddress(7, 12, 2, 2))
            FileStream fs2 = File.Create(strFilePath);
            workbook.Write(fs2);
            fs2.Close();

        }
        public DataTable ExcelToTable(Stream stream, int headrow, string suffix)
        {
            DataTable dt = new DataTable();

            IWorkbook workbook = null;
            if (suffix == "xlsx") // 2007版本
            {
                workbook = new XSSFWorkbook(stream);
            }
            else if (suffix == "xls") // 2003版本
            {
                workbook = new HSSFWorkbook(stream);
            }

            //获取excel的第一个sheet
            ISheet sheet = workbook.GetSheetAt(0);

            //获取sheet的第一行
            IRow headerRow = sheet.GetRow(headrow);

            //一行最后一个方格的编号 即总的列数
            int cellCount = headerRow.LastCellNum;
            //最后一列的标号  即总的行数
            int rowCount = sheet.LastRowNum;
            //列名
            for (int i = 0; i < cellCount; i++)
            {
                dt.Columns.Add(headerRow.GetCell(i).ToString());
            }

            for (int i = (sheet.FirstRowNum + headrow + 1); i <= sheet.LastRowNum; i++)
            {
                DataRow dr = dt.NewRow();

                IRow row = sheet.GetRow(i);
                for (int j = row.FirstCellNum; j < cellCount; j++)
                {
                    if (row.GetCell(j) != null)
                    {
                        dr[j] = row.GetCell(j).ToString().Trim();
                    }
                }

                dt.Rows.Add(dr);
            }

            sheet = null;
            workbook = null;

            return dt;
        }

        /// <summary>
        /// 合同相同单元格
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="columnIndex"></param>
        private static void MergeCol(ISheet sheet, int columnIndex, int startRow = 1)
        {
            //开始 要合并的内容为空
            var previous = "";
            //startRow  是你Excel 数据是 第几行开始的
            for (int rowNum = startRow; rowNum <= sheet.LastRowNum; rowNum++)
            {
                //获取 指定行，指定列的 数据内容
                var current = sheet.GetRow(rowNum).GetCell(columnIndex).StringCellValue;
                // 判断 获取到的内容是否和 上一列 相等
                if (current.Equals(previous))
                { continue; }
                else
                {
                    // 第一级 合并。
                    //将获取到的 内容 赋值到 previous 
                    previous = current;
                    // 判断开始行，是否小于 循环的行数。
                    if (startRow < rowNum)
                    {
                        //第二级 合并
                        var celltext = "";
                        var startAM = startRow;
                        for (int i = startRow; i <= rowNum; i++)
                        {
                            var endtext = sheet.GetRow(i).GetCell(1).StringCellValue;
                            if (celltext.Equals(endtext))
                            { continue; }
                            else
                            {
                                celltext = endtext;
                                if (startAM < i)
                                {
                                    //　CellRangeAddress（起始行号，终止行号， 起始列号，终止列号）
                                    //这里 终止行号 -1  原因是：上面循环判断时 内容不一样的  才进行 合并，
                                    //这时 行数 i 内容 已经不一样，所以 需要减去 1  进行合并

                                    //sheet.AddMergedRegion(new CellRangeAddress((startAM, i - 1, columnIndex + 1, columnIndex + 1));
                                }
                                // 将 当前行数，进行赋值给 启始行数。
                                startAM = i;
                            }
                        }
                        sheet.AddMergedRegion(new CellRangeAddress(startRow, rowNum - 1, columnIndex, columnIndex));
                    }
                    startRow = rowNum;
                }
            }
        }



        /// <summary>
        /// 根据模版导出Excel -- 特别处理,每个分组带合计
        /// </summary>
        /// <param name="source">源DataTable</param>
        /// <param name="cellKeys">需要导出的对应的列字段 例：string[] cellKeys = { "Date","Remarks" };</param>
        /// <param name="strFileName">要保存的文件名称（包含后缀） 例："要保存的文件名.xls"</param>
        /// <param name="templateFile">模版文件名（包含路径后缀） 例："模板文件名.xls"</param>
        /// <param name="rowIndex">从第几行开始创建数据行,第一行为0</param>
        /// <param name="mergeColumns">值相同时，可合并的前几列 最多支持2列 1=只合并第一列，2=判断前2列</param>
        /// <param name="isConver">是否覆盖数据，=false，将把原数据下移。=true，将覆盖插入行后面的数据</param>
        /// <param name="isTotal">是否带小计/合计项</param>
        /// <param name="addAllTotal">是否添加总计项</param>
        /// <returns>是否导出成功</returns>
        public static bool Export2Template2(DataTable source, string[] cellKeys, string strFileName, string templateFile, int rowIndex, int mergeColumns, bool isConver, bool isTotal, bool addAllTotal)
        {
            bool bn = false;
            int cellCount = cellKeys.Length; //总列数，第一列为0
                                             // IWorkbook workbook = null;
            HSSFWorkbook workbook = null;
            string temp0 = "", temp1 = "";
            int start0 = 0, start1 = 0; // 记录1，2列值相同的开始序号
            int end0 = 0, end1 = 0;// 记录1，2列值相同的结束序号

            try
            {
                using (FileStream file = new FileStream(templateFile, FileMode.Open, FileAccess.Read))
                {
                    workbook = new HSSFWorkbook(file);
                }

                #region 定义四类数据的单元格样式
                // 内容数据格式 -- 数值
                ICellStyle styleNum = workbook.CreateCellStyle();
                styleNum.BorderBottom = BorderStyle.Thin;
                styleNum.BorderLeft = BorderStyle.Thin;
                styleNum.BorderRight = BorderStyle.Thin;
                styleNum.BorderTop = BorderStyle.Thin;
                // styleNum.VerticalAlignment = VerticalAlignment.Center;
                // styleNum.Alignment = HorizontalAlignment.Center;

                // 内容数据格式 -- 字符串（做居中处理）
                ICellStyle styleStr = workbook.CreateCellStyle();
                styleStr.BorderBottom = BorderStyle.Thin;
                styleStr.BorderLeft = BorderStyle.Thin;
                styleStr.BorderRight = BorderStyle.Thin;
                styleStr.BorderTop = BorderStyle.Thin;
                styleStr.VerticalAlignment = VerticalAlignment.Center;
                styleStr.Alignment = HorizontalAlignment.Center;

                // 汇总数据格式 -- 数值
                ICellStyle styleTotalNum = workbook.CreateCellStyle();
                styleTotalNum.BorderBottom = BorderStyle.Thin;
                styleTotalNum.BorderLeft = BorderStyle.Thin;
                styleTotalNum.BorderRight = BorderStyle.Thin;
                styleTotalNum.BorderTop = BorderStyle.Thin;
                styleTotalNum.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                styleTotalNum.FillPattern = FillPattern.SolidForeground;
                styleTotalNum.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Red.Index;
                // 设置字体颜色
                HSSFFont ffont0 = (HSSFFont)workbook.CreateFont();
                // ffont0.FontHeight = 14 * 14;
                // ffont0.FontName = "宋体";
                ffont0.IsBold = true;
                //ffont0.Color = HSSFColor.Red.Index;
                styleTotalNum.SetFont(ffont0);

                // 汇总数据格式 -- 字符串（做居中处理）
                ICellStyle styleTotalStr = workbook.CreateCellStyle();
                styleTotalStr.BorderBottom = BorderStyle.Thin;
                styleTotalStr.BorderLeft = BorderStyle.Thin;
                styleTotalStr.BorderRight = BorderStyle.Thin;
                styleTotalStr.BorderTop = BorderStyle.Thin;
                styleTotalStr.VerticalAlignment = VerticalAlignment.Center;
                styleTotalStr.Alignment = HorizontalAlignment.Center;
                styleTotalStr.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                styleTotalStr.FillPattern = FillPattern.SolidForeground;
                // 设置字体颜色
                HSSFFont ffont1 = (HSSFFont)workbook.CreateFont();
                // ffont1.FontHeight = 14 * 14;
                // ffont1.FontName = "宋体";
                ffont1.IsBold = true;
                //ffont.Color = HSSFColor.Red.Index;
                styleTotalStr.SetFont(ffont1);
                #endregion

                ISheet sheet = workbook.GetSheetAt(0); // 打开第一个sheet页
                if (sheet != null && source != null && source.Rows.Count > 0) // 模板内容为空，不做处理
                {
                    IRow row;
                    for (int i = 0, len = source.Rows.Count; i < len; i++)
                    {
                        if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                        #region 第一行，写入数据后，对变量赋初值
                        if (i == 0) // 第一行，赋初值
                        {
                            row = sheet.CreateRow(rowIndex);
                            #region 创建列并插入数据
                            //创建列并插入数据
                            for (int index = 0; index < cellCount; index++)
                            {
                                ICell cell = row.CreateCell(index);

                                string strValue = !(source.Rows[i][cellKeys[index]] is DBNull) ? source.Rows[i][cellKeys[index]].ToString() : string.Empty;
                                // 其它列数据，数值进行汇总
                                switch (source.Columns[cellKeys[index]].DataType.ToString())
                                {
                                    case "System.Int16": //整型
                                    case "System.Int32":
                                    case "System.Int64":
                                    case "System.Byte":
                                        int intV = 0;
                                        int.TryParse(strValue, out intV);
                                        cell.CellStyle = styleNum; // 设置格式
                                        cell.SetCellValue(intV);
                                        break;
                                    case "System.Decimal": //浮点型
                                    case "System.Double":
                                    case "System.Single":
                                        double doubV = 0;
                                        double.TryParse(strValue, out doubV);
                                        cell.CellStyle = styleNum; // 设置格式
                                        cell.SetCellValue(doubV);
                                        break;
                                    default:
                                        cell.CellStyle = styleStr; // 设置格式
                                        cell.SetCellValue(strValue);
                                        break;
                                }
                            }
                            #endregion

                            if (mergeColumns > 0)
                            {
                                temp0 = source.Rows[i][cellKeys[0]].ToString(); // 保存第1列值
                                start0 = rowIndex;
                                end0 = rowIndex;
                            }
                            if (mergeColumns > 1)
                            {
                                temp1 = source.Rows[i][cellKeys[1]].ToString(); // 保存第2列值     
                                start1 = rowIndex;
                                end1 = rowIndex;
                            }

                            rowIndex++;
                            continue;
                        }
                        #endregion

                        // 不是第一行数据的处理
                        // 判断1列值变化没
                        string cellText0 = source.Rows[i][cellKeys[0]].ToString();
                        if (temp0 != cellText0) // 第1列值有变化
                        {
                            #region 第2列要合并
                            if (mergeColumns > 1) // 第2列要合并
                            {
                                if (start1 != end1) // 开始行和结束行不相同，才进行合并
                                {
                                    CellRangeAddress region1 = new CellRangeAddress(start1, end1, 1, 1); // 合并第二列
                                    sheet.AddMergedRegion(region1);
                                }

                                #region 第2列加小计
                                if (isTotal) // 加小计
                                {
                                    if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                                    IRow rowTotal1 = sheet.CreateRow(rowIndex);
                                    //创建列并插入数据
                                    #region 插入小计数据
                                    for (int index = 0; index < cellCount; index++)
                                    {
                                        object obj1;
                                        ICell newcell = rowTotal1.CreateCell(index);
                                        if (index == 0) //第1列
                                        {
                                            newcell.CellStyle = styleTotalStr;
                                            newcell.SetCellValue(temp0);
                                            continue;
                                        }
                                        if (index == 1) // 第2列
                                        {
                                            newcell.CellStyle = styleTotalStr;
                                            newcell.SetCellValue("小计");
                                            continue;
                                        }

                                        // 其它列数据，数值进行汇总
                                        switch (source.Columns[cellKeys[index]].DataType.ToString())
                                        {
                                            case "System.Int16": //整型
                                            case "System.Int32":
                                            case "System.Int64":
                                            case "System.Byte":
                                                obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' and {2} = '{3}' ", cellKeys[0], temp0, cellKeys[1], temp1));
                                                int intV = 0;
                                                int.TryParse(obj1.ToString(), out intV);
                                                newcell.CellStyle = styleTotalNum;
                                                newcell.SetCellValue(intV);
                                                break;
                                            case "System.Decimal": //浮点型
                                            case "System.Double":
                                            case "System.Single":
                                                obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' and {2} = '{3}' ", cellKeys[0], temp0, cellKeys[1], temp1));
                                                double doubV = 0;
                                                double.TryParse(obj1.ToString(), out doubV);
                                                newcell.CellStyle = styleTotalNum;
                                                newcell.SetCellValue(doubV);
                                                break;
                                            default:
                                                newcell.CellStyle = styleTotalStr;
                                                newcell.SetCellValue("");
                                                break;
                                        }
                                    }
                                    #endregion

                                    // 合并小计
                                    CellRangeAddress region0 = new CellRangeAddress(rowIndex, rowIndex, 1, 2); // 合并小计
                                    sheet.AddMergedRegion(region0);

                                }
                                #endregion
                                temp1 = source.Rows[i][cellKeys[1]].ToString();
                                end0++;
                                rowIndex++;
                            }
                            #endregion

                            #region 第1列要合并
                            if (mergeColumns > 0) // 第1列要合并
                            {
                                if (start0 != end0) // 开始行和结束行不相同，才进行合并
                                {
                                    CellRangeAddress region0 = new CellRangeAddress(start0, end0, 0, 0); // 合并第二列
                                    sheet.AddMergedRegion(region0);
                                }

                                #region 第1列加合计
                                if (isTotal) // 加合计
                                {
                                    if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                                    IRow rowTotal0 = sheet.CreateRow(rowIndex);
                                    //创建列并插入数据
                                    #region 加合计列
                                    for (int index = 0; index < cellCount; index++)
                                    {
                                        object obj1;
                                        ICell newcell = rowTotal0.CreateCell(index);
                                        if (index == 0)
                                        {
                                            newcell.CellStyle = styleTotalStr;
                                            newcell.SetCellValue("合计"); //第1列
                                            continue;
                                        }
                                        if (index == 1)
                                        {
                                            newcell.CellStyle = styleTotalStr;
                                            newcell.SetCellValue(""); // 第2列
                                            continue;
                                        }

                                        switch (source.Columns[cellKeys[index]].DataType.ToString())
                                        {
                                            case "System.Int16": //整型
                                            case "System.Int32":
                                            case "System.Int64":
                                            case "System.Byte":
                                                obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' ", cellKeys[0], temp0));
                                                int intV = 0;
                                                int.TryParse(obj1.ToString(), out intV);
                                                newcell.CellStyle = styleTotalNum;
                                                newcell.SetCellValue(intV);
                                                break;
                                            case "System.Decimal": //浮点型
                                            case "System.Double":
                                            case "System.Single":
                                                obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' ", cellKeys[0], temp0));
                                                double doubV = 0;
                                                double.TryParse(obj1.ToString(), out doubV);
                                                newcell.CellStyle = styleTotalNum;
                                                newcell.SetCellValue(doubV);
                                                break;
                                            default:
                                                newcell.CellStyle = styleTotalStr;
                                                newcell.SetCellValue("");
                                                break;
                                        }
                                    }
                                    #endregion

                                    // 合并合计
                                    CellRangeAddress region0 = new CellRangeAddress(rowIndex, rowIndex, 0, 2); // 合并合计
                                    sheet.AddMergedRegion(region0);

                                    end0++;
                                    rowIndex++;
                                }
                                #endregion
                                temp0 = cellText0;
                            }
                            #endregion

                            // 重新赋值
                            start0 = rowIndex;
                            end0 = rowIndex;
                            start1 = rowIndex;
                            end1 = rowIndex;
                        }
                        else // 第1列值没有变化
                        {
                            end0++;
                            // 判断第2列是否有变化
                            string cellText1 = source.Rows[i][cellKeys[1]].ToString();
                            if (cellText1 != temp1) // 第1列没变，第2列变化
                            {
                                #region 第2列要合并
                                if (mergeColumns > 1) // 第2列要合并
                                {
                                    if (start1 != end1) // 开始行和结束行不相同，才进行合并
                                    {
                                        CellRangeAddress region1 = new CellRangeAddress(start1, end1, 1, 1); // 合并第二列
                                        sheet.AddMergedRegion(region1);
                                    }

                                    #region 第2列加小计
                                    if (isTotal) // 加小计
                                    {
                                        if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                                        IRow rowTotal1 = sheet.CreateRow(rowIndex);
                                        //创建列并插入数据
                                        #region 插入小计数据
                                        for (int index = 0; index < cellCount; index++)
                                        {
                                            object obj1;
                                            ICell newcell = rowTotal1.CreateCell(index);
                                            if (index == 0) //第1列
                                            {
                                                newcell.CellStyle = styleTotalStr;
                                                newcell.SetCellValue(temp0);
                                                continue;
                                            }
                                            if (index == 1) // 第2列
                                            {
                                                newcell.CellStyle = styleTotalStr;
                                                newcell.SetCellValue("小计");
                                                continue;
                                            }

                                            // 其它列数据，数值进行汇总
                                            switch (source.Columns[cellKeys[index]].DataType.ToString())
                                            {
                                                case "System.Int16": //整型
                                                case "System.Int32":
                                                case "System.Int64":
                                                case "System.Byte":
                                                    obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' and {2} = '{3}' ", cellKeys[0], temp0, cellKeys[1], temp1));
                                                    int intV = 0;
                                                    int.TryParse(obj1.ToString(), out intV);
                                                    newcell.CellStyle = styleTotalNum;
                                                    newcell.SetCellValue(intV);
                                                    break;
                                                case "System.Decimal": //浮点型
                                                case "System.Double":
                                                case "System.Single":
                                                    obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' and {2} = '{3}' ", cellKeys[0], temp0, cellKeys[1], temp1));
                                                    double doubV = 0;
                                                    double.TryParse(obj1.ToString(), out doubV);
                                                    newcell.CellStyle = styleTotalNum;
                                                    newcell.SetCellValue(doubV);
                                                    break;
                                                default:
                                                    newcell.CellStyle = styleTotalStr;
                                                    newcell.SetCellValue("");
                                                    break;
                                            }
                                        }
                                        #endregion
                                        // 合并小计
                                        CellRangeAddress region0 = new CellRangeAddress(rowIndex, rowIndex, 1, 2); // 合并小计
                                        sheet.AddMergedRegion(region0);

                                        end0++;
                                        rowIndex++;
                                    }
                                    temp1 = cellText1; // 要合并，才进行重新赋值
                                    start1 = rowIndex;
                                    end1 = rowIndex;
                                    #endregion
                                }
                                #endregion
                            }
                            else // 第1列值没变，第2列也没变
                                end1++;
                        }

                        // 插入当前数据
                        row = sheet.CreateRow(rowIndex);
                        #region 创建行并插入当前记录的数据
                        //创建行并插入当前记录的数据
                        for (int index = 0; index < cellCount; index++)
                        {
                            ICell cell = row.CreateCell(index);
                            string strValue = !(source.Rows[i][cellKeys[index]] is DBNull) ? source.Rows[i][cellKeys[index]].ToString() : string.Empty; // 取值
                            switch (source.Columns[cellKeys[index]].DataType.ToString())
                            {
                                case "System.Int16": //整型
                                case "System.Int32":
                                case "System.Int64":
                                case "System.Byte":
                                    int intV = 0;
                                    int.TryParse(strValue, out intV);
                                    cell.CellStyle = styleNum;
                                    cell.SetCellValue(intV);
                                    break;
                                case "System.Decimal": //浮点型
                                case "System.Double":
                                case "System.Single":
                                    double doubV = 0;
                                    double.TryParse(strValue, out doubV);
                                    cell.CellStyle = styleNum;
                                    cell.SetCellValue(doubV);
                                    break;
                                default:
                                    cell.CellStyle = styleStr;
                                    cell.SetCellValue(strValue);
                                    break;
                            }
                        }
                        #endregion
                        // 下移一行
                        rowIndex++;
                    }

                    // 最后一条记录的合计
                    #region 对第2列进行合并
                    if (mergeColumns > 1) // 对第2列合并
                    {
                        if (start1 != end1) // 开始行和结束行不等，进行合并
                        {
                            CellRangeAddress region1 = new CellRangeAddress(start1, end1, 1, 1); // 合并第二列
                            sheet.AddMergedRegion(region1);
                        }

                        #region 第2列加小计
                        if (isTotal) // 加小计
                        {
                            if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                            IRow rowTotal1 = sheet.CreateRow(rowIndex);
                            //创建列并插入数据
                            #region 插入小计数据
                            for (int index = 0; index < cellCount; index++)
                            {
                                object obj1;
                                ICell newcell = rowTotal1.CreateCell(index);
                                #region 列值处理
                                if (index == 0) //第1列
                                {
                                    newcell.CellStyle = styleTotalStr;
                                    newcell.SetCellValue(temp0);
                                    continue;
                                }
                                if (index == 1) // 第2列
                                {
                                    newcell.CellStyle = styleTotalStr;
                                    newcell.SetCellValue("小计");
                                    continue;
                                }

                                // 其它列数据，数值进行汇总
                                switch (source.Columns[cellKeys[index]].DataType.ToString())
                                {
                                    case "System.Int16": //整型
                                    case "System.Int32":
                                    case "System.Int64":
                                    case "System.Byte":
                                        obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' and {2} = '{3}' ", cellKeys[0], temp0, cellKeys[1], temp1));
                                        int intV = 0;
                                        int.TryParse(obj1.ToString(), out intV);
                                        newcell.CellStyle = styleTotalNum;
                                        newcell.SetCellValue(intV);
                                        break;
                                    case "System.Decimal": //浮点型
                                    case "System.Double":
                                    case "System.Single":
                                        obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' and {2} = '{3}' ", cellKeys[0], temp0, cellKeys[1], temp1));
                                        double doubV = 0;
                                        double.TryParse(obj1.ToString(), out doubV);
                                        newcell.CellStyle = styleTotalNum;
                                        newcell.SetCellValue(doubV);
                                        break;
                                    default:
                                        newcell.CellStyle = styleTotalStr;
                                        newcell.SetCellValue("");
                                        break;
                                }
                                #endregion
                            }
                            #endregion
                            // 合并小计
                            CellRangeAddress region0 = new CellRangeAddress(rowIndex, rowIndex, 1, 2); // 合并小计
                            sheet.AddMergedRegion(region0);

                            rowIndex++;
                            end0++;
                        }
                        #endregion
                    }
                    #endregion

                    #region 对第1列合并
                    if (mergeColumns > 0) // 对第1列合并
                    {
                        if (start0 != end0) // 开始行和结束行不等，进行合并
                        {
                            CellRangeAddress region1 = new CellRangeAddress(start0, end0, 0, 0); // 合并第二列
                            sheet.AddMergedRegion(region1);
                        }

                        #region 第1列加合计
                        if (isTotal) // 加合计
                        {
                            if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                            IRow rowTotal0 = sheet.CreateRow(rowIndex);
                            //创建列并插入数据
                            #region 插入合计数据
                            for (int index = 0; index < cellCount; index++)
                            {
                                object obj1;
                                ICell newcell = rowTotal0.CreateCell(index);
                                #region 列值处理
                                if (index == 0) //第1列
                                {
                                    newcell.CellStyle = styleTotalStr;
                                    newcell.SetCellValue("合计");
                                    continue;
                                }
                                if (index == 1) // 第2列
                                {
                                    newcell.CellStyle = styleTotalStr;
                                    newcell.SetCellValue("");
                                    continue;
                                }

                                // 其它列数据，数值进行汇总
                                switch (source.Columns[cellKeys[index]].DataType.ToString())
                                {
                                    case "System.Int16": //整型
                                    case "System.Int32":
                                    case "System.Int64":
                                    case "System.Byte":
                                        obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' ", cellKeys[0], temp0));
                                        int intV = 0;
                                        newcell.CellStyle = styleTotalNum;
                                        int.TryParse(obj1.ToString(), out intV);
                                        newcell.SetCellValue(intV);
                                        break;
                                    case "System.Decimal": //浮点型
                                    case "System.Double":
                                    case "System.Single":
                                        obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), string.Format("{0} = '{1}' ", cellKeys[0], temp0));
                                        double doubV = 0;
                                        double.TryParse(obj1.ToString(), out doubV);
                                        newcell.CellStyle = styleTotalNum;
                                        newcell.SetCellValue(doubV);
                                        break;
                                    default:
                                        newcell.CellStyle = styleTotalStr;
                                        newcell.SetCellValue("");
                                        break;
                                }
                                #endregion
                            }
                            #endregion

                            // 合并合计
                            CellRangeAddress region0 = new CellRangeAddress(rowIndex, rowIndex, 0, 2); // 合并合计
                            sheet.AddMergedRegion(region0);

                        }
                        rowIndex++;
                        #endregion
                    }
                    #endregion



                    #region 进行汇总 - 加总计     
                    if (addAllTotal) // 加总计
                    {
                        if (!isConver) sheet.ShiftRows(rowIndex, sheet.LastRowNum, 1, true, false); // 不覆盖，数据向下移

                        IRow rowTotal0 = sheet.CreateRow(rowIndex);
                        //创建列并插入数据
                        #region 插入总计数据
                        for (int index = 0; index < cellCount; index++)
                        {
                            object obj1;
                            ICell newcell = rowTotal0.CreateCell(index);
                            #region 列值处理
                            if (index == 0) //第1列
                            {
                                newcell.CellStyle = styleTotalStr;
                                newcell.SetCellValue("总计");
                                continue;
                            }
                            if (index == 1) // 第2列
                            {
                                newcell.CellStyle = styleTotalStr;
                                newcell.SetCellValue("");
                                continue;
                            }

                            // 其它列数据，数值进行汇总
                            switch (source.Columns[cellKeys[index]].DataType.ToString())
                            {
                                case "System.Int16": //整型
                                case "System.Int32":
                                case "System.Int64":
                                case "System.Byte":
                                    obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), "");
                                    int intV = 0;
                                    int.TryParse(obj1.ToString(), out intV);
                                    newcell.CellStyle = styleTotalNum;
                                    newcell.SetCellValue(intV);
                                    break;
                                case "System.Decimal": //浮点型
                                case "System.Double":
                                case "System.Single":
                                    obj1 = source.Compute(string.Format("sum({0})", cellKeys[index]), "");
                                    double doubV = 0;
                                    double.TryParse(obj1.ToString(), out doubV);
                                    newcell.CellStyle = styleTotalNum;
                                    newcell.SetCellValue(doubV);
                                    break;
                                default:
                                    newcell.CellStyle = styleTotalStr;
                                    newcell.SetCellValue("");
                                    break;
                            }
                            #endregion
                        }
                        #endregion

                        // 合并总计
                        CellRangeAddress region0 = new CellRangeAddress(rowIndex, rowIndex, 0, 2); // 合并总计
                        sheet.AddMergedRegion(region0);

                    }
                    #endregion

                }
                return Save2Xls(strFileName, workbook); // 保存为xls文件
            }
            catch (Exception ex)
            {
                // FileHelper.WriteLine(logfile, "处理数据异常：" + ex.Message);
                // msg = ex.Message;
            }
            return bn;
        }
        public static bool Save2Xls(string fileName, IWorkbook workbook)
        {
            bool bn = false;
            try
            {
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);

                MemoryStream ms = new MemoryStream();
                workbook.Write(ms);
                BinaryWriter w = new BinaryWriter(fs);
                w.Write(ms.ToArray());
                fs.Close();
                ms.Close();

                bn = true;
            }
            catch (Exception ex)
            {
                //FileHelper.WriteLine(logfile, "保存文件异常：" + ex.Message);
            }
            return bn;
        }


        #endregion


        #region 请求操作








        public static string PostWebRequest(string postUrl, string paramData, Encoding dataEncode)
        {
            string ret = string.Empty;
            try
            {
                byte[] byteArray = dataEncode.GetBytes(paramData); //转化
                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(new Uri(postUrl));
                webReq.Method = "POST";
                webReq.ContentType = "application/json; charset=UTF-8";

                webReq.ContentLength = byteArray.Length;
                Stream newStream = webReq.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                newStream.Close();
                HttpWebResponse response = (HttpWebResponse)webReq.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.Default);
                ret = sr.ReadToEnd();
                sr.Close();
                response.Close();
                newStream.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return ret;
        }

        public static HttpWebResponse CreateHttpResponse(string url, IDictionary<string, string> parameters, int timeout, string userAgent, CookieCollection cookies, string strType = "POST")
        {
            HttpWebRequest request = null;
            //如果是发送HTTPS请求  
            if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
            {
                //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                request = WebRequest.Create(url) as HttpWebRequest;
                //request.ProtocolVersion = HttpVersion.Version10;
            }
            else
            {
                request = WebRequest.Create(url) as HttpWebRequest;
            }
            request.Method = strType;
            request.ContentType = "application/x-www-form-urlencoded";

            //设置代理UserAgent和超时
            //request.UserAgent = userAgent;
            //request.Timeout = timeout; 

            if (cookies != null)
            {
                request.CookieContainer = new CookieContainer();
                request.CookieContainer.Add(cookies);
            }
            //发送POST数据  
            if (!(parameters == null || parameters.Count == 0))
            {
                StringBuilder buffer = new StringBuilder();
                int i = 0;
                foreach (string key in parameters.Keys)
                {
                    if (i > 0)
                    {
                        buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                    }
                    else
                    {
                        buffer.AppendFormat("{0}={1}", key, parameters[key]);
                        i++;
                    }
                }
                byte[] data = Encoding.UTF8.GetBytes(buffer.ToString());
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            string[] values = request.Headers.GetValues("Content-Type");
            return request.GetResponse() as HttpWebResponse;
        }

        /// <summary>  
        /// 创建POST方式的HTTP请求  
        /// </summary>  
        public static HttpWebResponse CreatePostHttpResponse(string url, IDictionary<string, string> parameters, int timeout, string userAgent, CookieCollection cookies, string strType = "POST")
        {
            HttpWebRequest request = null;
            //如果是发送HTTPS请求  
            if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
            {
                //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                request = WebRequest.Create(url) as HttpWebRequest;
                //request.ProtocolVersion = HttpVersion.Version10;
            }
            else
            {
                request = WebRequest.Create(url) as HttpWebRequest;
            }
            request.Method = strType;
            request.ContentType = "application/x-www-form-urlencoded";

            //设置代理UserAgent和超时
            //request.UserAgent = userAgent;
            //request.Timeout = timeout; 

            if (cookies != null)
            {
                request.CookieContainer = new CookieContainer();
                request.CookieContainer.Add(cookies);
            }
            //发送POST数据  
            if (!(parameters == null || parameters.Count == 0))
            {
                StringBuilder buffer = new StringBuilder();
                int i = 0;
                foreach (string key in parameters.Keys)
                {
                    if (i > 0)
                    {
                        buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                    }
                    else
                    {
                        buffer.AppendFormat("{0}={1}", key, parameters[key]);
                        i++;
                    }
                }
                byte[] data = Encoding.UTF8.GetBytes(buffer.ToString());
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            string[] values = request.Headers.GetValues("Content-Type");
            return request.GetResponse() as HttpWebResponse;
        }

        /// <summary>
        /// 获取请求的数据
        /// </summary>
        public static string GetResponseString(HttpWebResponse webresponse)
        {
            using (Stream s = webresponse.GetResponseStream())
            {
                StreamReader reader = new StreamReader(s, Encoding.Default);
                return reader.ReadToEnd();

            }
        }


        //
        public static bool HasData(Stream excelFileStream)
        {
            using (excelFileStream)
            {
                IWorkbook workbook = new HSSFWorkbook(excelFileStream);
                if (workbook.NumberOfSheets > 0)
                {
                    ISheet sheet = workbook.GetSheetAt(0);
                    return sheet.PhysicalNumberOfRows > 0;
                }
            }
            return false;
        }



        public static string HttpGet(string Url)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }
        public static string GetAPIData(string strUrl)
        {
            string strReturn = "";
            string strHost = GetConfig("APITX");
            strHost = strHost.Substring(0, strHost.IndexOf("/api"));
            strReturn = CommonHelp.HttpGet(strUrl.Replace("$API_HOST", strHost));
            return strReturn;
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadUrl"></param>
        /// <param name="fileToUpload"></param>
        /// <param name="poststr"></param>
        /// <returns></returns>
        public static string PostFile(JH_Auth_QY QYinfo, string fileToUpload, string poststr = "")
        {
            string result = "";
            string uploadUrl = QYinfo.FileServerUrl.TrimEnd('/') + "/document/fileupload/" + QYinfo.QYCode;
            try
            {
                string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uploadUrl);
                webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
                webrequest.Method = "POST";
                StringBuilder sb = new StringBuilder();
                if (poststr != "")
                {
                    foreach (string c in poststr.Split('&'))
                    {
                        string[] item = c.Split('=');
                        if (item.Length != 2)
                        {
                            break;
                        }
                        string name = item[0];
                        string value = item[1];
                        sb.Append("–" + boundary);
                        sb.Append("\r\n");
                        sb.Append("Content-Disposition: form-data; name=\"" + name + "\"");
                        sb.Append("\r\n\r\n");
                        sb.Append(value);
                        sb.Append("\r\n");
                    }
                }
                sb.Append("--");
                sb.Append(boundary);
                sb.Append("\r\n");
                sb.Append("Content-Disposition: form-data; name=\"file");
                //sb.Append(fileFormName);
                sb.Append("\"; filename=\"");
                sb.Append(Path.GetFileName(fileToUpload));
                sb.Append("\"");
                sb.Append("\r\n");
                sb.Append("Content-Type: application/octet-stream");
                //sb.Append(contenttype);
                sb.Append("\r\n");
                sb.Append("\r\n");
                string postHeader = sb.ToString();
                byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);
                byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
                FileStream fileStream = new FileStream(fileToUpload, FileMode.Open, FileAccess.Read);
                long length = postHeaderBytes.Length + fileStream.Length + boundaryBytes.Length;
                webrequest.ContentLength = length;
                Stream requestStream = webrequest.GetRequestStream();
                requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);
                byte[] buffer = new Byte[(int)fileStream.Length];
                int bytesRead = 0;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    requestStream.Write(buffer, 0, bytesRead);
                }
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                fileStream.Close();
                WebResponse responce = webrequest.GetResponse();
                requestStream.Close();
                using (Stream s = responce.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        result = sr.ReadToEnd();
                    }
                }

            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG(uploadUrl + "|||" + fileToUpload + "|||" + ex.ToString());
            }
            return result;
        }





        public static string SendGGDX(string Mobile, string Content, string SendTime)
        {
            try
            {
                string returnData = "";
                // string url = CommonHelp.GetConfig("DXURL") + "&Mobile=" + Mobile + "&Content=" + Content;
                if (CommonHelp.GetConfig("DXURL") != "")
                {
                    string strContent = "【" + CommonHelp.GetConfig("DXQZ") + "】" + Content;
                    //string url = CommonHelp.GetConfig("DXURL") + "&Mobile=" + Mobile + "&msg=" + HttpUtility.UrlEncode(strContent);
                    string url = CommonHelp.GetConfig("DXURL") + "&Mobile=" + Mobile + "&Content=" + strContent;
                    CommonHelp.WriteLOG(url);
                    WebClient WC = new WebClient();
                    WC.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    int p = url.IndexOf("?");
                    string sData = url.Substring(p + 1);
                    url = url.Substring(0, p);
                    byte[] postData = Encoding.GetEncoding("utf-8").GetBytes(sData);
                    byte[] responseData = WC.UploadData(url, "POST", postData);
                    returnData = Encoding.GetEncoding("utf-8").GetString(responseData);
                }
                return returnData;


            }
            catch (Exception Ex)
            {
                return Ex.Message;

            }

        }

        public static string SendDX(string Mobile, string Content, string SendTime)
        {
            try
            {
                string returnData = "";
                if (CommonHelp.GetConfig("DXURL") != "")
                {

                    string url = CommonHelp.GetConfig("DXURL") + "&phone=" + Mobile + "&msg=" + Content;
                    //string url = CommonHelp.GetConfig("DXURL") + "&Mobile=" + Mobile + "&Content=" + strContent;
                    CommonHelp.WriteLOG(url);
                    CommonHelp.HttpGet(url);

                    //WebClient WC = new WebClient();
                    //WC.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    //int p = url.IndexOf("?");
                    //string sData = url.Substring(p + 1);
                    //url = url.Substring(0, p);
                    //byte[] postData = Encoding.GetEncoding("gb2312").GetBytes(sData);
                    //byte[] responseData = WC.UploadData(url, "POST", postData);
                    //returnData = Encoding.GetEncoding("gb2312").GetString(responseData);
                }
                return returnData;


            }
            catch (Exception Ex)
            {
                CommonHelp.WriteLOG("发送短信" + Ex.Message);

                return "";
            }

        }



        #endregion


        #region 其它
        public static T DeepCopyByReflect<T>(T obj)
        {
            //如果是字符串或值类型则直接返回
            if (obj is string || obj.GetType().IsValueType) return obj;

            object retval = Activator.CreateInstance(obj.GetType());
            FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            foreach (FieldInfo field in fields)
            {
                try { field.SetValue(retval, DeepCopyByReflect(field.GetValue(obj))); }
                catch { }
            }
            return (T)retval;
        }
        /// <summary>
        /// 从html中提取纯文本
        /// </summary>
        /// <param name="strHtml"></param>
        /// <returns></returns>
        public string StripHT(string strHtml)  //从html中提取纯文本
        {
            Regex regex = new Regex("<.+?>", RegexOptions.IgnoreCase);
            string strOutput = regex.Replace(strHtml, "");//替换掉"<"和">"之间的内容
            strOutput = strOutput.Replace("<", "");
            strOutput = strOutput.Replace(">", "");
            strOutput = strOutput.Replace("&nbsp;", "");
            return strOutput;
        }


        /// <summary>
        /// 移除html标签
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string RemoveHtml(string html)
        {
            if (string.IsNullOrEmpty(html)) return html;

            Regex regex = new Regex("<.+?>");
            var matches = regex.Matches(html);

            foreach (Match match in matches)
            {
                html = html.Replace(match.Value, "");
            }
            return html;
        }




        private int rep = 0;


        /// <summary>
        /// 生成随机不重复的字符串（分享码用）
        /// </summary>
        /// <param name="codeCount"></param>
        /// <returns></returns>
        public string GenerateCheckCode(int codeCount)
        {
            string str = string.Empty;
            long num2 = DateTime.Now.Ticks + this.rep;
            this.rep++;
            Random random = new Random(((int)(((ulong)num2) & 0xffffffffL)) | ((int)(num2 >> this.rep)));
            for (int i = 0; i < codeCount; i++)
            {
                char ch;
                int num = random.Next();
                if ((num % 2) == 0)
                {
                    ch = (char)(0x30 + ((ushort)(num % 10)));
                }
                else
                {
                    ch = (char)(0x41 + ((ushort)(num % 0x1a)));
                }
                str = str + ch.ToString();
            }
            return str;
        }
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string GetMD5(string content)
        {
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(content));
                var strResult = BitConverter.ToString(result);
                return strResult.Replace("-", "");
            }
        }



        public static string GetConfig(string strKey, string strDefault = "")
        {
            return Appsettings.app(strKey) ?? strDefault;
        }

        /// <summary>
        /// 获取数字验证码
        /// </summary>
        /// <param name="codenum"></param>
        /// <returns></returns>
        public static string numcode(int codenum)
        {
            string Vchar = "0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9";
            string[] VcArray = Vchar.Split(',');
            string[] stray = new string[codenum];
            Random random = new Random();
            for (int i = 0; i < codenum; i++)
            {
                int iNum = 0;
                while ((iNum = Convert.ToInt32(VcArray.Length * random.NextDouble())) == VcArray.Length)
                {
                    iNum = Convert.ToInt32(VcArray.Length * random.NextDouble());
                }
                stray[i] = VcArray[iNum];
            }

            string identifycode = string.Empty;
            foreach (string s in stray)
            {
                identifycode += s;
            }
            return identifycode;
        }
        /// <summary>
        /// 登录验证码
        /// </summary>
        /// <param name="codenum"></param>
        /// <returns></returns>
        public static string yzmcode(int codenum)
        {
            string Vchar = "0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,W,X,Y,Z";
            string[] VcArray = Vchar.Split(',');
            string[] stray = new string[codenum];
            Random random = new Random();
            for (int i = 0; i < codenum; i++)
            {
                int iNum = 0;
                while ((iNum = Convert.ToInt32(VcArray.Length * random.NextDouble())) == VcArray.Length)
                {
                    iNum = Convert.ToInt32(VcArray.Length * random.NextDouble());
                }
                stray[i] = VcArray[iNum];
            }

            string identifycode = string.Empty;
            foreach (string s in stray)
            {
                identifycode += s;
            }
            return identifycode;
        }

        private static bool IsIPAddress(string str1)
        {
            if (str1 == null || str1 == string.Empty || str1.Length < 7 || str1.Length > 15) return false;

            string regformat = @"^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$";

            Regex regex = new Regex(regformat, RegexOptions.IgnoreCase);
            return regex.IsMatch(str1);
        }




        public static void WriteLOG(string err)
        {
            try
            {
                //string path = AppContext.BaseDirectory; .NET CORE
                string path = HttpContext.Current.Request.MapPath("/");

                if (!Directory.Exists(path + "/log/"))
                {
                    Directory.CreateDirectory(path + "/log/");
                }

                string name = DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                if (!File.Exists(path + "/log/" + name))
                {
                    FileInfo myfile = new FileInfo(path + "/log/" + name);
                    FileStream fs = myfile.Create();
                    fs.Close();
                }

                StreamWriter sw = File.AppendText(path + "/log/" + name);
                sw.WriteLine(err + "\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {

            }


        }








        public static bool ProcessSqlStr(string Str, int type)
        {
            string SqlStr = "";
            if (type == 1)  //Post方法提交  
            {
                SqlStr = "script|iframe|xp_loginconfig|xp_fixeddrives|Xp_regremovemultistring|Xp_regread|Xp_regwrite|xp_cmdshell|xp_dirtree|count(|substring(|mid(|master|truncate|char(|declare|replace(|varchar(|cast(";
            }
            else if (type == 2) //Get方法提交  
            {
                SqlStr = "'|script|iframe|xp_loginconfig|xp_fixeddrives|Xp_regremovemultistring|Xp_regread|Xp_regwrite|xp_cmdshell|xp_dirtree|count(|*|asc(|chr(|substring(|mid(|master|truncate|char(|declare|replace(|;|varchar(|cast(";
            }
            else if (type == 3) //Cookie提交  
            {
                SqlStr = "script|iframe|xp_loginconfig|xp_fixeddrives|Xp_regremovemultistring|Xp_regread|Xp_regwrite|xp_cmdshell|xp_dirtree|count(|asc(|chr(|substring(|mid(|master|truncate|char(|declare";
            }
            else  //默认Post方法提交  
            {
                SqlStr = "script|iframe|xp_loginconfig|xp_fixeddrives|Xp_regremovemultistring|Xp_regread|Xp_regwrite|xp_cmdshell|xp_dirtree|count(|asc(|chr(|substring(|mid(|master|truncate|char(|declare|replace(";
            }

            bool ReturnValue = true;
            try
            {
                if (Str != "")
                {
                    string[] anySqlStr = SqlStr.ToUpper().Split('|'); ;
                    foreach (string ss in anySqlStr)
                    {
                        if (Str.ToUpper().IndexOf(ss) >= 0)
                        {
                            ReturnValue = false;
                        }
                    }
                }
            }
            catch
            {
                ReturnValue = false;
            }
            return ReturnValue;
        }


        public static string Filter(string str)
        {
            string[] pattern = { " insert ", " delete ", "count\\(", "drop table", " update ", " truncate ", " xp_cmdshell ", "exec   master", "netlocalgroup administrators", "net use " };
            for (int i = 0; i < pattern.Length; i++)
            {

                str = str.Replace(pattern[i].ToString(), "");
            }
            return str;
        }





        public static string CreateqQsql(string strQFiled, string strQtype, string strQvalue)
        {
            string strSQL = " AND ";
            strSQL = strSQL + strQFiled;
            if (strQtype == "0")
            {
                strSQL = strSQL + " = ";

            }
            strSQL = strSQL + "'" + strQvalue + "'";
            return strSQL.FilterSpecial();

        }



        /// <summary>
        /// 生成流水号格式：8位日期加3位顺序号，如20100302001。
        /// </summary>
        public static string GetWFNumber(string serialNumber, string ywcode, string strWFQZ)
        {
            if (serialNumber.Length < 13)
            {
                strWFQZ = "1";
                //没法子,只能曲线救国
            }
            string strLSTemp = strWFQZ == "0" ? "yyyyMMdd" : "yyyy";
            if (serialNumber != "0" && serialNumber != "")
            {
                string headDate = serialNumber.Substring(ywcode.Length + 1, strLSTemp.Length);
                int lastNumber = int.Parse(serialNumber.Substring(ywcode.Length + 1 + strLSTemp.Length));
                //如果数据库最大值流水号中日期和生成日期在同一天，则顺序号加1
                if (headDate == DateTime.Now.ToString(strLSTemp))
                {
                    lastNumber++;
                    return ywcode + "-" + headDate + lastNumber.ToString("000");
                }
            }
            return ywcode + "-" + DateTime.Now.ToString(strLSTemp) + "001";
        }

        /// <summary>
        /// 转化时分秒
        /// </summary>
        /// <param name="strSFM"></param>
        /// <returns></returns>
        public static int GetSencond(string strSFM)
        {
            int[] ListTemp = strSFM.SplitTOInt(':');

            return ListTemp[0] * 3600 + ListTemp[1] * 60 + ListTemp[2];
        }



        /// <summary>
        /// 在DataTable末尾添加合计行，最后一个参数设置需要合计的列，Tools.dbDataTableSumRowsWithColList(dt,"车号",new string[]{"个人产值","节油（元）","超油（元）"});
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="nColHeJi">需要显示‘合计’字段的列</param>
        /// <param name="colsHeJi">需要合计的列</param>
        public static DataRow dbDataTableSumRowsWithColList(DataTable dt, string sColHeJi, string[] colsHeJi)
        {
            DataRow dr = dt.NewRow();
            dr[sColHeJi] = "合计";
            dt.Rows.Add(dr);
            //初始化合计数组
            decimal[] arrDec = new decimal[colsHeJi.Length];
            for (int i = 0; i < colsHeJi.Length; i++)
            {
                arrDec[i] = decimal.Zero;
            }
            //合计
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                for (int j = 0; j < colsHeJi.Length; j++)
                {
                    string cName = colsHeJi[j];
                    if (Convert.IsDBNull(dt.Rows[i][cName])) continue;
                    arrDec[j] += decimal.Parse(dt.Rows[i][cName] + "");
                }
            }
            //赋值
            for (int i = 0; i < colsHeJi.Length; i++)
            {
                string cName = colsHeJi[i];
                if (arrDec[i] == decimal.Zero) dr[cName] = "0";
                else dr[cName] = arrDec[i];
            }
            return dr;
        }



        /// <summary>
        /// 分类小计，并有合计行，
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="nColHeJi">需要显示‘合计’字段的列</param>
        /// <param name="colsHeJi">需要合计的列</param>
        public static DataRow dbDataTableSubSumRowsWithColList(DataTable dt, string sColHeJi, string strGroupName, string[] colsHeJi)
        {
            DataRow dr = dt.NewRow();
            dr[sColHeJi] = "合计";
            dt.Rows.Add(dr);
            //初始化合计数组
            decimal[] arrDec = new decimal[colsHeJi.Length];
            for (int i = 0; i < colsHeJi.Length; i++)
            {
                arrDec[i] = decimal.Zero;
            }
            //合计
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                for (int j = 0; j < colsHeJi.Length; j++)
                {
                    string cName = colsHeJi[j];
                    if (Convert.IsDBNull(dt.Rows[i][cName])) continue;
                    arrDec[j] += decimal.Parse(dt.Rows[i][cName] + "");
                }
            }
            for (int i = 0; i < colsHeJi.Length; i++)
            {
                string cName = colsHeJi[i];
                if (arrDec[i] == decimal.Zero) dr[cName] = 0;
                else dr[cName] = arrDec[i];
            }
            if (dt.Rows.Count <= 1) return dr;
            //小计
            string sRate = "";
            int currSubSumCol = dt.Rows.Count - 2;
            ArrayList ar = new ArrayList();
            for (int i = dt.Rows.Count - 2; i >= 0; i--)
            {
                string currRate = dt.Rows[i][strGroupName] + "";
                if (sRate != currRate)
                {
                    if (i != dt.Rows.Count - 2)
                    {
                        dr = dt.NewRow();
                        dr[sColHeJi] = "小计";
                        for (int j = 0; j < colsHeJi.Length; j++)
                        {
                            string cName = colsHeJi[j];
                            if (arrDec[j] == decimal.Zero) dr[cName] = 0;
                            else dr[cName] = arrDec[j];
                        }
                        dt.Rows.InsertAt(dr, currSubSumCol + 1);
                    }
                    currSubSumCol = i;
                    sRate = currRate;
                    for (int j = 0; j < colsHeJi.Length; j++)
                    { //归零
                        arrDec[j] = decimal.Zero;
                    }
                }
                for (int j = 0; j < colsHeJi.Length; j++)
                {
                    string cName = colsHeJi[j];
                    if (Convert.IsDBNull(dt.Rows[i][cName])) continue;
                    arrDec[j] += decimal.Parse(dt.Rows[i][cName] + "");
                }
                if (i == 0)
                {
                    dr = dt.NewRow();
                    dr[sColHeJi] = "小计";
                    for (int j = 0; j < colsHeJi.Length; j++)
                    {
                        string cName = colsHeJi[j];
                        if (arrDec[j] == decimal.Zero) dr[cName] = 0;
                        else dr[cName] = arrDec[j];
                    }
                    dt.Rows.InsertAt(dr, currSubSumCol + 1);
                }
            }

            return dr;
        }


        /// <summary>

        /// 在指定的字符串列表CnStr中检索符合拼音索引字符串

        /// </summary>

        /// <param name="CnStr">汉字字符串</param>

        /// <returns>相对应的汉语拼音首字母串</returns>

        public static string GetSpellCode(string CnStr)
        {

            string strTemp = "";

            int iLen = CnStr.Length;

            int i = 0;

            for (i = 0; i <= iLen - 1; i++)
            {

                strTemp += GetCharSpellCode(CnStr.Substring(i, 1));
                break;
            }

            return strTemp;

        }

        /// <summary>

        /// 得到一个汉字的拼音第一个字母，如果是一个英文字母则直接返回大写字母

        /// </summary>

        /// <param name="CnChar">单个汉字</param>

        /// <returns>单个大写字母</returns>

        private static string GetCharSpellCode(string CnChar)
        {

            long iCnChar;

            byte[] ZW = System.Text.Encoding.Default.GetBytes(CnChar);

            //如果是字母，则直接返回首字母

            if (ZW.Length == 1)
            {

                return CnChar.Substring(0, 1);

            }
            else
            {

                // get the array of byte from the single char

                int i1 = (short)(ZW[0]);

                int i2 = (short)(ZW[1]);

                iCnChar = i1 * 256 + i2;

            }

            // iCnChar match the constant

            if ((iCnChar >= 45217) && (iCnChar <= 45252))
            {

                return "A";

            }

            else if ((iCnChar >= 45253) && (iCnChar <= 45760))
            {

                return "B";

            }
            else if ((iCnChar >= 45761) && (iCnChar <= 46317))
            {

                return "C";

            }
            else if ((iCnChar >= 46318) && (iCnChar <= 46825))
            {

                return "D";

            }
            else if ((iCnChar >= 46826) && (iCnChar <= 47009))
            {

                return "E";

            }
            else if ((iCnChar >= 47010) && (iCnChar <= 47296))
            {

                return "F";

            }
            else if ((iCnChar >= 47297) && (iCnChar <= 47613))
            {

                return "G";

            }
            else if ((iCnChar >= 47614) && (iCnChar <= 48118))
            {

                return "H";

            }
            else if ((iCnChar >= 48119) && (iCnChar <= 49061))
            {

                return "J";

            }
            else if ((iCnChar >= 49062) && (iCnChar <= 49323))
            {

                return "K";

            }
            else if ((iCnChar >= 49324) && (iCnChar <= 49895))
            {

                return "L";

            }
            else if ((iCnChar >= 49896) && (iCnChar <= 50370))
            {

                return "M";

            }
            else if ((iCnChar >= 50371) && (iCnChar <= 50613))
            {

                return "N";

            }
            else if ((iCnChar >= 50614) && (iCnChar <= 50621))
            {

                return "O";

            }
            else if ((iCnChar >= 50622) && (iCnChar <= 50905))
            {

                return "P";

            }
            else if ((iCnChar >= 50906) && (iCnChar <= 51386))
            {

                return "Q";

            }
            else if ((iCnChar >= 51387) && (iCnChar <= 51445))
            {

                return "R";

            }
            else if ((iCnChar >= 51446) && (iCnChar <= 52217))
            {

                return "S";

            }
            else if ((iCnChar >= 52218) && (iCnChar <= 52697))
            {

                return "T";

            }
            else if ((iCnChar >= 52698) && (iCnChar <= 52979))
            {

                return "W";

            }
            else if ((iCnChar >= 52980) && (iCnChar <= 53640))
            {

                return "X";

            }
            else if ((iCnChar >= 53689) && (iCnChar <= 54480))
            {

                return "Y";

            }
            else if ((iCnChar >= 54481) && (iCnChar <= 55289))
            {

                return "Z";

            }
            else

                return ("?");

        }



        /// <summary>
        /// 在指定时间过后执行指定的表达式
        /// </summary>
        /// <param name="interval">事件之间经过的时间（以毫秒为单位）</param>
        /// <param name="action">要执行的表达式</param>
        public static void SetTimeout(double interval, Action action)
        {
            System.Timers.Timer timer = new System.Timers.Timer(interval);
            timer.Elapsed += delegate (object sender, System.Timers.ElapsedEventArgs e)
            {
                timer.Enabled = false;
                action();
            };
            timer.Enabled = true;
        }
        /// <summary>
        /// 在指定时间周期重复执行指定的表达式
        /// </summary>
        /// <param name="interval">事件之间经过的时间（以毫秒为单位）</param>
        /// <param name="action">要执行的表达式</param>
        public static void SetInterval(double interval, Action<ElapsedEventArgs> action)
        {
            System.Timers.Timer timer = new System.Timers.Timer(interval);
            timer.Elapsed += delegate (object sender, System.Timers.ElapsedEventArgs e)
            {
                action(e);
            };
            timer.Enabled = true;
        }



        /// <summary>
        /// 获取硬件信息
        /// </summary>
        /// <returns></returns>
        public static List<string> getSysInfo()
        {
            List<string> ListInfo = new List<string>();

            string strSys = "";
            ManagementClass manag = new ManagementClass("Win32_OperatingSystem");
            ManagementObjectCollection managCollection = manag.GetInstances();
            foreach (ManagementObject m in managCollection)
            {
                strSys = m["Name"].ToString();
                break;
            }
            ListInfo.Add(strSys.Split('|')[0]);


            string strCPU = "";
            ManagementClass mcCPU = new ManagementClass("Win32_Processor");
            ManagementObjectCollection mocCPU = mcCPU.GetInstances();
            foreach (ManagementObject m in mocCPU)
            {
                strCPU = m["Name"].ToString();
                break;
            }

            int coreCount = 0;
            foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor ").Get())
            {
                coreCount += int.Parse(item["NumberOfCores"].ToString());
            }
            string strcpudata = coreCount.ToString();
            strCPU = strCPU + "," + strcpudata;

            ListInfo.Add(strCPU);


            ManagementObjectSearcher searcher = new ManagementObjectSearcher();   //用于查询一些如系统信息的管理对象
            searcher.Query = new SelectQuery("Win32_PhysicalMemory", "", new string[] { "Capacity" });//设置查询条件
            ManagementObjectCollection collection = searcher.Get();   //获取内存容量 
            ManagementObjectCollection.ManagementObjectEnumerator em = collection.GetEnumerator();
            long capacity = 0;
            while (em.MoveNext())
            {
                ManagementBaseObject baseObj = em.Current;
                if (baseObj.Properties["Capacity"].Value != null)
                {
                    capacity += long.Parse(baseObj.Properties["Capacity"].Value.ToString());
                }
            }
            long nc = capacity / 1024 / 1024 / 1024;

            long availablebytes = 0;
            ManagementClass mos = new ManagementClass("Win32_OperatingSystem");
            foreach (ManagementObject mo in mos.GetInstances())
            {
                if (mo["FreePhysicalMemory"] != null)
                {
                    availablebytes = long.Parse(mo["FreePhysicalMemory"].ToString());
                }
            }
            long kync = availablebytes / 1024 / 1024;


            ListInfo.Add((nc - kync) + "/" + nc + "GB");


            string strYPInfo = "";
            System.IO.DriveInfo[] drives = System.IO.DriveInfo.GetDrives();
            foreach (var drive in drives)
            {
                long totalFreeSpace = 0;
                long totalDiskSize = 0;
                string strName = "";

                if (drive.IsReady)  //判断代码运行时 磁盘是可操作作态 
                {
                    totalFreeSpace = drive.AvailableFreeSpace;
                    totalDiskSize = drive.TotalSize;
                    strName = drive.Name + "盘";
                    strYPInfo = strYPInfo + strName + "--" + (totalDiskSize - totalFreeSpace) / 1024 / 1024 / 1024 + "/" + totalDiskSize / 1024 / 1024 / 1024 + "G,";
                }
            }
            ListInfo.Add(strYPInfo.TrimEnd(','));
            return ListInfo;
        }



        /// <summary>
        /// 根据Model生成相关SQL
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string CreateModelSql<T>(T model)
        {
            Type type = typeof(T);
            //string[] strSqlNames = type.GetProperties().Select(p => $"[{p.Name}]").ToArray();
            //以下是不要ID这个字段的  比如自增列ID  就不能像上名那样写   
            string[] strSqlNames = type.GetProperties().Where(p => !p.Name.Equals("Id")).Select(p => $"[{p.Name}]").ToArray();
            string strSqlName = string.Join(",", strSqlNames);

            //string[] strSqlValues = type.GetProperties().Select(P => $"@{P.Name}").ToArray();

            string[] paraval = type.GetProperties().Select(p => $"{p.GetValue(model)}").ToArray();

            string strSqlValue = string.Join(",", paraval).ToFormatLike();
            //  strSql得到的  sql语句为insert into testModel ( [ID],[Name],[QQ],[Email] ) values (@ID,@Name,@QQ,@Email)
            string strSql = "insert into " + type.Name + " ( " + strSqlName + " ) values ('" + strSqlValue + "')";


            //para Sql是参数

            return strSql;
        }


        /// <summary>
        /// 验证身份证号码正确性
        /// </summary>
        /// <param name="idNumber"></param>
        /// <returns></returns>
        public static bool CheckIDCard18(string idNumber)
        {
            long n = 0;
            if (long.TryParse(idNumber.Remove(17), out n) == false
                || n < Math.Pow(10, 16) || long.TryParse(idNumber.Replace('x', '0').Replace('X', '0'), out n) == false)
            {
                return false;//数字验证  
            }
            string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
            if (address.IndexOf(idNumber.Remove(2)) == -1)
            {
                return false;//省份验证  
            }
            string birth = idNumber.Substring(6, 8).Insert(6, "-").Insert(4, "-");
            DateTime time = new DateTime();
            if (DateTime.TryParse(birth, out time) == false)
            {
                return false;//生日验证  
            }
            string[] arrVarifyCode = ("1,0,x,9,8,7,6,5,4,3,2").Split(',');
            string[] Wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');
            char[] Ai = idNumber.Remove(17).ToCharArray();
            int sum = 0;
            for (int i = 0; i < 17; i++)
            {
                sum += int.Parse(Wi[i]) * int.Parse(Ai[i].ToString());
            }
            int y = -1;
            Math.DivRem(sum, 11, out y);
            if (arrVarifyCode[y] != idNumber.Substring(17, 1).ToLower())
            {
                return false;//校验码验证  
            }
            return true;//符合GB11643-1999标准  
        }



        /// <summary>
        /// 获取API接口方法说明
        /// </summary>
        /// <returns></returns>
        public static string GetApiDes(string strAction)
        {
            string strDEC = "";
            try
            {
                var basePath = HttpContext.Current.Request.MapPath("/");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(basePath + "//bin//QJY.API.xml");

                string strKey = strAction.Split('_')[0] + "Manage." + strAction.Split('_')[1].ToString();
                XmlNodeList topM = xmlDoc.DocumentElement.ChildNodes;

                foreach (XmlElement element in topM)
                {
                    if (element.Name.ToLower() == "members")
                    {
                        //得到该节点的子节点 
                        XmlNodeList nodelist = element.ChildNodes;
                        if (nodelist.Count > 0)
                        {
                            //DropDownList1.Items.Clear(); 
                            foreach (XmlElement el in nodelist)//读元素值 
                            {

                                if (el.Attributes["name"].Value.ToString().Contains(strKey))
                                {
                                    strDEC = el.ChildNodes[0].InnerText.Trim();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return strDEC;

            }

            return strDEC;
        }



        /// <summary>
        /// 根据前端JArray参数获取查询SQL
        /// </summary>
        /// <param name="JQ"></param>
        /// <returns></returns>
        public static string GetQuertSQL(JArray JQ)
        {
            string strQWhere = "";

            foreach (JObject queryitem in JQ)
            {
                string strcxzd = (string)queryitem["colid"];
                string strcal = (string)queryitem["cal"];
                string strglval = (string)queryitem["val"];
                string cxtype = (string)queryitem["cxtype"];
                string strtempwhere = "";

                if (!string.IsNullOrEmpty(strglval))
                {
                    switch (strcal)
                    {
                        case "0":
                            strtempwhere = " and " + strcxzd + " ='" + strglval + "'";
                            strQWhere = strQWhere + strtempwhere;
                            break;
                        case "1":
                            strtempwhere = " and " + strcxzd + " <'" + strglval + "'";
                            strQWhere = strQWhere + strtempwhere;

                            break;
                        case "2":
                            strtempwhere = " and " + strcxzd + " >'" + strglval + "'";
                            strQWhere = strQWhere + strtempwhere;

                            break;
                        case "3":
                            strtempwhere = " and " + strcxzd + " !='" + strglval + "'";
                            strQWhere = strQWhere + strtempwhere;

                            break;
                        case "4":
                            strtempwhere = " and " + strcxzd + "  LIKE  '%" + strglval.ToFormatLike() + "%'";
                            strQWhere = strQWhere + strtempwhere;

                            break;
                        case "5":
                            strtempwhere = " and " + strcxzd + "  BETWEEN '" + strglval.Split(',')[0] + " 00:00:00' AND '" + strglval.Split(',')[1] + " 23:59:59 '";
                            strQWhere = strQWhere + strtempwhere;

                            break;
                        case "6":
                            strtempwhere = " and " + strcxzd + "  IN  ('" + strglval.ToFormatLike() + "')";
                            strQWhere = strQWhere + strtempwhere;
                            break;
                    }


                }


            }
            return strQWhere;
        }
        #endregion

    }

}