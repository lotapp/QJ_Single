﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace QJY.Common
{
    public class JwtHelper
    {

        static readonly string baseData = "abcdefghijklmnopqrstuvwxyz0123456789-";
        static readonly string basekey = "qijiekeji";


        /// <summary>
        /// 颁发JWT字符串
        /// </summary>
        /// <param name="tokenModel"></param>
        /// <returns></returns>
        public static TokenJWT CreateJWT(string strUserName)
        {
            TokenJWT Token = new TokenJWT();
            Token.UserName = strUserName;
            string exp = DateTime.Now.AddSeconds(int.Parse(CommonHelp.GetConfig("exptime", "24200"))).ToUniversalTime().Ticks.ToString();
            Token.Exp = exp;
            Token.Token = "";
            string strTemp = strUserName + "-" + exp;
            string strToken = EncrpytHelper.EncodeDES(strTemp, basekey);
            Token.Token = strToken;
            return Token;
        }

        public static TokenJWT DePJWT(string strCode)
        {
            string strTemp = EncrpytHelper.DecodeDES(strCode, basekey);
            TokenJWT Token = new TokenJWT();
            Token.UserName = Unicode2String(strTemp.Split('-')[0]);
            Token.Exp = strTemp.Split('-')[1];
            Token.Token = strCode;

            return Token;

        }


        /// <summary>
        /// <summary>
        /// 字符串转Unicode
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <returns>Unicode编码后的字符串</returns>
        public static string String2Unicode(string source)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(source);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i += 2)
            {
                stringBuilder.AppendFormat("\\u{0}{1}", bytes[i + 1].ToString("x").PadLeft(2, '0'), bytes[i].ToString("x").PadLeft(2, '0'));
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Unicode转字符串
        /// </summary>
        /// <param name="source">经过Unicode编码的字符串</param>
        /// <returns>正常字符串</returns>
        public static string Unicode2String(string source)
        {
            return new Regex(@"\\u([0-9A-F]{4})", RegexOptions.IgnoreCase | RegexOptions.Compiled).Replace(
                source, x => string.Empty + Convert.ToChar(Convert.ToUInt16(x.Result("$1"), 16)));
        }

    }

    /// <summary>
    /// 令牌
    /// </summary>
    public class TokenJWT
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Exp { get; set; }
    }
}