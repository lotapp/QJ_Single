﻿using System;
using System.Collections.Generic;

namespace QJY.Data
{
    public partial class QJ_JW
    {
        public int? ComId { get; set; }
        public int ID { get; set; }
        public int? Class { get; set; }
        public DateTime? CRDate { get; set; }
        public int? intProcessStanceid { get; set; }
    }
}
